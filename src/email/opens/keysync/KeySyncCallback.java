/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync;

import java.util.HashMap;
import java.util.HashSet;

import javax.mail.Folder;

import email.opens.keysync.core.Store_msg;

public class KeySyncCallback {

    public HashMap<String, String>			store 		= new HashMap<String, String>();
    public HashSet<Store_msg.TextMsgStruct>	msgs		= new HashSet<Store_msg.TextMsgStruct>();
    public String							keystroke	= "";
    public boolean							done		= false;

    public Folder							folder		= null;

    public String get (String key) {
        return store.get(key);
    }

    public boolean put (String field_parameter, String value_parameter) {

        boolean success = false;
        // if any issue check is done on the passed data, then success can be set to false
        store.put(field_parameter, value_parameter);
        return success;
    }

    public boolean putMsgs(HashSet<Store_msg.TextMsgStruct> msgs_parameter) {
        msgs = msgs_parameter;

        return true;
    }

    public void completed () {
        done	= true;
    }

}

		
