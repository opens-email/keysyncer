/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.Callable;

import email.opens.keysync.core.*;

import email.opens.keysync.io.IO;
import email.opens.keysync.io.local.Store_xml;
import email.opens.keysync.io.net.Invite_ServerComm;
import email.opens.keysync.io.net.StandBy_email;
import email.opens.keysync.cli.SimulateAndMonitor;
import email.opens.keysync.cli.CLI;
import email.opens.keysync.cli.Test;

/****** main								- is the command line entry point, it hands over args to run and prints out the "feedback"
******* run									- can run an api function using args and return feedback - e.g. for tests
 
******* API	(3 types of API callers are expected): 1st handeles emails itself 2nd lets KX do that, 3rd uses KX by the method "run"

1  2  3 

x		getMsgActions						- mail tasks taken by caller: caller provides inbox and receives mails to be sent, postSend is mandatory
   x	getMsgSetFromInboxAndAddressbook	- mail tasks handled by "KeyExchange" using javamail: receive is included, send by IO.sendMsg
x		postSend							- mail tasks taken by caller: use this method after email have been sent to update the internal status
   x	send								- mail tasks handled by "KeyExchange" using javamail: receive is included, will send text "text" to email address "to" (using IO.sendMsg)

x   x	timer								- cares about internal status, give back the date, when internal status needs next actions earliest
x   x	initialize							- (not implemented) setup from addressbook in csv or something like that

		incoming_post_send					- not needed anymore
x		inbox_move_store					- tbd. mail tasks taken by caller: just do the store updates (xml)
    x	inbox_move							- mail tasks taken by "KeyExchange": will send emails to those who exchange and where keys are available
x   x	inbox_conf
x   x	card_create
x   x	card_conf_xs
x   x	profile_create
x   x	profile_configure_xs
x   x	profile_update_by_invitation
x   x	getSetupScreenDataFromWebService
x   x	card_link_all

x   x	Callback */

public class KeySyncer {

	private static 			KeySyncer						instance					= null;
	public	static 			Profile								profile						= null;
	public	static 			Store_xml							localstore						= null;
	public	static 			Store_xml							inbox_store						= null;
			static			Scanner								scan						= null;
			public	static	HashSet<HashMap<String, String>>	feedback					= null;
			static final	String								default_id_location			= "local";
			static final	String								default_id_solution			= "xml";
	public	static final	String								default_location_solution	= default_id_location + "/" + default_id_solution;

	private KeySyncer () {
		localstore	= IO.getStoreInstance("identities.xml", default_location_solution);
	}

	public static KeySyncer getInstance() {
		if (instance == null) {
			instance	= new KeySyncer();
			profile		= new Profile();
			profile.setup_addressbook();
			inbox_store = new Store_xml("inbox.xml");
		}
		return instance;
	}

	public static void main(String[] args) {

		String []	dev_args		= {"test"};
		String []	new_args;
		boolean		isDeveloping	= true; // with "false" this will simply pass all the args to CLI

		if ( (args.length == 0) && isDeveloping ) {
			new_args = dev_args;
		} else {
			new_args = args;
		}

		KeySyncer kx = getInstance();

		Log.mode_screen_allowed = true;		// true:	screen output 	/ false:	no screen output
		Log.v					= 5;		// 5:		screen output 	/ 4:		no screen output
		
		kx.run(new_args);

		/*if ( ( KeySyncer.feedback != null ) && ( KeySyncer.feedback.size() > 0 ) ) {
			final String[]							reserved_ordered_keys	= {"from", "body"};

			Iterator<HashMap<String, String>> i = KeySyncer.feedback.iterator();

			while ( i.hasNext() ) {

				HashMap<String, String> an_output_map = i.next();

				for ( int n = 0 ; n < reserved_ordered_keys.length ; n++ ) {
					String item = an_output_map.get(reserved_ordered_keys[n]);
					System.out.print(" | " + ((item == null)?"":item));
				}
				System.out.println(" | ");
			}
		} */

	}

	public void run(String[] args) {

//		Log.mode_screen_allowed 	= true;		// true:	screen output 	/ false:	no screen output
//		Log.v						= 4;		// 5:		screen output 	/ 4:		no screen output

		boolean write_after_simulate	= false;
		boolean simulate				= false; 
		boolean do_incoming				= true;

		if ( email.opens.keysync.cli.SimulateAndMonitor.init(this, write_after_simulate, simulate, args) ) {
			write_after_simulate 	= SimulateAndMonitor.write_after_simulate;
			simulate				= SimulateAndMonitor.simulate;
			args					= SimulateAndMonitor.args;
		}

		CLI.ArgResult arg_var;

		if ( (args.length > 0) && "test".equals(args[0]) ) {
			Test.test( (  (args.length > 1 ) && ( ! "".equals(args[1]) )  )?args[1]:"");
			do_incoming	= false;
			simulate	= false; // finish tests with write the xml if != null
		}
		
		else

		if (args.length > 0) { // first decide wether to go through command line arguments or without run the timer
			arg_var = CLI.getActionFromParameters(args);

			if (!arg_var.error_detected) {
				do_incoming = CLI.performActionFromParameters(arg_var);
			}
		} else {
			//CLI_IO.getInboxInstance(p.id, true);
			StandBy_email.profile_auto(this, profile.id, "no-mailer"); //... p.getMsgSetFromInboxAndAddressbook(null); // CLI_IO.incoming_result(p, true);
		}

		if (simulate) {
			do_incoming = SimulateAndMonitor.performSimulation(do_incoming, write_after_simulate, args);
		}
		
		if ( (localstore != null) && (!simulate || write_after_simulate) ) {
			if (do_incoming) {
				IO.sendMsg( getMsgSetFromInboxAndAddressbook(null, false), profile.id);
			}
			localstore.write(); // STEP 5 - timer - next action
			inbox_store.write(); // STEP 5 - timer - next action
			profile.ab.ab_store.write(); // STEP 5 - timer - next action
		}

		if ( (scan != null) && !simulate ) {

			scan.close();
		}

		feedback = Log.collected_output;

	}

	public HashSet<HashMap<String, String>> getMsgActions(HashSet<HashMap<String, String>> inbox_parameter) {

		HashSet<HashMap<String, String>> out = new HashSet<HashMap<String, String>>();

		HashSet<Transfer_msg> actions = getMsgSetFromInboxAndAddressbook(inbox_parameter, false /* a mailer not use the InboxInstance */);

		Iterator<Transfer_msg> i = actions.iterator();
			
		while ( i.hasNext() ) {
			Transfer_msg m = i.next();

			HashMap<String, String> item = new HashMap<String, String>();

			item.put("from", m.id_localkey_owner);
			item.put("to", m.id_foreignkey_owner);
			item.put("subject", "");
			item.put("body", Transfer_msg.getXmlBodyFromMeta(m));
		}

		return out;
	}

	public HashSet<Transfer_msg> getMsgSetFromInboxAndAddressbook(HashSet<HashMap<String, String>> inbox_input, boolean new_mails_out) {

		HashSet<Transfer_msg>	outgoing	= new HashSet<Transfer_msg>();
							profile.xs 	= new Xs(profile.id);

		if ( profile.xs.intends_exchange ) {
								profile.ab.purge();
			Inbox 			i 			=	new Inbox(inbox_input, new_mails_out);
			Inbox.Result	ir			=	i.getOpenActions();
							outgoing		=	profile.ab.getOpenActions(ir);
		}

		Log.log(5, "API msgFromInboxAndAddr    [profile.id, xs, count(outgoing)]: " + profile.id + ", " + profile.xs.string + ", " + outgoing.size());

		return outgoing;
	}

	// CLI uses either xml or email with the java mail package, however the caller can send himself,
	//			but it is mandatory to do the post_send!

	public void postSend(HashSet<HashMap<String, String>> msgs){

		HashSet<Store_msg> m_set = new HashSet<Store_msg>();

		Iterator <HashMap<String, String>> i = msgs.iterator();

		while ( i.hasNext() ) {
			Store_msg m = new Store_msg();

			HashMap<String, String>	msg = new HashMap<String, String>();

									msg = i.next();

			if ( Xs.manual.equals(i.next().get("xs_to_be_sent")) ) {
				Key	k 									= new Key();
							m.meta.key_set						= k;
							
							m.meta.key_set.value[0]	= "@";
							m.meta.key_set.value[1]	= "@";

							m.meta.proof_key			= "@";
							m.meta.xs_to_be_sent		= Xs.manual;

							m.id_localkey_owner	= msg.get("from");
							m.id_foreignkey_owner	= msg.get("to");
			}
			m_set.add(m);
		}

		incoming_post_send(m_set);
	}

	public void send(String to, String text) {

		HashSet<Transfer_msg> out = getMsgSetFromInboxAndAddressbook(null, false);
		Transfer_msg.merge(out, new Transfer_msg(profile.id, new Card(to, "card_name_indicator").id, text));

		IO.sendMsg(out, profile.id);
	}

	public void timer() { // main without "to" and "text"

	HashSet<Transfer_msg> out = getMsgSetFromInboxAndAddressbook(null, false);
	IO.sendMsg(out, profile.id);
	}

	public void initialize() {

	}

	void incoming_post_send(HashSet<Store_msg> m) {

		// you need to call this, in case incoming returned msg and these
		//		messages have been sent succesfully by the caller

		// mandatory post actions e.g. if an exchange status
		// change has been done and "updateXsToBeSent" needs to be updated

		// to be on the safe side: call it!

		Store_msg.post_send( /* Store_msg.addrbk2store( */ m /*  ) */ );
	}

	public String inbox_move(String old_profile_id, String new_profile_id, Inbox.Properties ips, String decision) {
		// check, if everybody who does "exchange" has got keys from me
		// 		if not and decision is "check", return "suggest_to_send_keys_first"
		// 		if not and decision is "use_old_account_if_possibe", first send everybody keys who can
		//				work with keys, then send from the new address/account includekey, new keys and
		//				explanation for those who are not automatic
		//		if not and decision is "do_not_use_old_account"
		return "";
	}

	public String profile_conf(String remove_profile_id, String profile_id_parameter, Inbox.Properties ips) {
		
		inbox_conf(ips, remove_profile_id);
		profile.switchItem(remove_profile_id, profile_id_parameter);

		// send automatic
		// send manual

		return "";
	}

	public void inbox_conf(Inbox.Properties ips, String id_parameter) {

		Inbox.setProp(ips, id_parameter);
		Log.log(5, "API inbox_conf                                  [profile.id]: " + id_parameter);
	}

	public void card_create(String profile_id, String card_name, String card_id, String card_phone) {

		profile.ab.addCard(profile_id, card_name, card_id, card_phone);

			Log.log(5, "API card_create                                 [profile.id, card.name]: " + profile_id + ", " + card_name);
		}

	public void card_conf_xs(String profile_id, String card_id, String xs_aspired) {

		profile.id = profile_id;
		Card	c	=	new Card(card_id);
		c.updateXs(xs_aspired);
		c.updateXsToBeSentByCardid(card_id, Xs.manual);

		Log.log(5, "API card_conf_xs           [profile.id, card.id, xs_aspired]: " + profile_id + ", " + card_id + ", " + xs_aspired);
	}

	public void card_create_invitation() {

	}

	public void profile_create(String name, String id, String phone) {
		profile.newItem(name, id, phone);
		profile.ab.addProfile(name, id, phone); // set_addressbook_newProfile(name, id, phone);
		Inbox.newItem(name, id, phone); // .set_inbox_newItem(name, id, phone);

		Log.log(5, "API profile_create [profile.id, profile.name, profile.phone]: " + id + ", " + name + ", " + phone);
	}

	public void profile_configure_xs(String profile_id, String xs_aspired) {

		profile.id			= profile_id;

		profile.xs 			= new Xs(profile.id);
		if ( Xs.any_automatic.contains(xs_aspired) && ( profile.xs.intends_no_automatic(profile.xs.string) ) ) {

			HashSet<String> card_ids = /* new Addressbook() */ profile.ab.getCardIds(Xs.any_automatic_path("exchange_status"));

			Iterator<String> it_card_id = card_ids.iterator();

			while ( it_card_id.hasNext() ) {
				String	card_id	= it_card_id.next();
				Card	c		= new Card(card_id);
				c.updateXsToBeSentByCardid(card_id, Xs.manual);
			}
		}
		profile.setXs(profile_id, xs_aspired);

		Log.log(5, "API profile_configure_xs  [profile.id, xs_is, xs_aspired]:    " + profile.id + ", " + profile.xs.string + ", " + xs_aspired);
	}

	public Callable<Boolean> profile_update_by_invitation(String profile_id_parameter, String card_name_parameter, KeySyncCallback cb) {

		Log.log(5, "API profile_update_by_invitation [profile.id, card.name]:     " + profile_id_parameter + ", " + card_name_parameter);

		return (new Invite_ServerComm.InvitationOutgoingToServerReceiveExpirydateTanc(

									profile_id_parameter,
									card_name_parameter,
									cb

									 											)
				);
	}

	public Callable<Integer> getSetupScreenDataFromWebService(String profile_phone_parameter, KeySyncCallback cb) {
		
		Log.log(5, "API getSetupScreenDataFromWebService [profile.id, card.name]: " + profile_phone_parameter);

		return (new Invite_ServerComm.GetSetupScreenDataFromWebService(profile_phone_parameter, cb));
	}

	public void card_link_all() {
		HashMap<String, Profile.Person> profiles = profile.getProfileItems();

		Iterator<String> it_profile = profiles.keySet().iterator();
		
		// go through all collected id attributes
		while (it_profile.hasNext()) {
			profile.id = it_profile.next();

			HashSet<String> card_ids = /* new Addressbook() */ profile.ab.getCardIds("");
			
			Iterator<String> all_identities = profiles.keySet().iterator();
			while (all_identities.hasNext()) {
				String add = all_identities.next();
				//		if the card already exists gotonext
				if ( !profile.id.equals(add) && !card_ids.contains(add)) {
					//			if not create the card
					profile.ab.addCard(profile.id,	profiles.get(add).name, profiles.get(add).id, profiles.get(add).phone);
				}
			}
		}
		Log.log(5, "API card_link_all");
	}

}
		
