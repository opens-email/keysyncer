/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
// for the case it is intended to exclude Simulate and Monitor from this package
//		remove this class (SimulateAndMonitor.java) completely consider also deleting
//		the following parts of the source code which refer to this class

// in KeySyncer.java - delete until closing "{":
//	if ( email.opens.keysync.cli.SimulateAndMonitor.init(this, write_after_simulate, simulate, args) ) {
//	if (simulate) {
// delete the import Simulate...

// in Test.java - delete until closing "{":
//	case KEY_EX_BASIC: {
//	case KEY_EX_INV_BASIC: {

// in IO.java - delete until closing "{":
//	if ( done_send ) {
// delete the import Simulate...

package email.opens.keysync.cli;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import email.opens.keysync.KeySyncer;
import email.opens.keysync.core.Transfer_msg;
import email.opens.keysync.core.Log;
import email.opens.keysync.io.IO;

public class SimulateAndMonitor {

	static			Scanner			scan						= null;

	static final	boolean			STOP_ON_STEPS				= false;	// "false": there will be no stop done
																			// "true" there will be saved to XML file and stopped as follows

	static final	int				STOP_AND_DO_NOT_WAIT		= 0;
	static final	int				STOP_AND_WAIT_5_SECONDS		= 1;
	static final	int				STOP_AND_WAIT_10_SECONDS	= 2;
	static final	int				STOP_AND_WAIT_15_SECONDS	= 3;
	static final	int				STOP_AND_WAIT_20_SECONDS	= 4;
	static final	int				STOP_AND_WAIT_KEYSTROKE		= 9;

	static final int 				STOP_BEHAVIOUR_SELECTED		= STOP_AND_WAIT_5_SECONDS;

	public static			String[]		args;
	public static			boolean			write_after_simulate;
	public static			boolean			simulate;

	public static			KeySyncer	k;

	public static boolean init(KeySyncer k_parameter, boolean write_after_simulate_parameter, boolean simulate_parameter, String[] args_parameter) {

		write_after_simulate	= write_after_simulate_parameter;
		simulate				= simulate_parameter;
		args					= args_parameter;
		k						= k_parameter;

		if ( ( args_parameter.length > 0 ) && ( ( args_parameter[0].equals("simulate") ) || ( args_parameter[0].equals("simulate_write") ) ) ) {
			if ( args_parameter[0].equals("simulate_write") ) {
				write_after_simulate = true;
			}
			String[] new_args = Arrays.copyOfRange(args_parameter, 1, args_parameter.length);
			args = new_args;
			simulate = true;
		}

		return true;
	}

	public static boolean performSimulation (boolean do_incoming, boolean write_after_simulate, String[] args) {

		String				active_profile_id	= KeySyncer.profile.id;
		HashSet<String>		profile_ids			= KeySyncer.profile.getProfileIds();

		boolean 			once_more			= true;
		boolean 			done_send			= false;
		HashSet<Transfer_msg> out;
		
		profile_ids.remove(active_profile_id); // if set: active_profile_id will always be treated first and taken
												// off the set to not do it twice

		while (once_more) {
			once_more = false;
			if (do_incoming) {
				KeySyncer.profile.id	= active_profile_id;
				out		 				= k.getMsgSetFromInboxAndAddressbook(null, false);
				done_send 				= IO.sendMsg(out, KeySyncer.profile.id);
			} else {
				do_incoming = true;
			}

			Iterator<String> it_ids = profile_ids.iterator();

			while (it_ids.hasNext()) {
				KeySyncer.profile.id = it_ids.next();
				out 					= k.getMsgSetFromInboxAndAddressbook(null, false);
				done_send 				= IO.sendMsg(out, KeySyncer.profile.id);
				if (done_send) once_more = true;
			}
		}

		if ( !done_send ) {
			consider_stop();
		}

		return do_incoming;

	}

	public static void consider_stop () {
		
		if (STOP_ON_STEPS) {
			KeySyncer.localstore.write(); // STEP 5 - timer - next action
			KeySyncer.inbox_store.write(); // STEP 5 - timer - next action
			KeySyncer.profile.ab.ab_store.write(); // STEP 5 - timer - next action
			if (STOP_BEHAVIOUR_SELECTED == STOP_AND_WAIT_KEYSTROKE) {
//progress bar				System.out.print("Press the Enter key to continue!");

				if ( scan == null ) {
					scan = new Scanner(System.in);
				}
				scan.nextLine();
			} else if (STOP_BEHAVIOUR_SELECTED != STOP_AND_DO_NOT_WAIT) {
//progress bar				System.out.print("Wait... ");

				int seconds = STOP_BEHAVIOUR_SELECTED * 5;
				try {
					for ( int n = seconds ; n > 0; n-- ) {
						if ( n < 10 ) {
//progress bar							System.out.println(0);
						}
//progress bar						System.out.print(n);
						TimeUnit.SECONDS.sleep(1);
					}
				}	catch (InterruptedException e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
					}
//progress bar				System.out.print("00\r");
			}

		}
		//return true;

	}

}
