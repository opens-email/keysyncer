/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
// for the case it is intended to exclude tests from this package
//		remove this class (Test.java) completely consider also deleting the following parts of the
//		source code which refer to this class

// in KeySyncer.java - delete until closing "{" and also delete the "else" which appears after:
//	if ( (args.length > 0) && "test".equals(args[0]) ) {

// in KeySyncer.java - in the method main replace all the lines from the beginning until the line before 'KeyExchanger kx = getInstance();' by
//	kx.run(args);

package email.opens.keysync.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.UIDFolder;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;

import email.opens.keysync.KeySyncCallback;
import email.opens.keysync.KeySyncer;
import email.opens.keysync.core.Log;
import email.opens.keysync.io.IO;
import email.opens.keysync.io.local.Store_xml;
import email.opens.keysync.io.net.Receive_email;
import email.opens.keysync.Private;
/* import email.opens.keysync.thomsel.Private; */

public class Test {

	/**
	 * 
	 * do a full test and display the result
	 * 		if net tests not possible, do the local tests only
	 * 
	 * finally the result is: 	- status, if net test are done, if yes show the details
	 * 							- status of the local tests
	 * 			local check if status has been transfered to other user with the use of a key
	 * 			local check, maybe add cases where something is done in a wrong way and does have the expected effect
	 * 			
	 * 			net check if exchange of keys works
	 * 			net check if profiles and the contacts in the addresbook have been created as expected
	 * 			net check if invitation works
	 * 
	 *  find a solution how to make these tests also available in gitlab
	 *  maybe find a way to let these test be done by gitlab automatically when checking in new code
	 *  adding function to change the account and also create a test for that
	 * 
	 * use of imaps instead of imap without s (is that encrypted imap?)
	 * turned doc to static in Store_xml.java: check this!!!
	 * 
	 * account change testcase:
	 * 1. tm3 startet mit seinem vorhanden festen, bekannten Konto
	 * 2. m lässt sich einladen und sie schreiben ein paar Nachrichten m->3 "Hallo, da bin ich.", 3->m "Schön.", 3->m "Kommst du am WE?.", m->3 "Ja, mache ich.", m->3 "Am Sa um 20h bin ich da."
	 * 3. m wechselt das Konto (irgendeins aus dem Einladungspool) zu tm4 (sendet mit einem Key mit einer neuen Absenderadresse an tm3, tm3 mailt in den nächsten Tagen aber gar nicht)
	 * 3a. im Hintergrund wird tm3 dies mitgeteilt, obwohl m ihm gar keine Textnachricht sendet
	 * 4. tm3 schreibt eine neue Nachricht (ohne ein Bewusstsein dafür, welche Adresse m überhaupt hat), und diese Nachricht wird an das tm4 Konto gesendet.
	 * 4a. Wochen später holt tm3's keysyncer die Mails ab, durch die E-Mail mit dem Key von m wird m's neue E-Mail Adresse tm4 eingetragen
	 * 4b tm3 sendet, ohne etwas beachten zu müssen die neue E-Mail an m, aber an seine neue Adresse tm4
	 * 5. m antwortet
	 * 
	 * 3a: 	m ruft die Funktion 'account change' mit der Ziel E-Mail Adresse 'tm4@chat.trink2.com' (und allen zugehörigen Daten, um auf das neue Konto zuzugreifen)
	 * 				auf. Das bewirkt folgendes: Er trägt in seiner identity die neue id 'tm4@chat.trink2.com' (u.s.w.) ein. An jede Person, mit der m Keys
	 * 				austauscht, also an jede id, die in einer Card mit dem xs 'automatic*' und an alle mit 'manual' sendet er die folgende E-Mail
	 * 		m verwendet einen Foreignkey, den er zuvor von tm3 erhalten hat und fügt ihm einen neuen eigenen Localkey hinzu, da tm3 ihm ja, wegen des gerade
	 * 				verbrauchten Keys einen neuen senden wird.
	 * 		Der Vorgang 4a kann tatsächlich viel später passieren, schickt m weitere Nachrichten, so kommen die mit der Absender E-Mail Adresse tm4 an
	 * 				Da tm3 aber die obige E-Mail mit den Keys (einen Foreign, damit die neue Abesenderadresse akzeptiert wird und einen Local, damit tm3 den
	 * 				gerade "verbrauchten" Key wieder erneuern kann) zuerst findet, wird tm3 auch die später eingehenden E-Mails korrekt zuordnen.
	 * 4a:	tm3 vertraut dem Inhalt der Nachricht, da er seinen zuvor an m gesendeten Localkey wiedererkennt und kann die Nachricht trotz einer unbekannten
	 * 				Abenseremailadresse m zuvordnen. Er speichert den erhaltenen Key als zusätzlichen Foreignkey bei m ab (einer mehr als die 3, die er hat ist
	 * 				ja in Ordnung) und löscht den gerade eraknnten und "verbrauchten" Localkey. Zusätzlich speichert tm3 die neue E-Mail Adresse von m, die
	 * 				tm4 lautet. tm3 stellt fest, dass er nur noch 2 Localkeys in der Card von m (jetzt mit E-Mail Adresse tm4) findet und sendet m (tm4...) einen
	 * 				neuen an die neue E-Mailadresse tm4. Damit hat er wieder 3 Localkeys, die er bei m platziert hat und "verbraucht" dafür einen der Foreignkeys
	 * 				von m (jetzt hat er nur noch 3, das ist der von m gewollte Wert, deshalb hat er den Key ja mitgesendet).
	 * 		m erkennt den angehängten Key von tm4 als einen seiner Localkeys und kann ihn tm3 zuordnen. Er löscht den "verbrauchten" Localkey (und hat jetzt wieder
	 * 				3 davon), weiter speichert er den erhaltenen Foreignkey von tm3 und hat nun auch 3 davon. Der zuvor verbrauchte ist somit wieder ausgeglichen.
	 * 
	 *  full test:
	 * 	1 - clear: identities.xml, DB table invitation, DB table health_info: for ID 0 set no_of_req_today to 0, DB table accounts2 all columns "terms" to 0
	 *  2 - set "chosen" to "CREATE_TM34"		and run KX with String []	dev_args		= {"test"};
	 *  3 - check if one account has been created by invitation and that it exchanges with tm4 and that tm3 and tm3 exchange with each other
	 *  4 - clear identities.ml
	 *  5 - set "chosen" to "KEY_EX_INV_BASIC" 	and run KX with String []	dev_args		= {"test"};
	 *  6 - check if...
	 *  7 - clear identities.xml
	 *  8 - set "chosen" to "KEY_EX_BASIC" 		and run KX with String []	dev_args		= {"test"};
	 *  9 - check if...
	 * 
	 * 	 * how to perform the >> CREATE_TM34_SETUP and CREATE_TM4_puts_TM3_automatic_listen_spread << tests together?
	 *			MAKE sure WHO is WHO (and run / use the java code from different folders):
	 * 
	 *			for tm3: clear identities.xml	(CREATE_TM34_SETUP)	(java KX)													java KX do=auto
	 *			for tm4: clear identities.xml	(CREATE_TM34_SETUP)	(java KX)	(CREATE_TM4_puts_TM3_automatic_listen_spread)	(java KX)
	 *
	 */

	final static int KEY_EX_BASIC									= 5;
	final static int KEY_EX_INV_BASIC								= 6;
	final static int CREATE_TM34									= 7;
	final static int CHANGE_ACCOUNT									= 8;

	final static int S1_TM3_SETUP									= 11;
	final static int S2_TM4_SETUP									= 12;
	final static int S3_MX3_SETUP									= 13;

 public static String check_an_emailaddress(String from, String from_pwd, String to, String to_pwd) {

		String	code 		= createValue();
		boolean check 		= false;
		long	uid_msg 	= -1;

		send_email(from, from, from_pwd, to, code, Private.smtp, Private.smtpp);
		for ( int n = 1; n <4 ; n++) {
			uid_msg = receive_email(to, to, to_pwd, from, Private.imap, (Private.imapp).equals("default")?"":Private.imapp, code);
			if ( uid_msg != -1 ) {
				check = true;
				n = 4;
				delete_email(to, to, to_pwd, Private.imap, (Private.imapp).equals("default")?"":Private.imapp, uid_msg);
			}
		}
		
		return check?"OK":"E-Mail exchange failed.";
	 }

	 public static String createValue() {
		
		String ret = "";
		
		for (int x = 0 ; x <= 6 ; x++) {
			String nxt = "";
			int nx = new Random().nextInt(16);
			if ( nx < 10 ) nxt = String.valueOf(nx);
			else {
				if ( nx == 10 ) nxt = "a";
				if ( nx == 11 ) nxt = "b";
				if ( nx == 12 ) nxt = "c";
				if ( nx == 13 ) nxt = "d";
				if ( nx == 14 ) nxt = "e";
				if ( nx == 15 ) nxt = "f";
			}
			ret = ret + nxt;
		}
		return ret;
	}
	
	private static String dumpPart(Part p) {

		String text = "";

		try {
			Object o = p.getContent();
			if (o instanceof String) {
				text = text + (String)o;
			} else if (o instanceof Multipart) {
				Multipart mp = (Multipart)o;
				int count = mp.getCount();
				for (int i = 0; i < count; i++)
				text = text + dumpPart(mp.getBodyPart(i));
			} else if (o instanceof Message) {
				text = text + dumpPart((Part)o);
			}
		} catch (IOException | MessagingException e) {
			Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
		}
		return text;
	}

	public static boolean delete_email(String to, String to_login, String to_pwd, String imap_server, String imap_port, long uid_msg) {

		boolean 	check 	= false;
		
		if ( imap_server != null && to_login != null && to_pwd != null ) {
			try {

				Properties	properties	= System.getProperties();
				Session		session		= Session.getInstance(properties, null);
				Store		store		= session.getStore("imap");
				store.connect(imap_server, to_login, to_pwd);
				Folder		folder		= store.getDefaultFolder();

				if (folder != null) {
					folder = folder.getFolder("INBOX");
					if ( folder.exists() && (folder instanceof UIDFolder) ) {
						folder.open(Folder.READ_WRITE);

						Message msg = ((UIDFolder)folder).getMessageByUID(uid_msg);
						msg.setFlags(new Flags(Flags.Flag.DELETED), true);
						check = true;
					}
				}
				folder.close(true);
				store.close();
			} catch (MessagingException e) {
				check = false;
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
		}

		return check;
	}

	public static long receive_email (String from, String log, String pwd, String to, String imap_server, String port, String code) {

		String 		login 	= from;
		String 		body 	= "";
		boolean 	check 	= false;
		long		uid		= -1;
		
		if ( imap_server != null && login != null && pwd != null ) {
			try {

				Properties	properties	= System.getProperties();
				properties.put("mail.imap.ssl.enable", "true");
				Session		session		= Session.getInstance(properties, null);
				Store		store		= session.getStore("imap");
				store.connect(imap_server, login, pwd);
				Folder		folder		= store.getDefaultFolder();

				if (folder != null) {
					folder = folder.getFolder("INBOX");
					if ( folder.exists() && (folder instanceof UIDFolder) ) {
						folder.open(Folder.READ_ONLY);

						Message[] messages = folder.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));

						Arrays.sort( messages, ( m1, m2 ) -> {
							try {
							return m2.getSentDate().compareTo( m1.getSentDate() );
							} catch ( MessagingException e ) {
							throw new RuntimeException( e );
							}
						} );

						boolean gotit = false;
						for (Message msg : messages) {
							if ( !gotit ) {
								body = dumpPart(msg);
								uid = ((UIDFolder)folder).getUID(msg);

								gotit = true;
							}
						}
					}
				}
		
				folder.close(false);
				store.close();
			} catch (MessagingException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
		}

		if (body.length() > 2) body = body.substring(0, body.length() - 2);
		check = (code.equals(body));
		if ( ! check ) uid = -1;

		return uid;
	 }

	 public static boolean send_email (String from, String log, String pwd, String to, String code, String smtp_host, String port) {

		Boolean success = true;
		try {

			String subject 		= "·";

			Properties 	props = new Properties();;

			props.put("mail.smtp.host",			smtp_host	); //SMTP Host
			props.put("mail.smtp.ssl.enable",	"true"		); // for SSL (see https://www.ionos.com/help/email/general-topics/settings-for-your-email-programs-imap-pop3/)
			props.put("mail.smtp.auth",			"true"		); //Enabling SMTP Authentication
			props.put("mail.smtp.port",			port		); //SMTP Port

			MimeMessage msg = new MimeMessage(Session.getInstance(props, null));

			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(									from				);
			msg.setRecipients(	Message.RecipientType.TO,	to					);
			msg.setSentDate(								new Date()			);
			msg.setSubject(									subject,	"UTF-8"	);
			msg.setText(									code,		"UTF-8"	);

			Transport.send(msg, log, pwd);
		} catch (Exception e) {
			Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			success = false;
		}

		return success;
	 }

	 public static String check_if_webapplication_responds(String url) {

		String page = getPageResult(url + "answer.php");

		return page; //"error - no message to feed back";
	 }

	public static String getPageResult(String url_parameter) {

		String ret = "";
		String msg = "";

		try {
			URL url = new URL(url_parameter);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

			String line;
			String searched = "";

			while ((line = br.readLine()) != null) {
				if (line.contains("body")) searched = line;
			}

			String body = "<body>";
			int body_start = searched.indexOf(body) + body.length() ;
			int body_end = searched.indexOf("</body>");

			ret = (body_end <= body_start)?"":searched.substring(body_start, body_end);

			if ( ( (body_start + body_end) < 400 ) || ( (body_start + body_end) > 600 ) ) {
				ret = Integer.toString(body_start) + ";" + Integer.toString(body_end) + ";" + ret + ";";
			}
		}	catch (IOException e) {
				msg = e.toString();// getStackTrace().toString();
			}
            
			if ( !"".equals(msg) ) {
				ret = ret + ";" + msg;
			}

            return ret;
	}

	public static boolean pre_check_email() {

		boolean email1 = false;
		boolean email2 = false;

		String feedback = "";

		System.out.print("Check email address " + Private.tm3_id + " - result:");

		feedback = check_an_emailaddress(Private.tm3_id, Private.tm3_pwd, Private.tm4_id, Private.tm4_pwd);
		if ( "OK".equals(feedback)) email1 = true;
		System.out.println(" --------- " + feedback);
		
		System.out.print("Check email address " + Private.tm4_id + " - result:");

		feedback = check_an_emailaddress(Private.tm4_id, Private.tm4_pwd, Private.tm3_id, Private.tm3_pwd);
		if ( "OK".equals(feedback)) email2 = true;
		System.out.println(" --------- " + feedback);

		return (email1 && email2);
	}

	public static boolean pre_check_webapplication() {

		String feedback = "";

		int check_attempt = 1;
		boolean values_accepted = false;
		feedback = check_if_webapplication_responds(Private.URL_PREFIX);
		if ( !"".equals(feedback) ) {
			System.out.println("Check webpage " + Private.URL_PREFIX + " (general availability)");
			System.out.println(" --------- " + feedback);
		}
		else {
			System.out.println("Webpage available");
			while (check_attempt <= 2) {
				feedback = getPageResult(Private.URL_PREFIX + "status.php?req=read");

				int start = feedback.indexOf("x") + 1;
				int end = feedback.indexOf(" ");
				String x = (end <= start)?"":feedback.substring(start, end);
				if ( !"".equals(x) ) {
					feedback = feedback.substring(end);
				}
				start = feedback.indexOf("y") + 1;
				end = start + feedback.substring(start).indexOf(" ");
				String y = (end <= start)?"":feedback.substring(start, end);
				if ( !"".equals(y) ) {
					feedback = feedback.substring(end);
				}
				start = feedback.indexOf("z") + 1;
				end = start + feedback.substring(start).indexOf(" ");
				String z = (end <= start)?"":feedback.substring(start, end);;

				int x_available = 9; int y_available = 50; int x_warn = 5; int y_warn = 37;
				int x_max = 7; int y_max = 40;
				int ix = 0;
				int iy = 0;
				int iz = 0;

				if ( !"".equals(x) ) {
					ix = Integer.valueOf(x);
				}

				if ( !"".equals(y) ) {
					iy = Integer.valueOf(y);
				}

				if ( !"".equals(z) ) {
					iz = Integer.valueOf(z);
				}

				if ( ix >= x_max ) {
					System.out.println("Out of " + x_available + " available email accounts only " + ( 1 + x_available - ix ) + " left. - Cleanup needed.");
				}

				if ( ( ix >= x_warn ) && ( ix < x_max ) ) {
					System.out.println("Out of " + x_available + " available email accounts only " + ( 1 + x_available - ix ) + " left.");
				}

				if (iy >= y_max ) {
					System.out.println("Out of " + y_available + " requests maximum only " + iy + " left. - Cleanup needed.");
				}

				if ( ( iy >= y_warn ) && ( iy < y_max ) ) {
					System.out.println("Out of " + y_available + " requests maximum only " + iy + " left.");
				}

				if ( iz > 0 ) {
					System.out.println("Invitations found, that is not allowed for tests. Cleanup needed.");
				}

				values_accepted = ( ( ix < x_max ) && ( iy < y_max ) && ( iz == 0 ) );
				check_attempt++;

				if ( values_accepted ) {
					check_attempt = 3;
					System.out.println("Webpage health OK.");
				}
				else if (check_attempt == 2) {
					String url_parameter = Private.URL_PREFIX + "status.php?req=cleanup";
					System.out.println("Clean up launched - " + getPageResult(url_parameter));
				}
			}
		}

		return ( values_accepted );
	}

	public static HashMap<String, String>  reset_identities() {

		HashMap<String, String>		nw		= new HashMap<String, String>();
		nw.put("id", "");
		nw.put("bo", "");
		nw.put("in", "");
		return reset_identities(nw);
	}

	public static HashMap<String, String> reset_identities(HashMap<String, String> filenames) {
		File 		f_id;
		File 		f_bo;
		File 		f_in;

		boolean		got_none = (filenames.get("id") + filenames.get("bo") + filenames.get("in") ).length() == 0;

		String 		default_id_fn = "identities.xml";
		String 		default_bo_fn = "book.xml";
		String 		default_in_fn = "inbox.xml";

		String 		move_to_id = default_id_fn;
		String 		move_to_bo = default_bo_fn;
		String 		move_to_in = default_in_fn;

		if ( got_none ) {
			move_to_id = move_to_id + "_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			move_to_bo = move_to_bo + "_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			move_to_in = move_to_in + "_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		} else {
			default_id_fn = filenames.get("id");
			default_bo_fn = filenames.get("bo");
			default_in_fn = filenames.get("in");
		}

		try {
			f_id = new File(default_id_fn);
			f_bo = new File(default_bo_fn);
			f_in = new File(default_in_fn);

			boolean rename_id = f_id.exists() && (f_id.length() > 20);
			boolean rename_bo = f_bo.exists() && (f_bo.length() > 20);
			boolean rename_in = f_in.exists() && (f_in.length() > 20);

			if ( rename_id && rename_bo && rename_in ) {
				KeySyncer.localstore = null;
				KeySyncer.profile.ab.ab_store = null;
				KeySyncer.inbox_store = null;

				File f_temp = new File( move_to_id );
				f_temp.delete();
				f_id.renameTo(f_temp);

				f_temp = new File( move_to_in );
				f_temp.delete();
				f_in.renameTo(f_temp);

				f_temp = new File( move_to_bo );
				f_temp.delete();
				f_bo.renameTo(f_temp);
			}
		} catch (Exception e) {
			Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
		}

		KeySyncer.localstore = new Store_xml(default_id_fn);
		KeySyncer.profile.ab.ab_store = new Store_xml(default_bo_fn);
		KeySyncer.inbox_store = new Store_xml(default_in_fn);

		HashMap<String, String>		move_to		= new HashMap<String, String>();

		move_to.put("id", move_to_id);
		move_to.put("bo", move_to_bo);
		move_to.put("in", move_to_in);

		return move_to;
	}

	public static HashSet<String[]> getSet(String attr) {

		HashSet<String[]> set = new HashSet<String[]>();
		String[] t = {"./attribute::" + attr, "STRING", attr};	set.add(t);

		return set;
	}
	
	public static boolean validate_KEY_EX_BASIC () {

		String path = "";

		boolean siz_eq = false;

		boolean val1 = false;
		boolean val2 = false;
		boolean val3 = false;
		String result_val1 = "";
		String result_val2 = "";
		String result_val3 = "";

		// *************************************************************************************
		// **  validation 1/4: keys received by 1st identitiy equal keys sent by 2nd identity **
		// *************************************************************************************
		HashSet<HashMap<String, Object>> ident_c_has_card_t_has_foreignkeys;
		HashSet<HashMap<String, Object>> ident_t_has_card_c_has_localkeys;

		path = "/identities/identity[@id='" + Private.c_id + "']/addressbook/card[@id='" + Private.t_id + "']/foreignkey";
		System.out.println(path);
		ident_c_has_card_t_has_foreignkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		path = "/identities/identity[@id='" + Private.t_id + "']/addressbook/card[@id='" + Private.c_id + "']/localkey";
		System.out.println(path);
		ident_t_has_card_c_has_localkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		siz_eq = ident_c_has_card_t_has_foreignkeys.size() == ident_t_has_card_c_has_localkeys.size();
		val1 = ( siz_eq && ( 1 < ident_c_has_card_t_has_foreignkeys.size() ) );
		result_val1 = "c_for=t_loc: result " + val1 + " c_size " + ident_c_has_card_t_has_foreignkeys.size() + "t_size" + ident_t_has_card_c_has_localkeys.size();

		// *************************************************************************************
		// **  validation 2/4: keys received by 2nd identitiy equal keys sent by 1st identity **
		// *************************************************************************************
		HashSet<HashMap<String, Object>> ident_t_has_card_c_has_foreignkeys;
		HashSet<HashMap<String, Object>> ident_c_has_card_t_has_localkeys;

		path = "/identities/identity[@id='" + Private.t_id + "']/addressbook/card[@id='" + Private.c_id + "']/foreignkey";
		ident_t_has_card_c_has_foreignkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		path = "/identities/identity[@id='" + Private.c_id + "']/addressbook/card[@id='" + Private.t_id + "']/localkey";
		ident_c_has_card_t_has_localkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		siz_eq = ident_t_has_card_c_has_foreignkeys.size() == ident_c_has_card_t_has_localkeys.size();
		val2 = ( siz_eq && ( 1 < ident_t_has_card_c_has_foreignkeys.size() ) );
		result_val2 = "t_for=c_loc: result " + val2 + " t_size " + ident_t_has_card_c_has_foreignkeys.size() + "c_size" + ident_c_has_card_t_has_localkeys.size();

		// ***************************************************************************************************************************************
		// **  validation 3/4: 3rd identitiy has no keys at all, no xs and m's card of identity 1 and 2 have no xs and no keys for 3rd identity **
		// ***************************************************************************************************************************************
		HashSet<HashMap<String, Object>> ident_m_any_card_has_foreignkeys;
		HashSet<HashMap<String, Object>> ident_m_any_card_has_localkeys;
		String ident_m_has_card_c_has_phone;
		String ident_m_has_card_t_has_phone;
		String ident_m_has_xs;

		String ident_c_has_card_m_has_phone;
		String ident_c_has_card_m_has_xs;
		String ident_t_has_card_m_has_phone;
		String ident_t_has_card_m_has_xs;
		HashSet<HashMap<String, Object>> ident_c_has_card_m_has_foreignkeys;
		HashSet<HashMap<String, Object>> ident_c_has_card_m_has_localkeys;
		HashSet<HashMap<String, Object>> ident_t_has_card_m_has_foreignkeys;
		HashSet<HashMap<String, Object>> ident_t_has_card_m_has_localkeys;

		// get values to clarify m
		path = "/identities/identity[@id='" + Private.m_id + "']/addressbook/card/foreignkey";
		System.out.println(path);
		ident_m_any_card_has_foreignkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		path = "/identities/identity[@id='" + Private.m_id + "']/addressbook/card/localkey";
		System.out.println(path);
		ident_m_any_card_has_localkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		path = "/identities/identity[@id='" + Private.m_id + "']/addressbook/card[@id='" + Private.t_id + "']/attribute::phone";
		ident_m_has_card_t_has_phone = KeySyncer.profile.ab.ab_store.readString(path);
		path = "/identities/identity[@id='" + Private.m_id + "']/addressbook/card[@id='" + Private.c_id + "']/attribute::phone";
		ident_m_has_card_c_has_phone = KeySyncer.profile.ab.ab_store.readString(path);
		
		path = "/identities/identity[@id='" + Private.m_id + "']/attribute::xs";
		ident_m_has_xs = KeySyncer.localstore.readString(path);

		// get values to clarify c and t
		path = "/identities/identity[@id='" + Private.c_id + "']/addressbook/card[@id='" + Private.m_id + "']/attribute::phone";
		ident_c_has_card_m_has_phone = KeySyncer.profile.ab.ab_store.readString(path);
		path = "/identities/identity[@id='" + Private.c_id + "']/addressbook/card[@id='" + Private.m_id + "']/attribute::xs";
		ident_c_has_card_m_has_xs = KeySyncer.profile.ab.ab_store.readString(path);
		path = "/identities/identity[@id='" + Private.t_id + "']/addressbook/card[@id='" + Private.m_id + "']/attribute::phone";
		ident_t_has_card_m_has_phone = KeySyncer.profile.ab.ab_store.readString(path);
		path = "/identities/identity[@id='" + Private.t_id + "']/addressbook/card[@id='" + Private.m_id + "']/attribute::xs";
		ident_t_has_card_m_has_xs = KeySyncer.profile.ab.ab_store.readString(path);

		path = "/identities/identity[@id='" + Private.c_id + "']/addressbook/card[@id='" + Private.m_id + "']/foreignkey";
		System.out.println(path);
		ident_c_has_card_m_has_foreignkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		path = "/identities/identity[@id='" + Private.c_id + "']/addressbook/card[@id='" + Private.m_id + "']/localkey";
		System.out.println(path);
		ident_c_has_card_m_has_localkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		path = "/identities/identity[@id='" + Private.t_id + "']/addressbook/card[@id='" + Private.m_id + "']/foreignkey";
		System.out.println(path);
		ident_t_has_card_m_has_foreignkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		path = "/identities/identity[@id='" + Private.t_id + "']/addressbook/card[@id='" + Private.m_id + "']/localkey";
		System.out.println(path);
		ident_t_has_card_m_has_localkeys = KeySyncer.profile.ab.ab_store.testReadNodeList(path, getSet("value"));

		// no foreigns, no locals, phone for 1, phone for 2, empty xs
		boolean val3_m = ( ( ident_m_any_card_has_foreignkeys.size() == 0 ) && ( ident_m_any_card_has_localkeys.size() == 0 )
			&&				( ident_m_has_card_c_has_phone.length() > 0 ) && ( ident_m_has_card_t_has_phone.length() > 0 )
			&&				( "".equals(ident_m_has_xs) ) );

		boolean val3_c = ( ( ident_c_has_card_m_has_phone.length() > 0 )
			&&				( ident_c_has_card_m_has_foreignkeys.size() == 0 ) && ( ident_c_has_card_m_has_localkeys.size() == 0 )
			&&				( "".equals(ident_c_has_card_m_has_xs) ) );
		
		boolean val3_t = ( ( ident_t_has_card_m_has_phone.length() > 0 )
			&&				( ident_t_has_card_m_has_foreignkeys.size() == 0 ) && ( ident_t_has_card_m_has_localkeys.size() == 0 )
			&&				( "".equals(ident_t_has_card_m_has_xs) ) );
		
	
		val3 = val3_m && val3_c && val3_t;
		result_val3 = "tc_!=m: result " + val2 + " tm_xs_empty / tm_phone " + "".equals(ident_t_has_card_m_has_xs) + "/" + ident_t_has_card_m_has_phone + 
						" cm_xs_empty / cm_phone " + "".equals(ident_c_has_card_m_has_xs)  + "/" + ident_c_has_card_m_has_phone + " m_no_keys: " + 
						( ( ident_t_has_card_m_has_foreignkeys.size() == 0 ) && ( ident_t_has_card_m_has_localkeys.size() == 0 ) );
		
		System.out.println(val1 + " : " + val2 +  " : " + val3 + " Validated status: " + (val1 && val2 && val3) );
		System.out.println(result_val1);
		System.out.println(result_val2);
		System.out.println(result_val3);
		
		return val1 && val2 && val3;
	}

public static void test(String testcase) {

	if ( "". equals(testcase) ) {
	HashMap<String, String> backup_filename = reset_identities();

	testcase(KEY_EX_BASIC);
	validate_KEY_EX_BASIC();

	if ( pre_check_webapplication() ) {

		System.out.println("Continue network related test...");

		reset_identities();
		testcase(KEY_EX_INV_BASIC);
		//verify the result!!
	
		if ( pre_check_email() ) {

			System.out.println("Continue email related test...");

			reset_identities();
			testcase(CREATE_TM34);
			//verify the result!!
		}

	}
	else System.out.println("Prerequisite failed for network related test...");

	reset_identities(backup_filename);
	} else {
		int choice = 0;

		switch (testcase) {
			case "setup1": choice = S1_TM3_SETUP;
			break;
			case "setup2": choice = S2_TM4_SETUP;
			break;
			case "setup3": choice = S3_MX3_SETUP;
			break;
			case "basic": choice = KEY_EX_BASIC;
			break;
			case "inv": choice = KEY_EX_INV_BASIC;
			break;
			case "mail": choice = CREATE_TM34;
			break;
			default:
				break;
		}

		HashMap<String, String> backup_filename = reset_identities();
//		reset_identities();
		testcase(choice);
//		validate_KEY_EX_BASIC();
//		reset_identities(backup_filename);

	}

	// give summary and conclustion: success or failed

	// testcase(S1_TM3_SETUP); // so war es zuletzt gespeichert


	}

	public static void testcase (int chosen) {

//		Log.mode_screen_allowed = false;

		String [][] arg_matrix = {{"a", "b"}, {"c", "d"}};
		switch (chosen) {
			case CREATE_TM34: {
				String[][] argm = { // *************** TM3 **************** account creation and adding TM4
{ "do=profile_create", "profile_name=MX3", "profile_id=a@bbb.com", "profile_phone=16" },
{ "do=card_create" , "profile_id=a@bbb.com", "card_name=TM3" , "card_id=" + Private.tm3_id, "card_phone=00"  },
{ "do=card_create" , "profile_id=a@bbb.com", "card_name=TM4" , "card_id=" + Private.tm4_id, "card_phone=09"  },
{ "do=profile_create", "profile_name=TM3", "profile_id=" + Private.tm3_id, "profile_phone=00" },
Private.inbox_tm3,
{ "do=profile_conf_xs", "profile_id=" + Private.tm3_id, "xs=automatic_listen_spread" },
{ "do=card_create" , "profile_id=" + Private.tm3_id, "card_name=TM4" , "card_id=" + Private.tm4_id, "card_phone=09"  },

 									// *************** TM4 **************** account creation and adding TM3
{ "do=profile_create", "profile_name=TM4", "profile_id=" + Private.tm4_id, "profile_phone=09" },
Private.inbox_tm4,
{ "do=profile_conf_xs", "profile_id=" + Private.tm4_id, "xs=automatic_listen_spread" },
{ "do=card_create" , "profile_id=" + Private.tm4_id, "card_name=TM3" , "card_id=" + Private.tm3_id, "card_phone=00"  },
{ "do=card_create" , "profile_id=" + Private.tm4_id, "card_name=MX3" , "card_id=a3@bbc.com", "card_phone=16"  },
									// ******************************* for added TM3: set xs to "automatic_listen_spread"
{ "do=card_conf_xs", "profile_id=" + Private.tm4_id, "card_id=" + Private.tm3_id, "xs=automatic_listen_spread" },
{ "do=card_inv_out", "profile_id=" + Private.tm4_id, "card_name=MX3" },
{ "do=profile_inv_in", "profile_phone=16" },

// warten auf erste mail: tm4 schickt an tm3 seine keys und kann keinen beilegen
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm3_id },

// warten auf zweite mail: tm3 schickt an tm4 seine keys und legt trykey bei
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm4_id },

{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=i_nvited_id" },

// warten auf dritte mail: tm4 schickt an tm3 seine 4 keys, denn er legt proofkey bei
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm3_id },

// warten auf vierte mail: tm3 schickt an tm4 seine 3 keys und legt proofkey bei (hat dann nur noch 3 anstatt der 4)
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm4_id },

{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=i_nvited_id" }

//{ "do=just_wait", "30", "" }

};
				arg_matrix = argm;
				break;
			}
			case CHANGE_ACCOUNT: {
				String[][] argm = { // *************** MX3 **************** account creation and adding TM3
{ "do=profile_create", "profile_name=MX3", "profile_id=a@bbb.com", "profile_phone=16" },
{ "do=card_create" , "profile_id=a@bbb.com", "card_name=TM3" , "card_id=" + Private.tm3_id, "card_phone=00"  },
 									// *************** TM3 **************** account creation and adding MX3
{ "do=profile_create", "profile_name=TM3", "profile_id=" + Private.tm3_id, "profile_phone=00" },
Private.inbox_tm3,
{ "do=profile_conf_xs", "profile_id=" + Private.tm3_id, "xs=automatic_listen_spread" },
{ "do=card_create" , "profile_id=" + Private.tm3_id, "card_name=MX3" , "card_id=a3@bbc.com", "card_phone=16"  },
 									// *************** TM3 **************** invite MX3 to get a new email address
{ "do=card_inv_out", "profile_id=" + Private.tm4_id, "card_name=MX3" },

 									// *************** MX3 **************** with invitation get email address, get in touch with TM3
{ "do=profile_inv_in", "profile_phone=16" },

// MX3 verwendet ein anderes Konto, gibt seine Kontoinformation an und unterrichtet "alle" (die entsprechend xs haben) über die neue E-Mail Adresse
{ "do=change_account", "remove_profile_id=a@bbb.com", "profile_id=" + Private.tm4_id }, Private.inbox_tm4,

// warten auf erste mail: tm4 schickt an tm3 seine keys und kann keinen beilegen
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm3_id },

// warten auf zweite mail: tm3 schickt an tm4 seine keys und legt trykey bei
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm4_id },

{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=i_nvited_id" },

// warten auf dritte mail: tm4 schickt an tm3 seine 4 keys, denn er legt proofkey bei
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm3_id },

// warten auf vierte mail: tm3 schickt an tm4 seine 3 keys und legt proofkey bei (hat dann nur noch 3 anstatt der 4)
{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=" + Private.tm4_id },

{ "do=wait_until_incoming", "30", "do=incoming", "profile_id=i_nvited_id" }

//{ "do=just_wait", "30", "" }

};
				arg_matrix = argm;
				break;
			}
			case KEY_EX_BASIC: {
				String[][] argm = {
		{ "simulate" , "do=profile_create", "profile_name=Claas", "profile_id=" + Private.c_id, "profile_phone=00" },
		{ "simulate" , "do=profile_create", "profile_name=Thomas", "profile_id=" + Private.t_id, "profile_phone=01" },
		{ "simulate" , "do=profile_create", "profile_name=Maike", "profile_id=" + Private.m_id, "profile_phone=03" },
		{ "simulate" , "do=profile_conf_xs", "profile_id=" + Private.c_id, "xs=automatic_listen_spread" },
		{ "simulate" , "do=profile_conf_xs", "profile_id=" + Private.t_id, "xs=automatic_listen_spread" },
		{ "simulate" , "do=card_link_all" },
		{ "simulate" , "do=card_conf_xs", "profile_id=" + Private.c_id, "card_id=" + Private.t_id, "xs=automatic_listen_spread" }
												};
				arg_matrix = argm;
				break;
			}
			case KEY_EX_INV_BASIC: {
				String[][] argm = {

		{ "simulate" , "do=profile_create", "profile_name=Claas", "profile_id=" + Private.c_id, "profile_phone=11" },
		{ "simulate" , "do=profile_create", "profile_name=Thomas", "profile_id=" + Private.t_id, "profile_phone=01" },
		{ "simulate" , "do=profile_create", "profile_name=Maike", "profile_id=" + Private.m_id, "profile_phone=03" },
		{ "simulate" , "do=profile_conf_xs", "profile_id=" + Private.c_id, "xs=automatic_listen_spread" },
		{ "simulate" , "do=profile_conf_xs", "profile_id=" + Private.t_id, "xs=automatic_listen_spread" },
		{ "simulate" , "do=card_link_all" },
		{ "simulate" , "do=card_conf_xs", "profile_id=" + Private.c_id, "card_id=" + Private.t_id, "xs=automatic_listen_spread" },
		{ "simulate" , "do=card_inv_out", "profile_id=" + Private.c_id, "card_name=Maike" },
		{ "simulate" , "do=profile_inv_in", "profile_phone=03" }
		
												};
				arg_matrix = argm;
				break;
			}
			case S1_TM3_SETUP: {
				String[][] argm = { // *************** TM3 **************** account creation and adding TM4 & MX3
{ "do=profile_create", "profile_name=TM3", "profile_id=" + Private.tm3_id, "profile_phone=00" },
Private.inbox_tm3,
{ "do=profile_conf_xs", "profile_id=" + Private.tm3_id, "xs=automatic_listen_spread" },
{ "do=card_create" , "profile_id=" + Private.tm3_id, "card_name=TM4" , "card_id=" + Private.tm4_id, "card_phone=09"  },
{ "do=card_create" , "profile_id=" + Private.tm3_id, "card_name=MX3" , "card_id=a3@bbc.com", "card_phone=16"  },

};
				arg_matrix = argm;
				break;
			}
			case S2_TM4_SETUP: {
				String[][] argm = { // *************** TM4 **************** account creation and adding TM3 & MX3
{ "do=profile_create", "profile_name=TM4", "profile_id=" + Private.tm4_id, "profile_phone=09" },
Private.inbox_tm4,
{ "do=profile_conf_xs", "profile_id=" + Private.tm4_id, "xs=automatic_listen_spread" },
{ "do=card_create" , "profile_id=" + Private.tm4_id, "card_name=TM3" , "card_id=" + Private.tm3_id, "card_phone=00"  },
{ "do=card_conf_xs", "profile_id=" + Private.tm4_id, "card_id=" + Private.tm3_id, "xs=automatic_listen_spread" },
{ "do=card_create" , "profile_id=" + Private.tm4_id, "card_name=MX3" , "card_id=a3@bbc.com", "card_phone=16"  },
{ "do=card_inv_out", "profile_id=" + Private.tm4_id, "card_name=MX3" }

};
				arg_matrix = argm;
				break;
			}
			case S3_MX3_SETUP: {
				String[][] argm = { // *************** MX3 **************** account creation and adding TM3 & TM4
{ "do=profile_create", "profile_name=MX3", "profile_id=a@bbb.com", "profile_phone=16" },
{ "do=card_create" , "profile_id=a@bbb.com", "card_name=TM3" , "card_id=" + Private.tm3_id, "card_phone=00"  },
{ "do=card_create" , "profile_id=a@bbb.com", "card_name=TM4" , "card_id=" + Private.tm4_id, "card_phone=09"  },
 									// *************** MX3 **************** picks invitation
{ "do=profile_inv_in", "profile_phone=16" }

};
				arg_matrix = argm;
				break;
			}
		}

		int		n_max			= arg_matrix.length;
		String	invited_number	= "";
		String	invited_id		= "";

		if ( (arg_matrix!= null) && (arg_matrix.length > 0) && (arg_matrix[0].length > 0) ) {
			for ( int n = 0; n < n_max; n++) {

				if ( arg_matrix[n][0].equals("do=wait_until_incoming") ) {
					int			seconds			= Integer.valueOf(arg_matrix[n][1]);
					String		id				= ( arg_matrix[n][3].split("=") )[1];

					String[]	arg_new			= Arrays.copyOfRange(arg_matrix[n], 2, arg_matrix[n].length);
								arg_matrix[n]	= arg_new;

					KeySyncCallback cb = new KeySyncCallback();

					IO.setInboxInstance(id, false);

					Receive_email.set_inbox_listener(id, cb);
					waiting_for_new_email( cb, seconds );
				}

				if ( arg_matrix[n][0].equals("do=just_wait") ) {
					int			seconds			= Integer.valueOf(arg_matrix[n][1]);

					String[]	arg_new			= Arrays.copyOfRange(arg_matrix[n], 2, arg_matrix[n].length);
								arg_matrix[n]	= arg_new;

					waiting(seconds);
				}

				if ( arg_matrix[n][0].equals("do=profile_inv_in") ) {
					String	inv_arg			= arg_matrix[n][1];
							invited_number	= inv_arg.substring(14, inv_arg.length());
				}

				if (  ( arg_matrix[n].length > 1 ) && (arg_matrix[n][1].equals("profile_id=i_nvited_id") )  ) {
					arg_matrix[n][1] = "profile_id=" + invited_id;
				}

				//KeySyncer.store.generateXMLOutputToScreen();
				KeySyncer.getInstance().run(arg_matrix[n]);

				if ( !"".equals(invited_number) ) {
					invited_id = KeySyncer.localstore.readString("/identities/identity[@phone='" + invited_number + "']/@" + "id");
					invited_number = "";
				}

			}
		}
	}

	public static void waiting_for_new_email (KeySyncCallback cb_parameter, int sec_parameter) {

		Log.l("Wait for new email in inbox up to... " + sec_parameter + " seconds:");

		try {
			for ( int n = sec_parameter ; ( (n > 0) && (!cb_parameter.done) ); n-- ) {
				if ( n < 10 )
				Log.l("0");
				Log.l(String.valueOf(n));
				TimeUnit.SECONDS.sleep(1);
				Log.l("\r");
			}
		}	catch (InterruptedException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
		}
		if ( Receive_email.executor != null ) {
			Receive_email.executor.shutdownNow();
		}

		if ( cb_parameter.folder!= null ) {
			try {
				cb_parameter.folder.close();
			} catch (MessagingException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			} catch (IllegalStateException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
		}
		
		Log.l("00\r");

	}

	public static void waiting (int sec_parameter) {

		Log.l("Waiting for " + sec_parameter + " seconds.");

		try {
			for ( int n = sec_parameter ; ( (n > 0)  ); n-- ) {
				if ( n < 10 )
				Log.l("0");
				Log.l(String.valueOf(n));
				TimeUnit.SECONDS.sleep(1);
				Log.l("\r");
			}
		}	catch (InterruptedException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
			Log.l("00\r");

	}

}
