/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.cli;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import email.opens.keysync.core.*;

import email.opens.keysync.io.IO;
import email.opens.keysync.io.net.StandBy_email;
import email.opens.keysync.KeySyncer;
import email.opens.keysync.KeySyncCallback;

	/*
CLI - Bugs:
* output needs to be nice in different situations, known issues: "|   |Inbox operation..."
* logging does not write into a file (or somewhere where I do not see it)
* CLI wildert in Profile.store.set_profile..., daraus soll ein Aufruf von Card werden
* the log is not yet written to a text file...
* quit using "q" or "r" sometimes works, but often not, "r" output look shitty
* vom ersten Parameter wird nur das erste Wort genommen, es soll bis vor "text=" gelesen werden:
	$ java -jar kex Thomas Mueller text=Hallo, du alte Saege!					=> Thomas Hallo, du alte Saege!
	$ java -jar kex Thomas Mueller Luedenscheidt text=Hallo, du alte Saege!		=> Thomas Hallo, du alte Saege!
* auch der Beginn mit "@", so dass der erste Parameter von do=send eine E-Mail Adresse ist, geht bestimmt nicht - prüfen


CLI - new features:
* API und CLI Kommando,um E-Mail Konto zu wechseln
* StandBy_email - for returning right after the first action for a mailer: treat addressbook change different to xml count
* StandBy_email - implement write
* StandBy_email - implement xml
* StandBy_email - implement waiting 15 / 20 / 30 minutes max for imap idle to restart it and get emails anyway
* CLI "inbox_move" mandatory (old_)id (identity, in der die inbox geändert wird), optional new_id (ändert dann
				die old_id, bisher nötig!!), props, decision (falls man nicht von allen exchange Schlüssel hat) 
* CLI "timer" optional id of profile, wenn ohne, wird der früheste Zeitpunkt zurückgegeben (aus Adressbuchaktionen)
				read/(auto)/inbox_move/ggf. weitere geben dieses Datum zurück

* StandBy_email - multiuser: 
	w-rite r-ead q-uit x-ml u-sers (list users with number so type, the (4) after "action" indicates the user number)
	 input || 18.03.21 - abc@cdef.abcdef.com - action (4)        | xmls |emails||users: 12 / sums:| xmls |emails
	-------++----------------------------------------------------+------+------++-----------------+------+------
* format for the current action including time max needed for the action, add >10k if number exceeds
* CLI Adressbuchimport aus xls oder csv


	e.g. java -jar kex Thomas Bring das erste Release raus!
	same: java -jar kex Thomas text=Bring das erste Release raus!
	or:		java -jar kex @tm3@chat.trink2.com Bring das erste Release raus!

	par=xxx, optional use ", 	if no "do=" parameter: "send" is assmumed, if the first parameter is not "profile...=" the first (and intended to be only profile in the xml) is taken
								if first parameter has "@" in it "id" is assumed, after that following loop will be followed until the end of all parameters:
								first field will be stored and all following fields will be assembled until a field with "=" (or the end) is detected,
										then if no field is found to put altogether in, the first value is put in the next default field
										then the whole thing will be put in the field found or the rest ist put together in the default field.
								the default fields are: card, name, text (only one up to now)
	*/

	public class CLI {

			static			Scanner					scan		= null;
	public	static	HashSet<HashMap<String, String>> feedback	= null;
			static	ExecutorService					executor	= null;
			static			boolean					title		= false;
			static			boolean					closing		= false;

	/*****************************************************************
	 * 
	 *    A R G U M E N T S   A N A L Y S E
	 * 
	 *****************************************************************/

	public static class ArgResult {

		public boolean error_detected;
		HashMap<String, String> parameters;
		HashMap<String, HashSet<String>> parameter_matrix;
		HashMap<String, String> assumed_pre;

		String[] nx1 = { "send", "card_name", "text" };
		String[] nx2 = { "profile_create", "profile_id", "profile_name", "profile_phone" };

		int nx1_is = 0;
		int n1l = nx1.length - 1;
		int nx2_is = 0;
		int n2l = nx2.length - 1;
	}

	private static ArgResult arg_result_init() {
		ArgResult ar = new ArgResult();

		String[][] arg_matrix = { { "none", }, // no parameter: timer
				{ "send", "card_name", "text" }, 
				{ "card_create", "card_id", "card_name", "card_phone" },
				{ "card_conf_xs", "card_id", "card_xs" }, 
				{ "card_inv_out", "profile_id", "card_name" },
				{ "profile_create", "profile_id", "profile_name", "profile_phone" },
				{ "profile_conf_xs", "profile_id", "profile_xs" }, // conf 2 dummies: is_active and is_primary_id: these are foreseen for multiple accounts
				{ "inbox_conf", "profile_id", "is_active", "id_location", "id_solution", "id_inbox", "login", "pwd", "smtp_server", "smtp_port", "imap_server", "imap_port", "parameter1_lastuid"}, 
				{ "profile_inv_in", "profile_phone" },
				{ "initialize", }, // no parameter
				{ "card_link_all", }, // no parameter
				{ "incoming" }, // no parameter
				{ "profile_auto", "profile_id", "mailer" }, // no parameter needed, can have: profile_id= (can later be 'all'), mailer= ('yes'/anything else), 
				{ "read" }, // no parameter needed, can have: profile_id=
				{ "timer" }, // no parameter needed
				{ "inbox_move" } // like inbox_conf, but will additionally send messages to notify all exchange cards
		};
		ar.parameter_matrix = new HashMap<String, HashSet<String>>();

		int max = arg_matrix.length;
		HashSet<String> dos = new HashSet<String>();

		for (int i = 0; i < max; i++) {
			HashSet<String> do_line = new HashSet<String>();
			int column_max = arg_matrix[i].length;
			for (int j = 1; j < column_max; j++) {
				do_line.add(arg_matrix[i][j]);
			}
			ar.parameter_matrix.put(arg_matrix[i][0], do_line);
			dos.add(arg_matrix[i][0]);
		}
		ar.parameter_matrix.put("do", new HashSet<String>(dos));

		ar.assumed_pre = new HashMap<String, String>();
		ar.assumed_pre.put("send", "card");

		return ar;
	}

	private static String containsInAnyKey(HashMap<String, HashSet<String>> matrix, HashMap<String, String> assumed, String value,
			String do_result) {

		Iterator<String> it = matrix.keySet().iterator();
		boolean does_contain = false;
		boolean continue_loop = true;
		String correct_value = value;

		while (it.hasNext() && continue_loop) {
			String key = it.next();
			HashSet<String> par_list = matrix.get(key);

			String key_pre;
			if (assumed.containsKey(key)) {
				key_pre = assumed.get(key) + "_";
			} else {
				key_pre = key.split("_")[0] + "_";
			}

			if ((par_list.contains(value)) || (par_list.contains(key_pre + value))) {
				does_contain = true;
				if (!(par_list.contains(value)) && key.equals(do_result)) {
					correct_value = key_pre + value;
					continue_loop = false;
				}
			}
		}

		if (does_contain && correct_value.equals(value)) {
			correct_value = "1";
		} else if (!does_contain) {
			correct_value = "0";
		}
		return correct_value;
	}

	private static class ArgItemResult {
		static final int FOUND = 1;
		static final int ATTRIBUTE = 2;
		static final int ERROR = 3;

		int category;
		String parameter;
		String value;
	}

	private static ArgItemResult analyzeArgItem(String arg_item, String[] rest, ArgResult a) {

		ArgItemResult o = new ArgItemResult();

		String[] split = arg_item.split("=");

		if (split.length == 2) {
			String left = split[0];
			String right = split[1];

			boolean matrixHasKey = false;
			String checkMatrixHasKey = containsInAnyKey(a.parameter_matrix, a.assumed_pre, left,
					a.parameters.get("do"));
			if (!checkMatrixHasKey.equals("0")) {
				matrixHasKey = true;
				if (!checkMatrixHasKey.equals("1")) {
					left = checkMatrixHasKey;
				}
			}

			if (!left.equals("do") && !matrixHasKey) {
				o.category = ArgItemResult.ERROR;
			} else {
				o.category = ArgItemResult.FOUND;
				o.parameter = left;
				o.value = right;
			}
		} else {
			o.category = ArgItemResult.ATTRIBUTE;
			o.parameter = "";
			o.value = arg_item;
		}
		return o;
	}

	private static class Rest {
		int items;
		String value;
	}

	private static Rest getValue(String value, String[] rest) {

		Rest rr = new Rest();
		String result_value = value;
		int m = 0;

		int n_max = rest.length;
		for (int n = 0; n < n_max; n++) {
			if (rest[n].contains("=")) {
				n = n_max;
			} else {
				result_value = result_value + " " + rest[n];
				m++;
			}
		}
		rr.items = m;
		rr.value = result_value;
		return rr;
	}

	private static String[] getRestField(int i, String[] field) {
		int index = 0;
		int n_max = field.length;

		String[] result_field = new String[n_max - i];

		for (int n = i; n < n_max; n++) {
			result_field[index++] = field[n];
		}

		return result_field;
	}

	private static boolean checkArgs(ArgResult a) {
		boolean error = a.error_detected;
		if (!error) {
			if (a.parameter_matrix.containsKey("do")) {
				HashSet<String> check_if_exist = a.parameter_matrix.get(a.parameters.get("do"));
				if (check_if_exist.size() > 0) {
					Iterator<String> it_check = check_if_exist.iterator();

					while (it_check.hasNext()) {
						String parameter = it_check.next();
						//System.out.print("Just checking: " + parameter + " / ");
						if ((!a.parameters.containsKey(parameter)) || (a.parameters.get(parameter).equals(""))) {
							error = true;
							//System.out.println("not found...");
						} else {
							//System.out.println("found: " + a.parameters.get(parameter));
						}
					}
				}
			} else {
				error = true;
			}
		}
		return error;
	}

	public static ArgResult getActionFromParameters(String[] a) {

		ArgResult ar = arg_result_init();
		ar.parameters = new HashMap<String, String>();
		ar.error_detected = false;
		ar.parameters.put("do", "send");

		int n_max = a.length;

		for (int n = 0; n < n_max; n++) {
			String arg_item = a[n];
			String empty[] = {};
			String rest[] = (n < n_max) ? getRestField(n + 1, a) : empty;

			ArgItemResult or = analyzeArgItem(arg_item, rest, ar);

			switch (or.category) {
				case ArgItemResult.FOUND: { // bsp.: do=send, do=profile_create, card_name=Thomas, name=Thomas,
											// profile_phone=491719876001
					Rest r = getValue(or.value, rest);
					ar.parameters.put(or.parameter, r.value);
					n = n + r.items;
					break;
				}
				case ArgItemResult.ATTRIBUTE: { // this

					// find the parameter, to put the the value (attribute), that was found wihout
					// the info, for which parameter it is

					String assumed_parameter = "";
					// 1. see, if it is for "send"
					if ((ar.parameters.get("do").equals(ar.nx1[0])) && (ar.nx1_is < ar.n1l)) {
						assumed_parameter = ar.nx1[++ar.nx1_is];
					}

					// 2. see, if it is for "profile_create"
					if ((ar.parameters.get("do").equals(ar.nx2[0])) && (ar.nx2_is < ar.n2l)) {
						assumed_parameter = ar.nx2[++ar.nx2_is];
					}

					if ((ar.nx1_is == ar.n1l) || (ar.nx2_is == ar.n2l)) {
						Rest r = getValue(or.value, rest);
						ar.parameters.put(assumed_parameter, r.value);
						n = n + r.items;
					} else {
						ar.parameters.put(assumed_parameter, or.value);
					}
					break;
				}
				case ArgItemResult.ERROR: { // e.g.: creste_card, nqme=Thomas (there is a "=", on the left there is
											// nothing that is found)
					n = n_max;
					ar.error_detected = true;
					break;
				}
				default: { // should not happen: either attribute or error if not found
					break;
				}
			}
		}

		ar.error_detected = checkArgs(ar);

		if (ar.error_detected) {
			Log.l("Given options are not correct. Usage: tbd.");
		}
		return ar;
	}

	/*****************************************************************
	 * 
	 *    A R G U M E N T S   P E R F O R M
	 * 
	 *****************************************************************/

	public static boolean performActionFromParameters(ArgResult a) {
		String active_profile_id = "";

		boolean do_incoming_after_this_action = false;

		KeySyncer k = KeySyncer.getInstance();

		switch (a.parameters.get("do")) {
			case "send": {
				if ((a.parameters.containsKey("id")) || (a.parameters.containsKey("profile_id"))) {
					active_profile_id = a.parameters.get("id");
					if ((active_profile_id == null) || (active_profile_id.equals(""))) {
						active_profile_id	= a.parameters.get("profile_id");
					}
					KeySyncer.profile.id				= active_profile_id; // ???what if there is only one id in the file???
				} else {
					active_profile_id = KeySyncer.profile.id;
				}
				// ??? will I have to check if there is also senseful content in "to" and "text"
				// ???
				String text = a.parameters.get("text");
				String to = a.parameters.get("card_name");

				HashSet<Transfer_msg> out = k.getMsgSetFromInboxAndAddressbook(null, true);
				Transfer_msg.merge(out, new Transfer_msg(KeySyncer.profile.id, new Card(to, "card_name_indicator").id, text));

				IO.sendMsg(out, active_profile_id);
				break;
			}
			case "profile_create": {
				k.profile_create(a.parameters.get("profile_name"), a.parameters.get("profile_id"),
						a.parameters.get("profile_phone"));
				break;
			}
			case "card_link_all": {
				k.card_link_all();
				break;
			}
			case "profile_conf_xs": {
				k.profile_configure_xs(a.parameters.get("profile_id"), a.parameters.get("profile_xs"));
				do_incoming_after_this_action = true;
				break;
			}
			case "card_create": {
				k.card_create(a.parameters.get("profile_id"), a.parameters.get("card_name"), a.parameters.get("card_id"), a.parameters.get("card_phone"));
				do_incoming_after_this_action = true;
				break;
			}
			case "card_conf_xs": {
				k.card_conf_xs(a.parameters.get("profile_id"), a.parameters.get("card_id"), a.parameters.get("card_xs"));
				do_incoming_after_this_action = true;
				break;
			}
			case "card_inv_out": {

				// So stelle ich mir das in Android vor:

				// Mit "+" kommt man auf die Liste der Kontakte, um a) eine neue Nachricht zu schreiben und 
				//		b) sind die Austauschkontakte gekennzeichnet (bspw. fette Schrift), tippt man jemand
				//				an, so sieht man jetzt den Chat, wenn es bereits einen gibt oder die Frage
				//				"Chat starten?". Neu kommt hier hinzu, dass immer eine Frage kommt, die
				//				dann zusätzlich die Option bietet, jemand einzuladen, tippt man "Einladen",
				//				dann wird der Aufruf unten ausgelöst (Mehfachauswahl kommt später...)
				//		Man kehrt erst in die Liste der Chats zurück wenn, das XML file geschrieben ist


				KeySyncCallback		cb	= new KeySyncCallback();
				Callable<Boolean>	c	= k.profile_update_by_invitation(a.parameters.get("profile_id"), a.parameters.get("card_name"), cb);

				ExecutorService executor = Executors.newSingleThreadExecutor();

				executor.execute(() -> {
					try {
						c.call();
					} catch (Exception e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
					}
				});
				executor.shutdown();

				try {
					while (!cb.done){ TimeUnit.MILLISECONDS.sleep(100); }
				}	catch (InterruptedException e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
					}
				break;
			}
			case "profile_inv_in": {

				KeySyncCallback		cb	= new KeySyncCallback();

				Callable<Integer>		c_setup	= k.getSetupScreenDataFromWebService(a.parameters.get("profile_phone"), cb);

				ExecutorService executor = Executors.newSingleThreadExecutor();
				executor.execute(() -> {
					try {
						c_setup.call();
					} catch (Exception e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
					}
				});
				executor.shutdown();

				try {
					while (!cb.done){ TimeUnit.MILLISECONDS.sleep(100); }
				}	catch (InterruptedException e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
				}

				String login				= cb.store.get("login");
				String passwd				= cb.store.get("passwd");

				KeySyncer.profile.id 				= login;
				active_profile_id 	= KeySyncer.profile.id;

				String inviter_email		= cb.store.get("inviter_email");
				String invited_phone		= cb.store.get("invited_phone");
				String invited_alt_email	= cb.store.get("invited_alt_email");
				String foreignkey	 		= cb.store.get("key");

				String inviter_number		= cb.store.get("inviter_number");

				// with invitation accepted: put your id and yourself to "automatic_listen_spread"
				KeySyncer.profile.setIdXsByPhone(invited_phone, KeySyncer.profile.id, Xs.auto_listen_spread); // .set_profile_ByPhoneToIdXs(invited_phone, KeySyncer.profile.id, Xs.auto_listen_spread);

				String test_inviter_1 = "11";
				//String test_inviter_2 = "t@trink2.com";

				if ( 		!test_inviter_1.equals(inviter_number)
						// && 	!test_inviter_2.equals(inviter_email)
																	) {
					Inbox.Properties 	ips = new Inbox.Properties();

					ips.str_is_active		= "yes";
					ips.is_active			= true;
					ips.id_location			= "net";
					ips.id_solution			= "email";
//					ips.id					= login;
//					ips.login				= login;
					ips.pwd					= passwd;
					ips.smtp_server			= "smtp.ionos.de";
					ips.smtp_port			= "465";
					ips.imap_server			= "imap.ionos.de";
					ips.imap_port			= "";
					ips.parameter1_lastuid	= 1;
	
					k.inbox_conf(ips, login);
				}

				Log.log(5, "    Class CLI: foreignkey: "		+ foreignkey);
				Log.log(5, "    Class CLI: inviter_email: "		+ inviter_email);
				Log.log(5, "    Class CLI: invited_phone: "		+ invited_phone);
				Log.log(5, "    Class CLI: invited_alt_email: "	+ invited_alt_email);

				// check the invitation string, if inviter's email seems correct (it is already in addressbook or decdide manually)
				//and decide to accept the invitation to get in touch
				Log.log(5, "    Class CLI: In Test Mode, yes is assumed -> Type if you agree to accept the invitation (yes/no)! ");

				//Scanner scan= new Scanner(System.in);
				String text = "yes"; //scan.nextLine();

				if ( (text.equals("yes")) || ( text.equals("ja")) ) {
					
					Log.log(5, "    Class CLI: invited as: " + KeySyncer.profile.id);
					Log.log(5, "    Class CLI: inviter_number: " + inviter_number);

					Card c = new Card (inviter_number, 0); // "0" is phone_number indicator

					// with invitation accepted: add inviter's email address (if not yet done) to his phone number's entry
					c.id	= inviter_email;

					// with invitation accepted: put the inviter to "automatic_listen_spread"

					// with invitation accepted: store the invitation's key
					// use the key use it to get in touch with the inviter, wait for his spread action hopefully or
					//		receive his "listen" xs
					c.setIdXsForeignkey(Xs.auto_listen_spread, foreignkey);
				}
				do_incoming_after_this_action = true;
				break;
			}
			case "inbox_conf": {

				Inbox.Properties 	ips = new Inbox.Properties();

				String profile_id		= a.parameters.get("profile_id");

				ips.str_is_active		= a.parameters.get("is_active");
				ips.is_active			= ("yes".equals(ips.str_is_active))?true:false;
				ips.id_location			= a.parameters.get("id_location");
				ips.id_solution			= a.parameters.get("id_solution");

				ips.id					= a.parameters.get("id_inbox");
				ips.id					= (ips.id.equals("default"))?"":ips.id;

				ips.login				= a.parameters.get("login");
				ips.login				= (ips.login.equals("default"))?"":ips.login;

				ips.pwd					= a.parameters.get("pwd");
				ips.pwd					= (ips.pwd.equals("default"))?"":ips.pwd;
				
				ips.smtp_server			= a.parameters.get("smtp_server");
				ips.smtp_server			= (ips.smtp_server.equals("default"))?"":ips.smtp_server;

				ips.smtp_port			= a.parameters.get("smtp_port");
				ips.smtp_port			= (ips.smtp_port.equals("default"))?"":ips.smtp_port;

				ips.imap_server			= a.parameters.get("imap_server");
				ips.imap_server			= (ips.imap_server.equals("default"))?"":ips.imap_server;

				ips.imap_port			= a.parameters.get("imap_port");
				ips.imap_port			= (ips.imap_port.equals("default"))?"":ips.imap_port;

				String lastuid			= a.parameters.get("parameter1_lastuid");
				long		lu			= 1;
				if ( ( lastuid != null ) && ( !"default".equals(lastuid) ) ) {
					try {
						lu = Long.parseLong(lastuid);
					} catch (NumberFormatException e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
					}
				}
				ips.parameter1_lastuid	= (lu > 1 )?lu:1;

				k.inbox_conf(ips, profile_id);
				break;
			}
			case "incoming": {

				KeySyncer.profile.id =  a.parameters.get("profile_id");

				do_incoming_after_this_action = true;
				break;
			}
			case "read": { // get new messages (text) and display write them to the "collected_output" table, for one user (maybe also: for for each user)

				boolean remember_allowed = Log.mode_screen_allowed; // save

				Log.mode_screen_allowed = false; // Log.MODE_TO_GOL;
				k.getMsgSetFromInboxAndAddressbook(null, true /* output will go to collected_output */); // if "r": show, if Space: stop

				Log.mode_screen_allowed = remember_allowed; // restore

				// KX will do the output finally.
				
				break;
			}
			case "profile_auto": {  // handles new messages: "KX-only ones" are processed, XML actions done are counted, new text messages are counted separately
							// if there have been actions a small table will be shown, if screen output is allowed, if not: no output
							// this is done for the "first" user in indentities.xml or for the given user id (maybe later alternatively: for all users / or with id "all" as parameter)
							// if "mailer=yes" is set, it will right away return a new text message and an addressbook manipulation (later, not yet) using "collected_output"
							// if used by CLI and screenoutput is allowed it will stop when pressing 'q' or show the collected messages by pressing "r", then continue
				if ((a.parameters.containsKey("id")) || (a.parameters.containsKey("profile_id"))) {
					active_profile_id = a.parameters.get("id");
					if ((active_profile_id == null) || (active_profile_id.equals(""))) {
						active_profile_id	= a.parameters.get("profile_id");
					}
					KeySyncer.profile.id				= active_profile_id; // ???what if there is only one id in the file???
				} else {
					active_profile_id = KeySyncer.profile.id;
				}

				StandBy_email.profile_auto(k, active_profile_id, a.parameters.get("mailer")); // can be: profile_id=all or empty, mailer=yes (or not)
				break;
			}
			case "timer": {
				k.timer();
				break;
			}
			case "inbox_move": {
				break;
			}
			case "initialize": {
				;
				break;
			}
		}
	return do_incoming_after_this_action;
	}

}
