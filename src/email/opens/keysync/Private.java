/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync;

public final class Private { // private data, which is needed, but with individual content, so define for yourself
    public	static final String		URL_PREFIX			= "https://domain.tld/path/to/webapplication";
	public static final String [] keys = { // keys for available users
		"none", "92a234a653e8", "8c2342dfd2ac","4cc6c648d244","eb2341f151d1","a5e234d2fb51"
	};

	public static final String tm3_id = "tm3@domain.tld";
	public static final String tm4_id = "tm4@domain.tld";

	public static final String tm3_pwd = "secret";
	public static final String tm4_pwd = "another";

	public static final String smtp = "smtp.ionos.de";
	public static final String smtpp = "465";
	
	public static final String imap = "imap.ionos.de";
	public static final String imapp = "default";

	public static final String t_id = "t@domain.tld";
	public static final String c_id = "c@domain.tld";
	public static final String m_id = "m@domain.tld";

	public static final String [] inbox_tm3 = { "do=inbox_conf", "profile_id=" + tm3_id, "is_active=yes", "id_location=net", "id_solution=email", "id_inbox=default", "login=default",
	"pwd=" + tm3_pwd, "smtp_server=" + smtp, "smtp_port=" + smtpp, "imap_server=" + imap, "imap_port=" + imapp, "parameter1_lastuid=default" };

	public static final String [] inbox_tm4 = { "do=inbox_conf", "profile_id=" + tm4_id, "is_active=yes", "id_location=net", "id_solution=email", "id_inbox=default", "login=default",
	"pwd=" + tm4_pwd, "smtp_server=" + smtp, "smtp_port=" + smtpp, "imap_server=" + imap, "imap_port=" + imapp, "parameter1_lastuid=default" };

}