/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.io.local;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import email.opens.keysync.core.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Store_xml { // the general link to persistent data

	static final int 	PATH			= 0;
	static final int 	TYPE			= 1;
	static final int 	DESCRIPTION		= 2;

	FileInputStream		file_input;
	public 				Document 			doc;
	XPath				xpath;
	XPathExpression		xpath_expr;
	
	private	static 			String 								fpath; // file path
	private					String 								fname; // file name
							File 								f;
			static final 	String 								default_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><identities></identities>";

	public Store_xml(String fn) {
		
		file_input = null;

		f = null;

		fpath = "";			// could also be oraganized as parameter
		fname = fn;

		if ( true )
		/*  case 1 out of 2: to include Android MOD let this line start with "//*", with "/*" Android is switched off
before deleting these 9 lines make sure this has been done: add to build.gradle in directory deltachat-android:
add in /android/packagingOptions as first line after the "{":
	pickFirst 'META-INF/LICENSE.txt' // picks the JavaMail license file
add in /repositories after "jcenter()" the following line:
	maven { url "https://maven.java.net/content/groups/public/" }
add in /dependencies after the last line starting with "implementation" the following 3 lines:
	// use whatever the current version is... 
	implementation 'com.sun.mail:android-mail:1.6.2'
	implementation 'com.sun.mail:android-activation:1.6.2' }
{
		  try {
        android.content.Context			context = org.thoughtcrime.securesms.WelcomeActivity.context;
        android.content.ContextWrapper	cw = new android.content.ContextWrapper(context);
        if ( !java.util.Arrays.asList( cw.fileList() ).contains(fname) ) {
          FileOutputStream temp = cw.openFileOutput(fname, android.content.Context.MODE_PRIVATE);
          temp.write(default_xml.getBytes());
          temp.close();
        }

        file_input = cw.openFileInput(fname);
      } catch (IOException e) {
		    Log.log(Log.VERBOSE_EXCEPTIONS,e.getStackTrace().toString());
      }
		}  else
			/* */ 
		{
			try {
				f = new File(fpath + fname);

			if (!f.exists() || (f.length() == 0)) {
				File g = new File(fpath + fname);
				FileOutputStream temp = new FileOutputStream(g);
				temp.write(default_xml.getBytes());
				temp.close();
			}

			file_input = new FileInputStream(f);
			}	catch (IOException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS,e.getStackTrace().toString());
			}
		}

		if ( file_input != null ) {
			DocumentBuilderFactory	factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder			builder;
			try {
				builder	= factory.newDocumentBuilder();
				doc		= builder.parse(file_input);
				file_input.close();
			}	catch (ParserConfigurationException | SAXException | IOException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS,e.getStackTrace().toString());
				}

			XPathFactory xPathfactory = XPathFactory.newInstance();
			xpath = xPathfactory.newXPath();
		}
	}

	FileOutputStream getFileOutputStream() {

		FileOutputStream fos = null;

		try {
			/*  case 2 out of 2: to include Android MOD let this line start with "//*", with "/*" Android is switched off
			if ( true )
			{
				android.content.Context			context = org.thoughtcrime.securesms.WelcomeActivity.context;
				android.content.ContextWrapper cw = new android.content.ContextWrapper(context);
				fos = cw.openFileOutput(fname, android.content.Context.MODE_PRIVATE);
			}  else /* */ 
				fos = new FileOutputStream(f);
		}	catch (FileNotFoundException e) {
			Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}

		return fos;
	}

	public String readString(String path_parameter) {

		Object eval = null;

		try {
			xpath_expr = xpath.compile(path_parameter);
			eval = xpath_expr.evaluate(doc, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			Log.log(Log.VERBOSE_EXCEPTIONS,e.getStackTrace().toString());
		}

		return (eval == null)?"":eval.toString();
	}

	public HashMap<String, Object> readNode(String path_parameter, Node node_parameter, HashSet<String[]> item_parameter) {

		HashMap<String, Object> map = new HashMap<String, Object>();

		try {
			if ( !path_parameter.equals("") ) {
				xpath_expr = xpath.compile(path_parameter);
				Node node_from_path = (Node) xpath_expr.evaluate(doc, XPathConstants.NODE);
				node_parameter = node_from_path;
			}

			Iterator<String[]> it = item_parameter.iterator();
			while (it.hasNext()) {
				String[] 		investigate	= it.next();
				XPathExpression xp 			= xpath.compile(investigate[PATH]);
				if (investigate[TYPE].equals("STRING")) {
					String n = (String) xp.evaluate(node_parameter, XPathConstants.STRING);
					String s = ( n == null )?"":n.toString();
					map.put(investigate[DESCRIPTION], s);
				}
				if (investigate[TYPE].equals("NODE")) {
					Node n = (Node) xp.evaluate(node_parameter, XPathConstants.NODE);
					map.put(investigate[DESCRIPTION], n);
				}
			}
		}	catch (XPathExpressionException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS,e.getStackTrace().toString());
			}

	return map;
	}

	public HashSet<HashMap<String, Object>> testReadNodeList(String path, HashSet<String[]> node_quest) {

		return readNodeList(path, doc, node_quest);
	}
	
	public HashSet<HashMap<String, Object>> readNodeList(String path, Node node_parameter, HashSet<String[]> node_quest) {

		HashSet<HashMap<String, Object>> node_list = new HashSet<HashMap<String, Object>>();
			
		NodeList nl = null;

		try {
			xpath_expr = xpath.compile(path);
			nl = (NodeList) xpath_expr.evaluate(node_parameter, XPathConstants.NODESET);
		}	catch (XPathExpressionException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS,e.getStackTrace().toString());
			}

		int n = (nl == null)?0:nl.getLength();
		
		for (int i = 0; i < n ; i++) {
			node_list.add(readNode("", nl.item(i), node_quest));
		}
		return node_list;
	}

	public void performXMLJob(String xslString, Vector<String[]> v) {
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Source xslt = new StreamSource(new StringReader(xslString));
		DOMResult docOutMsg = null;

		try {
        	Transformer transformer = tf.newTransformer(xslt);
			for (Enumeration<String[]> el=v.elements(); el.hasMoreElements(); ) {
				String [] p = el.nextElement();
				if (p[1]== null) {
					p[1] = "";
				}
				transformer.setParameter(p[0], p[1]);
			}
	
			DOMSource 	domSource = new DOMSource(doc);
						docOutMsg = new DOMResult();
			transformer.transform(domSource, docOutMsg);
		}	catch (TransformerException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
        
        
        doc = (Document) docOutMsg.getNode();
	}
	
	public void write() {
		try {
			Transformer trf = TransformerFactory.newInstance().newTransformer(new StreamSource(new StringReader(
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
				+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
				+ "\n"
				+ "    <xsl:template match=\"node()|@*\">\n"
				+ "        <xsl:copy>\n"
				+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
				+ "        </xsl:copy>\n"
				+ "    </xsl:template>\n"
				+ "\n"
				+ "</xsl:stylesheet>\n")));


			StringWriter stringWriter3 = new StringWriter();
			trf.setOutputProperty(OutputKeys.INDENT, "yes");
			trf.transform(new DOMSource(doc), new StreamResult(stringWriter3));
			
			FileOutputStream file_output = getFileOutputStream();
			file_output.write(stringWriter3.toString().replaceAll("(?m)^[ \t]*\r?\n", "").replaceAll("    <identity", "\r\n    <identity").getBytes());
			file_output.close();
		}	catch (TransformerFactoryConfigurationError | TransformerException | IOException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS,e.getStackTrace().toString());
			}
	}

}
