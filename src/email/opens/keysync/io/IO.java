/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.io;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import email.opens.keysync.KeySyncer;
import email.opens.keysync.core.Store_msg;
import email.opens.keysync.core.Transfer_msg;
import email.opens.keysync.core.Inbox;
import email.opens.keysync.io.local.Store_xml;
import email.opens.keysync.io.local.Receive_xml;
import email.opens.keysync.io.net.Receive_email;
import email.opens.keysync.io.net.Send_email;

import email.opens.keysync.cli.SimulateAndMonitor;

public final class IO {

    private static 	Store_xml	instance				= null;

	private static String		fn						= "";

            static 	Receive		inbox_instance			= null;
    private static 	String      inbox_loc_sol_instance	= "";

    private static 	Send  		send_instance			= null;
    private static 	String      send_loc_sol_instance	= "";


    public static Store_xml getStoreInstance (String fn_parameter, String location_solution) {

		fn = fn_parameter;
        if ( instance == null ) {
            if ( "local/xml".equals(location_solution) ) {
                instance = new Store_xml(fn);
            } /* if ( "net/imap_draft".equals(location_solution) ) {
                instance = new LinkStore_imap_draft();
            } */
        }

        return instance;
    }

						/* receiveMsg */
	public static void setInboxInstance(String profile_id_parameter, boolean new_mails_out) {

		String inbox_location_solution	= KeySyncer.default_location_solution;

		Inbox.updateProperties(profile_id_parameter);
		HashMap<String, Inbox.Properties>	prop_strings	= 	Inbox.properties_strings;

		if ( !prop_strings.keySet().isEmpty() ) {
			String	loc_sol_id				= prop_strings.keySet().iterator().next();
					inbox_location_solution	= prop_strings.get(loc_sol_id).id_location
												+ "/" + prop_strings.get(loc_sol_id).id_solution;
		}

        if ( ( inbox_instance == null ) || (!inbox_loc_sol_instance.equals(inbox_location_solution)) ) {
            if (    "local/xml".equals(inbox_location_solution) ) {
                inbox_instance = new Receive_xml();
                inbox_loc_sol_instance = inbox_location_solution;
            } if (  "net/email".equals(inbox_location_solution) ) {
                inbox_instance = new Receive_email();
                inbox_loc_sol_instance = inbox_location_solution;
            }
        }

		inbox_instance.set_inbox_from_IO(profile_id_parameter, new_mails_out);

	}

    public static Send getSendInstance (String fn_parameter, String send_location_solution) {

        if ( ( send_instance == null ) || (!send_loc_sol_instance.equals(send_location_solution)) ) {
            if (    "local/xml".equals(send_location_solution) ) {
                send_instance = new Transfer_msg(); // (Send) IO.getStoreInstance(fn_parameter, send_location_solution);
                send_loc_sol_instance = send_location_solution;
            } if (  "net/email".equals(send_location_solution) ) {
                send_instance = new Send_email();
                send_loc_sol_instance = send_location_solution;
            }
        }

		return send_instance;
    }

	public static void send_instance_update(String profile_id_parameter) {

		String outgoing_msg_location_solution	= KeySyncer.default_location_solution;

		Inbox.updateProperties(profile_id_parameter);
		HashMap<String, Inbox.Properties>	prop_strings	= 	Inbox.properties_strings;

		if ( !prop_strings.keySet().isEmpty() ) {
			String	loc_sol_id						= prop_strings.keySet().iterator().next();
					outgoing_msg_location_solution	= prop_strings.get(loc_sol_id).id_location
														+ "/" + prop_strings.get(loc_sol_id).id_solution;
		}

		getSendInstance(fn, outgoing_msg_location_solution);
	}

	static private void send_instance_use(HashSet<Transfer_msg> ms, String profile_id_parameter) {

		if (ms.size() > 0) {
			Iterator<Transfer_msg> it = ms.iterator();
			while (it.hasNext()) {
				Transfer_msg mg = it.next();

				String will_send_meta = "no";					// ist die lenth nicht IMMER 4?
				if ( 		(mg.meta.key_set != null)
						&& 	(mg.meta.key_set.value.length > 0)
						&& 	( !"".equals(mg.meta.key_set.value[0]))
					) {
					will_send_meta = "yes";
				}

				send_instance.set_msg_outgoing(mg, will_send_meta);
			}
		}
	}

	public static boolean sendMsg(HashSet<Transfer_msg> ms, String profile_id_parameter) {

		send_instance_update(profile_id_parameter);
		send_instance_use(ms, profile_id_parameter);
		Store_msg.post_send( Store_msg.addrbk2store(ms) );

		boolean done_send = ( ms.size() > 0 );

		if ( done_send ) {
			SimulateAndMonitor.consider_stop();
		}

		return done_send; // done_send = once_more
	}

}
