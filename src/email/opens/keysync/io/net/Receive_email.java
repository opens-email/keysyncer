/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
// for the case it is intended to exclude external IO
//		remove classes IO_Ext* completely and consider also deleting the following parts of the
//		source code which refer to this class

// in IO.java - delete until closing "{"
//	if (  "net/email".equals(inbox_location_solution) ) {
//	if (  "net/email".equals(send_location_solution) ) {
//	HashSet<IO_Ext_Inbox_email.TextMsgStruct>	msgs	= new HashSet<IO_Ext_Inbox_email.TextMsgStruct>();
//	public boolean putMsgs(HashSet<IO_Ext_Inbox_email.TextMsgStruct> msgs_parameter) {

package email.opens.keysync.io.net;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;

import com.sun.mail.imap.IMAPFolder;

import email.opens.keysync.io.Receive;
import email.opens.keysync.KeySyncCallback;
import email.opens.keysync.core.Store_msg;
import email.opens.keysync.core.Inbox;
import email.opens.keysync.core.Log;

public final class Receive_email implements Receive {

    public static			ExecutorService	executor	= null;

    public Receive_email() {
    }

    public void set_inbox_from_IO(String profile_id, boolean new_mails_out) {

        HashMap<String, Inbox.Properties> inbox_map = Inbox.properties_strings;
        String key = "net/email@" + profile_id;
        Inbox.Properties props = inbox_map.get(key);

        if (props != null) {

            KeySyncCallback	cb	        = new KeySyncCallback();
            Callable<Boolean>       c_email_in  = new EmailIn( props.imap_server,
                                                        props.login, props.pwd,
                                                        props.parameter1_lastuid,
                                                        cb);

            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                try {
                    c_email_in.call();
                } catch (Exception e) {
                    Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
                }
            });

            executor.shutdown();

            int count_up = 0;

            try {
                while (!cb.done){
                    Log.l(Log.pre2);
                    if ( (count_up) < 10 )
                    Log.l("0");
                        Log.l(count_up + "");
                    TimeUnit.SECONDS.sleep(1);

                    count_up ++;
                }
            }	catch (InterruptedException e) {
                    Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
            }
            Log.l(Log.pre2 + "00");

            if ( new_mails_out ) {

                Iterator<Store_msg.TextMsgStruct> i = cb.msgs.iterator();

                while (i.hasNext()) {
                    Store_msg.TextMsgStruct item = i.next();
                    if ( item.body.length() > 0 ) {
                        Log.l("from", item.from);
                        Log.l("to", item.to);
                        Log.l("subject", item.subject);
                        Log.l("body", item.body);
                        Log.l("New E-Mail received! (" + String.valueOf(Log.line_number_msg) + ") new - Done.",
                                    Log.MODE_TO_MAILER);
                    }
                }

            }

            String lastuid = cb.get("lastuid");

            if ( !"".equals(lastuid) ) {
                Inbox.setLastUIDById(props.login, "net", "email", lastuid);
            }

        }
	}

    static class MsgStruct {

        HashSet<Store_msg>  mg;
        long                next_time_highest;
        long                last_time_hightest;
    }

    static class EmailIn implements Callable<Boolean> {

        String                  imap_server         = null;
        String                  login               = null;
        String                  pwd                 = null;
        long                    last_time_hightest  = 1;
        KeySyncCallback   cb;

        public EmailIn (String imap_server_parameter, String login_parameter, String pwd_parameter, long last_time_hightest_parameter, KeySyncCallback cb_parameter) {

            imap_server         = imap_server_parameter;
            login               = login_parameter;
            pwd                 = pwd_parameter;
            cb                  = cb_parameter;
            last_time_hightest  = last_time_hightest_parameter;
        }

        @Override
        public Boolean call() {

            Boolean success = false;

            if ( imap_server != null && login != null && pwd != null ) {
                try {

                    Properties	properties	= System.getProperties();
                    properties.put("mail.imap.ssl.enable", "true");
                    Session		session		= Session.getInstance(properties, null);
                    Store		store		= session.getStore("imap");
                    Log.l(Log.pre + "Inbox operation...                        (1/3)");
                    store.connect(imap_server, login, pwd);
                    Folder		folder		= store.getDefaultFolder();

                    if (folder != null) {
                        folder = folder.getFolder("INBOX");
                        if ( folder.exists() && (folder instanceof UIDFolder) ) {
                    
                            folder.open(Folder.READ_ONLY);
                    
                            if (folder.getMessageCount() > 0) {

                                MsgStruct   mstruc                      = new MsgStruct();
                                            mstruc.mg                   = new HashSet<Store_msg>();
                                            mstruc.next_time_highest    = last_time_hightest;
                                            mstruc.last_time_hightest   = last_time_hightest;

                                HashSet<Store_msg.TextMsgStruct> textMsg = new HashSet<Store_msg.TextMsgStruct>();

                                success = true;

                                Log.l(Log.pre + "Inbox operation...                        (2/3)");
                                msg_read_loop(mstruc, textMsg, folder, login);
                    
                                if ( mstruc.mg.size() > 0 ) {
                                    
                                    Store_msg.msg_store_loop( mstruc.mg );
                                    Store_msg.post_send( mstruc.mg, false /* log */
                                    ); // will not be logged as it is not sending, but only putting it in xml
                                }
                                Log.l(Log.pre + "Inbox operation...                        (3/3)");
                                TimeUnit.SECONDS.sleep(1);

                                String lastuid = "";
                                if ( mstruc.next_time_highest > last_time_hightest ) {
                                    lastuid = String.valueOf( mstruc.next_time_highest );
                                }
                                cb.put("lastuid", lastuid );
                                cb.putMsgs(textMsg);
                                cb.done = true;
                            }
                        }
                    }
                    folder.close(false);
                    store.close();
                } catch (Exception e) {
                    success = false;
                    Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
                }
            }
            return success;

        }

        private static String dumpPart(Part p) {

            String text = "";
    
            try {
                Object o = p.getContent();
                if (o instanceof String) {
                    text = text + (String)o;
                } else if (o instanceof Multipart) {
                    Multipart mp = (Multipart)o;
                    int count = mp.getCount();
                    for (int i = 0; i < count; i++)
                    text = text + dumpPart(mp.getBodyPart(i));
                } else if (o instanceof Message) {
                    text = text + dumpPart((Part)o);
                }
            } catch (IOException | MessagingException e) {
                Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
            }
            return text;
        }
    
        static void msg_read_loop (MsgStruct mstr, HashSet<Store_msg.TextMsgStruct> tx_msg, Folder folder_parameter, String profile_id_parameter) {

            int amount4 = 0;
            int amount  = 4;
            try {
                UIDFolder               ufolder   = (UIDFolder)folder_parameter;
                Message[]               msgs      = ufolder.getMessagesByUID(mstr.last_time_hightest + 1, UIDFolder.LASTUID);
        
                int ml      = msgs.length;
                int limit   = 25;
                amount4 = ml/4/10*9;
                if ( ml > limit ) {
                    Log.l("\r          Detected more than " + limit + " unknown e-mails: " + ml + " items altogether.\r");
                }

                int count_up = 0;

                boolean notext;
                for (int i = 0; i < ml; i++) {
                    //System.out.println("b");
        
                    if ( ++count_up > limit ){
                        count_up = 0;
                        Log.l("      " + i + "/" + ml + " items done.                                              \r");
                    }
                    //System.out.println("c" + i);
                    long uid = 0;
                        uid = ufolder.getUID(msgs[i]);
                    if ( uid > mstr.last_time_hightest ) {
                        if ( uid > mstr.next_time_highest ) {
                            mstr.next_time_highest = uid;
                        }

                        Store_msg.TextMsgStruct item = new Store_msg.TextMsgStruct();

                        item.from       = msgs[i].getFrom()[0].toString();
                        item.to         = profile_id_parameter;
                        item.subject    = msgs[i].getSubject();
                        item.body       = dumpPart(msgs[i]);

                        Store_msg m = Store_msg.getMsgFromTextMsg(item);
                        notext = "".equals(m.text);

                        if ( m != null ) {
                            mstr.mg.add(m);
                        }

                        if ( mstr.last_time_hightest > 1 ) {
                            if ( notext ) item.body = "";
                            tx_msg.add(item);
                        }
                    }
                }
            } catch (MessagingException e) {
                Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
            }

        }

    }

    public static void set_inbox_listener(String id_parameter, KeySyncCallback cb_parameter){

        HashMap<String, Inbox.Properties>	inbox_map	= Inbox.properties_strings;
        String								key			= "net/email@" + id_parameter;
        Inbox.Properties					props 		= inbox_map.get(key);

		executor = null;

        if (props != null) {

            Callable<Boolean>       c_email_idle    = new EmailIdle( props.imap_server,
                                                        props.login, props.pwd,
                                                        props.parameter1_lastuid,
                                                        cb_parameter);

            executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                try {
                    c_email_idle.call();
                } catch (Exception e) {
                    Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
                }
            });
        }
    }

    static class EmailIdle implements Callable<Boolean> {

        String                  imap_server         = null;
        String                  login               = null;
        String                  pwd                 = null;
        long                    last_time_hightest  = 1;
        KeySyncCallback  cb;

        public EmailIdle (String imap_server_parameter, String login_parameter, String pwd_parameter, long last_time_hightest_parameter, KeySyncCallback cb_parameter) {

            imap_server         = imap_server_parameter;
            login               = login_parameter;
            pwd                 = pwd_parameter;
            cb                  = cb_parameter;
            last_time_hightest  = last_time_hightest_parameter;
        }

        @Override
        public Boolean call() {

            Boolean success = false;

            if ( imap_server != null && login != null && pwd != null ) {
                try {
            
                    Properties	properties	= System.getProperties();
                    Session		session		= Session.getInstance(properties, null);
                    Store		store		= session.getStore("imap");
                                store.connect(imap_server, login, pwd);
                    Folder		folder		= store.getDefaultFolder();

                    if ( (folder != null) && (folder.exists()) ) {
                        folder = folder.getFolder("INBOX");
                        if ( folder.exists() ) {
                            folder.open(Folder.READ_ONLY);
                            if (folder instanceof IMAPFolder) {
                                cb.folder = folder;
                                ((IMAPFolder) folder).idle(true);
                                cb.put("email", "email");
                                cb.done = true;
                            }
                        }
                    }
                    folder.close(false);
                    store.close();
                } catch (Exception e) {
                    success = false;
                    Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
                }
            }
            return success;

        }        
    }
}

