/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
// for the case it is intended to make this service free from any account or invitation server and you intend to
//		remove this class (file Invite_ServerComm.java) and the class Invite_Callback (file Invite_Callback.java)
//		completely, consider also deleting the following parts of the source code which refer to this class

// in KeySyncer.java - delete until closing "{":
//	public Callable<Boolean> profile_update_by_invitation(String profile_id_parameter, String card_name_parameter, Invite_Callback cb) {
//	public Callable<Integer> getSetupScreenDataFromWebService(String profile_phone_parameter, Invite_Callback cb) {

// in CLI.java - delete or if applicable delete until closing "{":
//	
//	case "card_inv_out": {
//	case "profile_inv_in": {

// in CLI_Test.java
// remove ",{ "simulate" , "do=card_inv_out",...}"
// remove ",{ "simulate" , "do=profile_inv_in",...}"
// also delete the import for Invite...

// in Inbox.java - delete until closing "{":
//	case Msg.MetaCheck.INVITATION: {
// also delete the import for Invite...

// in Addressbook.java - delete from the beginning of the line until closing "{":
//			checkForCrippledHash
// also delete the import for Invite...

package email.opens.keysync.io.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import email.opens.keysync.core.Log;
import email.opens.keysync.core.Profile;
import email.opens.keysync.core.Key;
import email.opens.keysync.KeySyncer;
import email.opens.keysync.KeySyncCallback;
import email.opens.keysync.core.Card;
import email.opens.keysync.Private;
/* import email.opens.keysync.thomsel.Private; */

import java.util.concurrent.Callable;

public class Invite_ServerComm {

    public	static final String		URL_PREFIX			= Private.URL_PREFIX;
	private	static final char[]		hex					= { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    public	static final Charset	UTF_8				= StandardCharsets.UTF_8;
    private static final String		ENCRYPT_ALGO		= "AES/GCM/NoPadding";
    private static final int		TAG_LENGTH_BIT		= 128; // must be one of {128, 120, 112, 104, 96}
    private static final int		IV_LENGTH_BYTE		= 12;
    private static final int		SALT_LENGTH_BYTE	= 16;
	public static final String []	keys				= Private.keys;

	static String byteArray2Hex(byte[] bytes) {
	    StringBuffer sb = new StringBuffer(bytes.length * 2);
	    for(final byte b : bytes) {
	        sb.append(hex[(b & 0xF0) >> 4]);
	        sb.append(hex[b & 0x0F]);
	    }
	    return sb.toString();
	}

	public static String getStringFromSHA256(String stringToEncrypt) {

		byte [] b = {0,0};

		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.update(stringToEncrypt.getBytes());
			byte [] 	d = messageDigest.digest();
			byte []		c = {d[d.length-2],d[d.length-1]};
						b = c;
		}	catch (NoSuchAlgorithmException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
	    return byteArray2Hex(b);
	}
	
	public static String getPage(String url_parameter) {

		String ret = "";

		try {
			URL url = new URL(url_parameter);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

			String line;
			String searched = "";

			while ((line = br.readLine()) != null) {
				if (line.contains("body")) searched = line;
			}
			
			String body = "<body>";
			int body_start = searched.indexOf(body) + body.length() ;
			int body_end = searched.indexOf("</body>");

			ret = (body_end <= body_start)?"":searched.substring(body_start, body_end);
		}	catch (IOException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
            
            return ret;
	}
	
	public static boolean keyArrivalExceeded (String key_lead_time_date, int minutes_crit) {

		String [] lead_time_dates = key_lead_time_date.split(", ");
		
		LocalDateTime crit_picked_min = LocalDateTime.parse("1946-01-01T10:15:30");
		LocalDateTime server_arr_min = LocalDateTime.parse("2046-01-01T10:15:30");

		if ( lead_time_dates.length == 2 ) {
			String picked = lead_time_dates[0];		// "2020-11-17 14:24"
			String arrived = lead_time_dates[1];	// "2020-11-17 14:24"
			if ( picked.length() >= 16 ) {
				crit_picked_min = LocalDateTime.parse(
						"20" + 	picked.substring(2, 4) + "-" + 		// 2020-
								picked.substring(5, 7) + "-" + 		// 12-
								picked.substring(8, 10) + "T" + 		// 03T
								picked.substring(11, 13) + ":" + 	// 10:
								picked.substring(14, 16) + ":00").plusMinutes(minutes_crit); 	// 15
			}
			if ( arrived.length() >= 16 ) {
				server_arr_min = LocalDateTime.parse(
						"20" + 	arrived.substring(2, 4) + "-" + 		// 2020-
								arrived.substring(5, 7) + "-" + 		// 12-
								arrived.substring(8, 10) + "T" + 		// 03T
								arrived.substring(11, 13) + ":" + 	// 10:
								arrived.substring(14, 16) + ":00"); 	// 15
			}
		}
		
		return (
				( crit_picked_min.compareTo(server_arr_min		) 	< 0 ) || 
				( crit_picked_min.compareTo(LocalDateTime.now()	) 	< 0 ) 
			);
	}

    static byte[] getRandomNonce(int numBytes) {
        byte[] nonce = new byte[numBytes];
        new SecureRandom().nextBytes(nonce);
        return nonce;
    }
    
    // Password derived AES 256 bits secret key
    static SecretKey getAESKeyFromPassword(char[] password, byte[] salt) {

		SecretKey secret = null;
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			// iterationCount = 65536
			// keyLength = 256
			KeySpec spec = new PBEKeySpec(password, salt, 65536, 256);
			secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
		}	catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}

        return secret;
    }
    
    // return a base64 encoded AES encrypted text
    static String encrypt(byte[] pText, String password) {

		String ret = "";

        // 16 bytes salt
        byte[] salt = getRandomNonce(SALT_LENGTH_BYTE);

        // GCM recommended 12 bytes iv?
        byte[] iv = getRandomNonce(IV_LENGTH_BYTE);

        // secret key from password
        SecretKey aesKeyFromPassword = getAESKeyFromPassword(password.toCharArray(), salt);

		try {
        	Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);
			// ASE-GCM needs GCMParameterSpec
			cipher.init(Cipher.ENCRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
			byte[] cipherText = cipher.doFinal(pText);
			// prefix IV and Salt to cipher text
			byte [] cipherTextWithIvSalt = ByteBuffer.allocate(iv.length + salt.length + cipherText.length)
					.put(iv)
					.put(salt)
					.put(cipherText)
					.array();
	        // string representation, base64, send this string to others for decryption.
			ret = Base64.getUrlEncoder().encodeToString(cipherTextWithIvSalt);
		}	catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
					InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}

        return ret;
    }

    // we need the same password, salt and iv to decrypt it
    static String decrypt(String cText, String password) {

		String ret = "";
		byte[] decode = Base64.getUrlDecoder().decode(cText.getBytes(UTF_8));

        // get back the iv and salt from the cipher text
        ByteBuffer bb = ByteBuffer.wrap(decode);

        byte[] iv = new byte[IV_LENGTH_BYTE];
        bb.get(iv);

        byte[] salt = new byte[SALT_LENGTH_BYTE];
        bb.get(salt);

        byte[] cipherText = new byte[bb.remaining()];
        bb.get(cipherText);

        // get back the aes key from the same password and salt
        SecretKey aesKeyFromPassword = getAESKeyFromPassword(password.toCharArray(), salt);

		try {
			Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

			cipher.init(Cipher.DECRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

			byte[] plainText = cipher.doFinal(cipherText);

			ret = new String(plainText, UTF_8);
		}	catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
					InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}

		return ret;
    }

	public static class InvitationOutgoingToServerReceiveExpirydateTanc implements Callable<Boolean> {

		String							profile_id;
		String							card_name;
		KeySyncCallback					cb;

		public InvitationOutgoingToServerReceiveExpirydateTanc(String profile_id_parameter, String card_name_parameter, KeySyncCallback cb_parameter) {
			this.profile_id	= profile_id_parameter;
			this.card_name	= card_name_parameter;
			this.cb			= cb_parameter;
		}
	
		@Override
		public Boolean call() {

			HashMap<String, Profile.Person> items = KeySyncer.profile.getProfileItems();

			String	myNumber 			= items.get(profile_id).phone; // derive my phone number
			String	myid 				= items.get(profile_id).id; // derive my ID as email address

			Card	c					= new Card(card_name, "name_indicator");
					c.profile.id		= profile_id;

			String	to_id 				= c.id;
			String	to_phone			= c.getPhoneFromName(card_name);

			String	invitation_key 		= myid + "@" + new Key(c).createValue(); //locl_crd.createKey();
			String	invitation_string 	= invitation_key + "@" + myid + "@" + to_phone + "@" + to_id;
			String	myHash 				= getStringFromSHA256(myNumber);
			String	youHash 			= getStringFromSHA256(to_phone);
			String	PASSWORD 			= myNumber + to_phone; // encrypt string with both phone numbers
			//System.out.println(PASSWORD);
			
			String	enc					= encrypt(invitation_string.getBytes(UTF_8), PASSWORD);
			
			String	invitation_page		= URL_PREFIX + "answer.php?req=invite&hash1="
											+ myHash + "&hash2=" + youHash +
											"&email=" + myid + "&invitation=" + enc; 
			
			if (invitation_page.length() < 1972) {
				String expiry_date_tanc = getPage(invitation_page);
				
				String [] xd_tanc = expiry_date_tanc.split(", ");
				String expiry_date	= xd_tanc[0];
				String tanc			= xd_tanc[1];
				
				c.addInvitation(to_phone, invitation_key, expiry_date, tanc);
			}

			cb.completed();

			return true;
		}
	}

	public static class GetSetupScreenDataFromWebService implements Callable<Integer> {

		String							inv_phone;
		//Profile						p;
		KeySyncCallback					cb;

		public GetSetupScreenDataFromWebService (String inv_phone_parameter, KeySyncCallback cb_parameter) {

			this.inv_phone	= inv_phone_parameter;
			this.cb			= cb_parameter;
		}

		public Integer call () {

			Integer store_count = 0;

			// ask for number
			String num = getPage(URL_PREFIX + "answer.php?req=number");
			String invitations = "";

			if ( !num.equals("") ) {
				int			number 					= Integer.valueOf(num);
				String		trustkey				= keys[number]; // derive from table using number
				String		myHash 					= getStringFromSHA256(inv_phone);
				
				// with trustkey (derived by number) and optional hash: ask for account / inviter_hash / tanb
				String		account_url				= "account&tana=" + trustkey + "&myhash=" + myHash;
				String		account_inviter_tanb	= getPage(URL_PREFIX + "answer.php?req=" + account_url);

				//System.out.println(URL_PREFIX + "answer.php?req=" + account_url + "  //  " + account_inviter_tanb);

				String [] 	login_pw_inviter_tanb	= account_inviter_tanb.split(", ");
				String		login					= "";
				String		passwd					= "";
				String		inviter_hash			= "";
				String		tanb					= "";
				
				boolean there_is_an_invitation = false;
				
				if ( (login_pw_inviter_tanb.length >= 2) && (login_pw_inviter_tanb[login_pw_inviter_tanb.length-1] != null) ) {
					login 				= login_pw_inviter_tanb[0								];
					passwd				= login_pw_inviter_tanb[1								];
					if ( login_pw_inviter_tanb.length >= 4 ) {
						inviter_hash 	= login_pw_inviter_tanb[login_pw_inviter_tanb.length-2	];
						tanb 			= login_pw_inviter_tanb[login_pw_inviter_tanb.length-1	];
						if ( (!inviter_hash.equals("")) && (!tanb.equals("")) ) there_is_an_invitation = true;
					}
				}
				
				// update profile attribute: id (which is the just received login)
				if ( !login.equals("") && login.contains("@") ) {

					cb.put("login",	login);
					cb.put("passwd",	passwd);
					store_count	= store_count + 2;

					if ( there_is_an_invitation ) { // for your crippled hash
						analyse_invitation(inv_phone, trustkey, tanb, invitations, inviter_hash);
						store_count = store_count + 5;
					}
				}
			}
			cb.completed();

			return store_count;
		}

		void analyse_invitation(String phone_parameter, String trustkey, String tanb, String invitations, String inviter_hash) {

			// with tana_tanb ask for invitation_string (server will inform inviter if he gave his email address (which is optional))
			String			tana_tanb 				= trustkey + tanb;
							invitations				= getPage(URL_PREFIX + "answer.php?req=invitations&tana_tanb=" + tana_tanb);

			// go through all cards as=ias and check hash of phone with inviter_hash: locl_crd.read(a, ias, "name", inv, "/@id", "nothing_special");
			Profile			p						= KeySyncer.profile;
							p.id					= p.getIdFromPhone(phone_parameter);
			HashSet<String> possible_phone_numbers 	= p.ab.checkForCrippledHash(inviter_hash);

			String			inviter_number			= "";
			int				count_fail				= 0;
			int				count_success			= 0;
			String			invitations_decrypt 	= "";

			Iterator<String> it_possible = possible_phone_numbers.iterator();

			while (it_possible.hasNext()) { // do decrypting with every possible phone number (hopefuly only one) which fits to the inviter_hash 
				try {
					String try_phone = it_possible.next();
					invitations_decrypt = decrypt(invitations, try_phone + inv_phone);
					//System.out.println("try: " + try_phone + inv_phone);
					count_success ++;
					inviter_number = try_phone;
				}
				catch (Exception e) { count_fail ++; }
			}

			cb.put("inviter_number", inviter_number);

			//System.out.println("success // fail // invitations_decrypt: " + count_success + " // " + count_fail + " // " + invitations_decrypt);
			
			String [] getInfos = invitations_decrypt.split("@");
			
			String foreignkey = "";
			String inviter_email = "";
			String invited_phone = "";
			String invited_alt_email = "";
			
			if ( getInfos.length >= 6 ) { // 1+2+3: key, 4+5: inviter email, 6: invited phone, 7+8: invited alternative email 
				if ( getInfos.length >= 8 ) {
					invited_alt_email	= getInfos[6] + "@" + getInfos[7];
				}
				foreignkey				=  getInfos[0] + "@" + getInfos[1] + "@" + getInfos[2];
				inviter_email			=  getInfos[3] + "@" + getInfos[4];
				invited_phone			=  getInfos[5];
			}

			cb.put("inviter_email",		inviter_email);
			cb.put("invited_phone",		invited_phone);
			cb.put("invited_alt_email",	invited_alt_email);
			
			cb.put("key",				foreignkey);
			cb.put("inviter_id",			inviter_email);
		}

	}

}
