/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
// for the case it is intended to exclude external IO
//		remove classes IO_Ext* completely and consider also deleting the following parts of the
//		source code which refer to this class

// in IO.java - delete until closing "{"
//	if (  "net/email".equals(inbox_location_solution) ) {
//	if (  "net/email".equals(send_location_solution) ) {
//	HashSet<IO_Ext_Inbox_email.TextMsgStruct>	msgs	= new HashSet<IO_Ext_Inbox_email.TextMsgStruct>();
//	public boolean putMsgs(HashSet<IO_Ext_Inbox_email.TextMsgStruct> msgs_parameter) {

package email.opens.keysync.io.net;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import email.opens.keysync.io.Send;
import email.opens.keysync.core.Transfer_msg;
import email.opens.keysync.core.Inbox;
import email.opens.keysync.core.Log;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

public final class Send_email implements Send {

	public Send_email() {
	}

	public void set_msg_outgoing(Transfer_msg m, String will_send_meta_parameter) {

		Inbox.Properties					props		= Inbox.properties_strings.get("net/email@" + m.id_localkey_owner);

		if ( ( props != null ) && ( m != null ) && ( !"".equals(m.id_foreignkey_owner)) ) {
			String		body = "";
			if ( ( m.text != null ) && ( !"".equals(m.text) ) ) body = m.text;

			if ( "yes".equals(will_send_meta_parameter) ) {
				body 		= 	body +	Transfer_msg.pre_open +
										Transfer_msg.getXmlBodyFromMeta( (Transfer_msg) m) +
										Transfer_msg.pre_close;
			}

			Callable<Boolean> c_email_transport = new EmailTransport(	props.id, // sender
																		m.id_foreignkey_owner,
																		body,
																		props.smtp_server,
																		props.smtp_port,
																		props.login,
																		props.pwd);

			ExecutorService executor = Executors.newSingleThreadExecutor();
			executor.execute(() -> {
				try {
					c_email_transport.call();
				} catch (Exception e) {
					Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
				}
			});
			executor.shutdown();
		}

		/*try {
			while (!cb.done){ TimeUnit.MILLISECONDS.sleep(100); }
		}	catch (InterruptedException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
		}*/
															
	}

	static class EmailTransport implements Callable<Boolean> {

		String from;
		String to;
		String body;

		String svs; // server name of smtp server
		String pos; // port of smtp server

		String log;
		String pwd;


		EmailTransport (String from_p, String to_p, String body_p, String svs_p, String pos_p, String log_p, String pwd_p) {

			from	= from_p;
			to		= to_p;
			body	= body_p;
			svs		= svs_p;
			pos		= pos_p;
			log		= log_p;
			pwd		= pwd_p;
		}

		@Override
		public Boolean call() {

			Boolean success = true;
			try {

				String subject 		= "🔗"; subject 		= "🌐"; //	p.sSubject 		= "·openthe.net·";

				Properties 	props = new Properties();;

				props.put("mail.smtp.host",			svs		); //SMTP Host
				props.put("mail.smtp.ssl.enable",	"true"	); // for SSL (see https://www.ionos.com/help/email/general-topics/settings-for-your-email-programs-imap-pop3/)
				props.put("mail.smtp.auth",			"true"	); //Enabling SMTP Authentication
				props.put("mail.smtp.port",			pos		); //SMTP Port

				MimeMessage msg = new MimeMessage(Session.getInstance(props, null));
	
				msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
				msg.addHeader("format", "flowed");
				msg.addHeader("Content-Transfer-Encoding", "8bit");

				msg.setFrom(									from				);
				msg.setRecipients(	Message.RecipientType.TO,	to					);
				msg.setSentDate(								new Date()			);
				msg.setSubject(									subject,	"UTF-8"	);
				msg.setText(									body,		"UTF-8"	);

				/* System.out.println("-----------------");
				System.out.println("from:" + from);
				System.out.println("to  :" + to);
				System.out.println("body:" + body);
				System.out.println("-----------------"); */

				Transport.send(msg, log, pwd);
			} catch (Exception e) {
				//String ee = e.getMessage();// getStackTrace(). toString();
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
				//System.out.println(ee);
				success = false;
			}
	
			return success;
		}
	}

}

