/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.io.net;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import email.opens.keysync.core.Profile;
import email.opens.keysync.core.Log;

import email.opens.keysync.io.IO;
import email.opens.keysync.KeySyncCallback;
import email.opens.keysync.KeySyncer;

import javax.mail.MessagingException;

	/* 

	StandBy is not supposeed to work for local/xml, but shall not only be provided to CLI, but also to a mail application

	the mail application then will only be rendering the messages and exchange data with StandBy

	 */

	public class StandBy_email {

	public static final int IDLE_RESULT_NOTHING			= 0;
    public static final int IDLE_RESULT_QUIT			= 1;
    public static final int IDLE_RESULT_WRITE			= 2;
    public static final int IDLE_RESULT_READ			= 3;
    public static final int IDLE_RESULT_RETURN			= 4;
    public static final int IDLE_RESULT_XML				= 5;

	static			Profile					p			= null;
	static			Scanner					scan		= null;
	static	ExecutorService					executor	= null;
	static			boolean					title		= false;
	static			boolean					closing		= false;
	final	static	String dashes		=	"--------------------------------------------------------------------------------"
									+		"--------------------------------------------------------------------------------"; // 2 x 80
	final	static	String spaces		= 	"                                                                                "
									+		"                                                                                "; // 2 x 80
	final	static	String long_title 	= " type 'r' to read emails, 'w' to write en email, 'x' for xml or 'q' to quit ";
	final	static	String col3_mg		= "new emails";
	final	static	String col2_ad		= "xml updates";
	final	static	String col1_cu		= "current action for ";
			static	String col1_center;
	final	static	String col0_input	= "input";

			static	boolean done		= false;

			static	int 	long_length = long_title.length();
			static	int 	space_pre_count = long_length - 1 - 1 - 1 - col3_mg.length() - col2_ad.length() - col0_input.length();
			static	int 	space_count; // = space_pre_count - col1_center.length();
			static	int 	half;
			static	int 	otherhalf;
			static	String	col1_what;

			static	KeySyncCallback 	cb;

	public static void init(String profile_id) {

		title		= false;
		done		= false;
		col1_center	= col1_cu + profile_id;
		space_count = space_pre_count - col1_center.length();
		half		= (int)Math.floor( (space_count / 2) );
		half 		= ( half < 0 )?15:half;
		otherhalf	= space_count - half;
		col1_what	= spaces.substring(0, half) + col1_center + spaces.substring(0, otherhalf);
		cb			= new KeySyncCallback();
	}

	public static int getIdleResult (KeySyncer k_parameter, String profile_id, String mailer) {

		int						result				= IDLE_RESULT_NOTHING;

		int						mailcount_before	= Log.line_number_msg - 1;

		IO.sendMsg( k_parameter.getMsgSetFromInboxAndAddressbook(null, true /* output will go to collected_output */) , KeySyncer.profile.id); // if "r": show, if Space: stop
		consider_print_status();
		KeySyncer.inbox_store.write();

		int						mailcount_after		= Log.line_number_msg -1;

		if ( mailcount_before == mailcount_after ) {

			cb.done = false;
		
			Receive_email.set_inbox_listener(profile_id, cb);

			if ( !"yes".equals(mailer) && Log.mode_screen_allowed && ( cb.keystroke==null || "".equals(cb.keystroke)) ) {
				set_keystroke_listener(cb);
			}

			try {
				while ( !cb.done ) {
					TimeUnit.SECONDS.sleep(1);
				}

				if ( Receive_email.executor != null )		Receive_email.executor.shutdownNow();
				if ( executor != null )						executor.shutdownNow();

			} catch (InterruptedException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}

			if ( (cb!=null) && (cb.keystroke!=null) ) {
				if ( (cb.folder!= null) && !"".equals(cb.keystroke) ) {
					try {
						cb.folder.close();
						System.out.print(cb.keystroke);
					} catch (MessagingException e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
					}
				}
					
				if ( "q".equals(cb.keystroke) ) {
					Log.l(" ...will quit.");
					result = IDLE_RESULT_QUIT;
				}

				if ( "r".equals(cb.keystroke) ) {
					Log.l(" ...will show new emails.");
					result = IDLE_RESULT_READ;
				}

				if ( "w".equals(cb.keystroke) ) {
					result = IDLE_RESULT_WRITE;
				}
			}

		} else if ( mailcount_before < mailcount_after ) {
			IO.sendMsg( k_parameter.getMsgSetFromInboxAndAddressbook(null, true ) , KeySyncer.profile.id);
			consider_print_status();
			KeySyncer.inbox_store.write();

			if ( "yes".equals(mailer) ) result = IDLE_RESULT_RETURN;

		} else if ( cb.folder!= null ) {
				try {
					cb.folder.close();
				} catch (MessagingException e) {
					Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
				}
		}

		return result;

	}

	public static void read(){

		Log.l("+" + dashes.substring(0, long_title.length()) + "+\n");
		
		if ( ( Log.collected_output != null ) && ( Log.collected_output.size() > 0 ) ) {

			Log.l("\n --- new emails: ---\n");

			final String[]							reserved_ordered_keys	= { "from", /*"to", "date", "time",*/ "body" };

			Iterator<HashMap<String, String>> i = Log.collected_output.iterator();

			while ( i.hasNext() ) {

				HashMap<String, String> an_output_map = i.next();

				for ( int n = 0 ; n < reserved_ordered_keys.length ; n++ ) {
					String item = an_output_map.get(reserved_ordered_keys[n]);
					item = item.replace("\n", "");
					item = item.replace("\r", "");
					System.out.print(" | " + ((item == null)?"":item));
				}
				System.out.println(" | ");
			}
			Log.l("\n --- new emails output done. ---\n");
			Log.line_number_msg = 1;

		} else {
			Log.l("\n --- There are no new emails. ---\n");
		}

		Log.collected_output = new HashSet<HashMap<String, String>>();
		Log.last_status = "";

		title = false;
	}

	public static void profile_auto(KeySyncer k_parameter, String profile_id, String mailer) {
		// table with emails: 				all HashMaps which have get("line_msg")!="" in it
		// table with addressbook changes: 	all HashMaps which have get("line_book")!="" in it

		init(profile_id);

		while ( !done ) {

			int result = getIdleResult(k_parameter, profile_id, mailer);

			switch (result) {
				case IDLE_RESULT_READ: {
					read();
					break;
				}
				case IDLE_RESULT_RETURN: {
					IO.sendMsg( k_parameter.getMsgSetFromInboxAndAddressbook(null, true /* output will go to collected_output */) , KeySyncer.profile.id); // if "r": show, if Space: stop
					consider_print_status();
					KeySyncer.inbox_store.write();
				}
				case IDLE_RESULT_QUIT: {
					done = true;
					break;
				}
				// "w" and "x" to be implemented

			}

		}

		closing = true;
		consider_print_status();
			
	}

	static void consider_print_status() {

		int no_xm = Log.line_number_xml - 1;
		int no_mg = Log.line_number_msg - 1;

		String	col3 = String.valueOf(no_mg);
		String	col2 = String.valueOf(no_xm);
		String	col1 = " Waiting..."; // default
	
		if ( !"".equals( Log.last_status ) ) {
			col1 = Log.last_status + col1;
			Log.last_status = "";
		}
		
		if ( !title ) {
			title = true;
			Log.l("\n+" + dashes.substring(0, long_title.length()) + "+\n");
			Log.l("|" + long_title + "|\n");
			Log.l("+" + dashes.substring(0, long_title.length()) + "+\n");
			Log.l("|" + col0_input + "|" + col1_what + "|" + col2_ad + "|" + col3_mg + "|\n");
			Log.l("+" + dashes.substring(0, col0_input.length()) + "+" + dashes.substring(0, col1_what.length()) + "+" + dashes.substring(0, col2_ad.length()) + "+" + dashes.substring(0, col3_mg.length()) + "|\n");
		}
	
															String	col0_space	= spaces.substring(0, col0_input.length());
		int diff1 = col1_what.length() - col1.length();		String	col1_space	= spaces.substring(0, ( diff1 < 0 )?115:diff1);
		int diff2 = col2_ad.length() - 	 col2.length();		String	col2_space	= spaces.substring(0, ( diff2 < 0 )?115:diff2);
		int diff3 = col3_mg.length() -	 col3.length();		String	col3_space	= spaces.substring(0, ( diff3 < 0 )?115:diff3);

		Log.l("\r|" + col0_space + "|" + col1 + col1_space + "|" + col2_space + col2 + "|" + col3_space + col3 + "|\r|");

		if ( closing ) {
			closing = false;
			Log.l("\n+" + dashes.substring(0, long_title.length()) + "+\n");
		}
	}

	static void set_keystroke_listener(KeySyncCallback cb_parameter) {

		executor = null;
		Callable<Boolean>	c_keystroke	= new Keystroke( cb_parameter );

		executor = Executors.newSingleThreadExecutor();
		executor.execute(() -> {
			try {
				c_keystroke.call();
			} catch (Exception e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}
		});

	}

    static class Keystroke implements Callable<Boolean> {

        KeySyncCallback  cb;
        Keystroke (KeySyncCallback cb_parameter) {cb = cb_parameter;}

        @Override
        public Boolean call() {
			Log.l("\r|");
			scan = new Scanner(System.in);
			String text = 	scan.next();
			/* if ( "".equals(text) ) {
				text = "*";
			}
			System.out.println("\nyes, input done.\n");*/
			cb.keystroke = text;
			cb.completed();
			return true;
        }
    }


}
