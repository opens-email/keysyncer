/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import email.opens.keysync.KeySyncer;
import email.opens.keysync.io.local.Store_xml;
import email.opens.keysync.io.net.Invite_ServerComm;

// Object Addressbook - has representation in XML as "addressbook"
// ==============
// general idea of objects which have a representation in the XML file:
// 		* usually an object is read from XML and analysed, as long as manipulations
//				are not to be written, it can be thrown away after that
//		* if a manipulation is done (and should be saved) it will be written to the dom representation
//				ALWAYS using ab_store... where "..." indicates [get|set] and the object in lowercase
//		* after all manipulations at the end of the main method the dom will be
//					written to the XML file

// collection of all situations where ab_store is used
// only standard methods used: readNodeList, performXMLJob and .doc

public class Addressbook {

	Profile profile;
	public  Store_xml ab_store;

	class KeyResult {
		String	id;
		String	phone;
		String	tanc;
		String	spread_name;
		boolean	isValidKey;
		boolean	isInvitationKey;
	}

	public Addressbook() {
		profile		= KeySyncer.profile;
		ab_store	= new Store_xml("book.xml");
	}

	/*
			general Addressbook related methods >> only if filter is "" <<
public  HashSet<String>     getCardIds(String filter)

			KeySyncer related methods
public  void                purge()
        KeyResult           checkForKey(String keyinclude_parameter)
public  HashSet<String>     checkForCrippledHash(String inviter_crippled_hash)
public  HashSet<String>     getCardIds(String filter)
public  HashSet<Transfer_msg> getOpenActions(Inbox.Result ir_parameter)
private Transfer_msg          setLocalKeysForCardId(String c_parameter, Boolean create_keys_anyway_parameter)
        HashSet<Transfer_msg> getOpenActionsLocalKeysForCards(HashSet<String> to_consider_keys_parameter)
        void                mergeOpenActionsSpreadBeenspreadForCards(HashSet<String[]> to_consider_spread_been_spread_parameter,
private void                addIncludeKeyForListenSpread (HashMap map, HashSet given, HashSet news, String sen, String lis, String spr, String ls)
public  void                addCard(String as, String name, String id, String phone)
public  void                addProfile(String name, String id, String phone)
public  void                setForeignKeysByMsgAndTrust(Inbox_msg m, boolean trust)

	 */

	public void purge() {

		set_addressbook_purge_outdated_keys(profile.id, Key.get_df(-3) + "0000");
	}

	KeyResult checkForKey(String keyinclude_parameter) {
		KeyResult kr = new KeyResult();
		kr.id = Card.getIdByLocalKey(profile.id, keyinclude_parameter); // instead of LocalKey -> InvitationKey ?

		if (kr.id.equals(""))
			kr.isValidKey = false;
		else
			kr.isValidKey = true;

		kr.isInvitationKey = false;
		if (!kr.isValidKey) {
			kr.phone		= Card.getPhoneByInvitationKey(profile.id, keyinclude_parameter);
			kr.tanc			= Card.getTancByInvitationKey(profile.id, keyinclude_parameter); // get_card_read(profile.id, "", "", "/invitation_key[@value='" + keyinclude_parameter + "']/@tanc", "nothing_special");
			kr.spread_name	= Card.getSpreadnameeByInvitationKey(profile.id, keyinclude_parameter); // get_card_read(profile.id, "", "", "/invitation_key[@value='" + keyinclude_parameter + "']/../@name", "nothing_special");
			kr.id			= Card.getIdByInvitationKey(profile.id, keyinclude_parameter); // get_card_read(profile.id, "", "", "/invitation_key[@value='" + keyinclude_parameter + "']/../@id", "nothing_special");
			if (!kr.phone.equals(""))
				kr.isInvitationKey = true;
		}

		return kr;
	}

	public HashSet<String> checkForCrippledHash(String inviter_crippled_hash) {
		
		HashSet<String>						phones_match	= new HashSet<String>();
		HashSet<HashMap<String, Object>>	phone_map		= get_addressbook_Phone(profile.id);

		Iterator <HashMap<String, Object>> 	it_phone		= phone_map.iterator();

		while (it_phone.hasNext()) {
			HashMap<String, Object>	a_phone_item				= it_phone.next();
			String					a_phone						= (String) a_phone_item.get("phone");
			String					crippled_hash_of_a_phone	= Invite_ServerComm.getStringFromSHA256(a_phone);
			
			if ( inviter_crippled_hash.equals(crippled_hash_of_a_phone) ) {
				phones_match.add(a_phone);
			}
		}

		return phones_match;
	}

	public HashSet<String> getCardIds(String filter) {

		HashSet<HashMap<String, Object>> 	id_map 	= get_addressbook_AllCardIdsWithFilter(profile.id, filter);
		HashSet<String> 					ids 	= new HashSet<String>();
	
		Iterator <HashMap<String, Object>>	it_map = id_map.iterator();
	
		while ( it_map.hasNext() ) {
			ids.add( (String) it_map.next().get("id"));
		}
	
		return ids;
	}

	public HashSet<Transfer_msg> getOpenActions(Inbox.Result ir_parameter) {

		HashSet<Transfer_msg> 	m = getOpenActionsLocalKeysForCards(ir_parameter.consider_send_keys);
								mergeOpenActionsSpreadBeenspreadForCards(ir_parameter.consider_spread_been_spread, m);

		return m;
	}

	public void addCard(String as, String name, String id, String phone) {
		set_addressbook_addCard(as, name, id, phone);
	}

	public void addProfile(String name, String id, String phone) {
		set_addressbook_newProfile(name, id, phone);
	}

	void setForeignKeysByMsgAndTrust(Store_msg m, boolean trust) {
		set_addressbook_ForeignKeysByMsgAndTrust(m, trust);
	}

	private Transfer_msg setLocalKeysForCardId(String c_parameter, Boolean create_keys_anyway_parameter) {

		Transfer_msg 	m			= null;
		Card		c			= new Card(c_parameter);
		Key 		local_keys 	= new Key(c); // create new local keys to send them

		boolean created_some 	= local_keys.createLocalKeys(create_keys_anyway_parameter);
		
		if ( created_some ) {
			m = c.setLocalKeys(local_keys);
		}

		return m;
	}

	private HashSet<Transfer_msg> getOpenActionsLocalKeysForCards(HashSet<String> to_consider_keys_parameter) {

		HashSet<Transfer_msg>		m		= new HashSet<Transfer_msg>();

		HashSet<HashMap<String,Object>> id_map = get_addressbook_AllCardIdsWithFilter(
																	profile.id, Xs.any_automatic_path("exchange_status")
																									);

		HashSet<String>						ids		= new HashSet<String>();
		Iterator<HashMap<String,Object>>	it_map	= id_map.iterator();
		while (it_map.hasNext()) {
			ids.add( (String) it_map.next().get("id") );
		}

		ids.addAll(to_consider_keys_parameter);

		Transfer_msg			m_new;
		Iterator<String>	it_id	= ids.iterator();

		while (it_id.hasNext()) {

			m_new = setLocalKeysForCardId( it_id.next(), false );

			if (m_new != null) {
				m.add(m_new);
			}
		}

		return m;
	}

	private void mergeOpenActionsSpreadBeenspreadForCards(HashSet<String[]> to_consider_spread_been_spread_parameter,
																HashSet<Transfer_msg> m_keys) {

		String log1 = "";
		if ( m_keys.size() > 0 ) {
			log1 = "Cards: " + String.valueOf(m_keys.size()) + ". ";
		}
																														
		HashSet<Transfer_msg>			new_m	= new HashSet<Transfer_msg>();

		HashMap<String, Transfer_msg>	msg_map = new HashMap<String, Transfer_msg>();

		Iterator<Transfer_msg>			it_msg	= m_keys.iterator();
		while (it_msg.hasNext()) {
			Transfer_msg m_id_msg = it_msg.next();
			msg_map.put(m_id_msg.id_foreignkey_owner, m_id_msg);
		}

		HashSet<HashMap<String,Object>> id_phone_map = get_addressbook_AllCardIdsPhonesWithFilter(
																	profile.id, Xs.any_automatic_listen_path("exchange_status")
																									);

		Iterator<String[]> it_sbs = to_consider_spread_been_spread_parameter.iterator();
		while (it_sbs.hasNext()) {
			String[] tospread 		= it_sbs.next(); // tospread = person to be spread (_id: his id, _phone, his phone_number)
			String tospread_id		= tospread[0]; // 0 = id
			String tospread_phone	= tospread[1]; // 1 = phone
																						
			Iterator<HashMap<String,Object>> it_ids_phones =  id_phone_map.iterator();
			while (it_ids_phones.hasNext()) {
				HashMap<String, Object> an_id_phone = it_ids_phones.next();

				String card_id		= (String) an_id_phone.get("id");

				if ( !card_id.equals(tospread_id) ) {
					String card_phone	= (String) an_id_phone.get("phone");

					// 1_1: message that spreds a new "automatic_listen_spread" user-id
					// 2_1: let an "automatic_listen_spread" know to whom his id has been spread
					// use msg which already has proofkey or create a msg with a new proofkey
					addIncludeKeyForListenSpread(msg_map, m_keys, new_m, card_id,		tospread_id, 	"",			tospread_phone); //listenrequest
					addIncludeKeyForListenSpread(msg_map, m_keys, new_m, tospread_id,	"",				card_id,	card_phone);	// spreadnote

					Log.l("from", profile.id);
					Log.l("tospread_id", tospread_id);
					Log.l("tospread_phone", tospread_phone);
					Log.l("card_id", card_id);
					Log.l("card_phone", card_phone);

					log1 = log1 + "Spread new user.";
				}
			}
		}

		if ( !"".equals(log1) ) {
			Log.l(log1 + " Done.", Log.MODE_TO_XML);
		}

		m_keys.addAll(new_m);
	}

	private void addIncludeKeyForListenSpread (HashMap<String, Transfer_msg> map, HashSet<Transfer_msg> given, HashSet<Transfer_msg> new_ones, String sendto, String listenrequest_id, String spreadnote_id, String ls_phone) {

		Transfer_msg m;
		if (map.keySet().contains(sendto)) {
			m  = map.get(sendto);
		} else {
			m  = setLocalKeysForCardId(sendto, true);
		}

		if ( m != null ) {
			given.remove(m);
			m.updateWithListenSpread(profile.id, sendto, listenrequest_id, spreadnote_id, ls_phone);
			Log.log(5, "Class Addressbook: sende listenrequest / spreadnote / phone: " + listenrequest_id + " / " + spreadnote_id + " / " + ls_phone);
			new_ones.add(m);
		}
	}

	/*
			Addressbook interface methods
	*******************************************************************************************************************
				The following methods ONLY interact with xml structure
									  ONLY  			 xml structure				_G_ETTER	SPEC object		HashSet
HashSet<HashMap<String, Object>>    get_addressbook_Phone(String profile_id_parameter) {
HashSet<HashMap<String, Object>>    get_addressbook_AllCardIdsWithFilter(String profile_id_parameter, String filter) {
HashSet<HashMap<String, Object>>    get_addressbook_AllCardIdsPhonesWithFilter(String profile_id_parameter, String filter) {
									  ONLY  			 xml structure				_S_ETTER	SPEC object
void                                set_addressbook_addCard(String as, String name, String id, String phone) {
void                                set_addressbook_purge_outdated_keys(String profile_id, String minimum_date
void                                set_addressbook_ForeignKeysByMsgAndTrust(Inbox_msg m, boolean trust) {
void                                set_addressbook_newProfile(String name_parameter, String id_parameter, String phone_parameter) {
	*******************************************************************************************************************
	 */

	private HashSet<HashMap<String, Object>> get_addressbook_Phone(String profile_id_parameter) {

		String 	path 	= 			"/identities/identity[@id='" + profile_id_parameter + "']/addressbook/card";

		HashSet<String[]> par = new HashSet<String[]>();
		String[] i = {"./attribute::phone",	"STRING",	"phone"};			par.add(i);

		return ab_store.readNodeList(path, ab_store.doc, par);
	}

	private HashSet<HashMap<String, Object>> get_addressbook_AllCardIdsWithFilter(String profile_id_parameter, String filter) {
		String 	path 	= 			"/identities/identity[@id='" + profile_id_parameter + "']/addressbook/card";

		if ( !filter.equals("") ) {
			path 	= path + 	"[" + filter + "]";
		}

		HashSet<String[]> par = new HashSet<String[]>();
		String[] i = {"./attribute::id",	"STRING",	"id"};			par.add(i);

		return ab_store.readNodeList (path, ab_store.doc, par);
	}

	private HashSet<HashMap<String, Object>> get_addressbook_AllCardIdsPhonesWithFilter(String profile_id_parameter, String filter) {
		String 	path 	= 			"/identities/identity[@id='" + profile_id_parameter + "']/addressbook/card";

		if ( !filter.equals("") ) {
			path 	= path + 	"[" + filter + "]";
		}

		HashSet<String[]> par = new HashSet<String[]>();
		String[] i = {"./attribute::id",	"STRING",	"id"};			par.add(i);
		String[] p = {"./attribute::phone",	"STRING",	"phone"};		par.add(p);

		return ab_store.readNodeList (path, ab_store.doc, par);
	}

	private void set_addressbook_addCard(String as, String name, String id, String phone) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 			as});
		v.add(new String[] {"name",			name});
		v.add(new String[] {"id", 			id});
		v.add(new String[] {"phone",		phone});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"id\"/>\n"
						+ "    <xsl:param name=\"name\"/>\n"
						+ "    <xsl:param name=\"phone\"/>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "                <card id=\"{$id}\" name=\"{$name}\" phone=\"{$phone}\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		ab_store.performXMLJob(xslt, v);
	}

	private void set_addressbook_purge_outdated_keys(String profile_id, String minimum_date) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as",			profile_id				});
		v.add(new String[] {"minimum_date",	minimum_date			});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"minimum_date\"/>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card\">\n"
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card/localkey[$minimum_date>@uniquepurge]\"/>\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card/foreignkey[$minimum_date>@uniquepurge]\"/>\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card/invitation_key[$minimum_date>@expiry]\"/>\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card/localkey_purgeme[$minimum_date>@uniquepurge]\"/>\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card/foreignkey_purgeme[$minimum_date>@uniquepurge]\"/>\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card/invitation_key_purgeme[$minimum_date>@expiry]\"/>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		ab_store.performXMLJob(xslt, v);
	}

	private void     set_addressbook_ForeignKeysByMsgAndTrust(Store_msg m, boolean trust) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as",								m.profile.id		});
		v.add(new String[] {"card_id",							m.meta.card_id			});
		v.add(new String[] {"trusted",							trust?"yes":"no"	});

		for (int n = 1; n <= (Key.GOOD_KEY_NUM + 1); n++) {
			v.add(new String[] {"foreign_key" + n + "_enc", "none"});
			v.add(new String[] {"foreign_key" + n + "_key", m.meta.key_set.value[n-1]});
			v.add(new String[] {"foreign_key" + n + "_date", m.meta.key_set.date[n-1]});
		}

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"card_id\"/>\n"
						+ "    <xsl:param name=\"trusted\"/>\n"
						+ "    <xsl:param name=\"foreign_key1_enc\"/>\n"
						+ "    <xsl:param name=\"foreign_key1_key\"/>\n"
						+ "    <xsl:param name=\"foreign_key1_date\"/>\n"
						+ "    <xsl:param name=\"foreign_key2_enc\"/>\n"
						+ "    <xsl:param name=\"foreign_key2_key\"/>\n"
						+ "    <xsl:param name=\"foreign_key2_date\"/>\n"
						+ "    <xsl:param name=\"foreign_key3_enc\"/>\n"
						+ "    <xsl:param name=\"foreign_key3_key\"/>\n"
						+ "    <xsl:param name=\"foreign_key3_date\"/>\n"
						+ "    <xsl:param name=\"foreign_key4_enc\"/>\n"
						+ "    <xsl:param name=\"foreign_key4_key\"/>\n"
						+ "    <xsl:param name=\"foreign_key4_date\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique1\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$foreign_key1_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"1600000000 + number($foreign_key1_date)\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique2\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$foreign_key2_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"1600000000 + number($foreign_key2_date)\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique3\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$foreign_key3_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"1600000000 + number($foreign_key3_date)\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique4\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$foreign_key4_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"1600000000 + number($foreign_key4_date)\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"count\" select=\"count(/identities/identity[@id=$as]/addressbook/card[@id=$card_id]/foreignkey/attribute::uniquepurge)\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"max_key\" select=\"3\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"num_key_which_remain\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$foreign_key4_key!=''\">\n"
						+ "                <xsl:value-of select=\"0\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:choose>\n"
						+ "                    <xsl:when test=\"$foreign_key3_key!=''\">\n"
						+ "                        <xsl:value-of select=\"0\"/>\n"
						+ "                    </xsl:when>\n"
						+ "                    <xsl:otherwise>\n"
						+ "                        <xsl:choose>\n"
						+ "                            <xsl:when test=\"$foreign_key2_key!=''\">\n"
						+ "                                <xsl:value-of select=\"1\"/>\n"
						+ "                            </xsl:when>\n"
						+ "                            <xsl:otherwise>\n"
						+ "                                <xsl:choose>\n"
						+ "                                    <xsl:when test=\"$foreign_key1_key!=''\">\n"
						+ "                                        <xsl:value-of select=\"2\"/>\n"
						+ "                                    </xsl:when>\n"
						+ "                                    <xsl:otherwise>\n"
						+ "                                        <xsl:value-of select=\"3\"/>\n"
						+ "                                    </xsl:otherwise>\n"
						+ "                                </xsl:choose>\n"
						+ "                            </xsl:otherwise>\n"
						+ "                        </xsl:choose>\n"
						+ "                    </xsl:otherwise>\n"
						+ "                </xsl:choose>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"index\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"($num_key_which_remain + 1) > $count\">\n"
						+ "                <xsl:value-of select=\"number($count)\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"number($num_key_which_remain + 1)\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"only_dates_above\">\n"
						+ "        <xsl:for-each select=\"/identities/identity[@id=$as]/addressbook/card[@id=$card_id]/foreignkey/attribute::uniquepurge\">\n"
						+ "            <xsl:sort select=\".\" data-type=\"number\" order=\"descending\"/>\n"
						+ "            <xsl:if test=\"position() = $index\">\n"
						+ "                <xsl:value-of select=\".\"/>\n"
						+ "            </xsl:if>\n"
						+ "        </xsl:for-each>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"minimum_date\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"($num_key_which_remain + 1) > $count\">\n"
						+ "                <xsl:value-of select=\"$only_dates_above\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"$only_dates_above + 1\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">\n"
						+ ""
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">\n"
						+ ""
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ ""
						+ "            <xsl:if test=\"$foreign_key1_key!=''\">\n"
						+ "                <foreignkey uniquepurge=\"{$foreign_key1_date}\" value=\"{$foreign_key1_key}\" enc=\"{$foreign_key1_enc}\" trust_purge_unique=\"{$trust_purge_unique1}\"/>\n"
						+ "            </xsl:if>\n"
						+ ""
						+ "            <xsl:if test=\"$foreign_key2_key!=''\">\n"
						+ "                <foreignkey uniquepurge=\"{$foreign_key2_date}\" value=\"{$foreign_key2_key}\" enc=\"{$foreign_key2_enc}\" trust_purge_unique=\"{$trust_purge_unique2}\"/>\n"
						+ "            </xsl:if>\n"
						+ ""
						+ "            <xsl:if test=\"$foreign_key3_key!=''\">\n"
						+ "                <foreignkey uniquepurge=\"{$foreign_key3_date}\" value=\"{$foreign_key3_key}\" enc=\"{$foreign_key3_enc}\" trust_purge_unique=\"{$trust_purge_unique3}\"/>\n"
						+ "            </xsl:if>\n"
						+ ""
						+ "            <xsl:if test=\"$foreign_key4_key!=''\">\n"
						+ "                <foreignkey uniquepurge=\"{$foreign_key4_date}\" value=\"{$foreign_key4_key}\" enc=\"{$foreign_key4_enc}\" trust_purge_unique=\"{$trust_purge_unique4}\"/>\n"
						+ "            </xsl:if>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card[@id=$card_id]/foreignkey\">\n"
						+ ""
						+ "        <xsl:if test=\"../foreignkey[@uniquepurge>=$minimum_date]\">\n"
						+ "            <xsl:copy>\n"
						+ "                <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "            </xsl:copy>\n"
						+ "        </xsl:if>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		ab_store.performXMLJob(xslt, v);
	}

	private void     set_addressbook_newProfile(String name_parameter, String id_parameter, String phone_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"id", 			id_parameter});
		v.add(new String[] {"name",			name_parameter});
		v.add(new String[] {"phone",		phone_parameter});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"id\"/>\n"
						+ "    <xsl:param name=\"name\"/>\n"
						+ "    <xsl:param name=\"phone\"/>\n"
						+ "\n"
						+ "    <xsl:variable name=\"max\">\n"
						+ "        <xsl:for-each select=\"/identities/identity/attribute::number\">\n"
						+ "            <xsl:sort select=\".\" data-type=\"number\" order=\"descending\"/>\n"
						+ "            <xsl:if test=\"position() = 1\"><xsl:value-of select=\".\"/></xsl:if>\n"
						+ "        </xsl:for-each>\n"
						+ "    </xsl:variable>\n"
						+ "\n"
						+ "    <xsl:variable name=\"max_num\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"number($max) = $max\">\n"
						+ "                <xsl:value-of select=\"$max\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"-1\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "                <identity id=\"{$id}\" name=\"{$name}\" number=\"{$max_num+1}\" phone=\"{$phone}\">\n"
						+ "                    <addressbook/>\n"
						+ "                </identity>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		ab_store.performXMLJob(xslt, v);
	}

}
