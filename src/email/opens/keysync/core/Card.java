/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import email.opens.keysync.KeySyncer;
import java.util.Vector;

// Object Card - has representation in XML as "card"
// ==============
// general idea of objects which have a representation in the XML file:
// 		* usually an object is read from XML and analysed, as long as manipulations
//				are not to be written, it can be thrown away after that
//		* if a manipulation is done (and should be saved) it will be written to the dom representation
//				ALWAYS using ...profile.ab.ab_store... where the last "..." indicates [get|set] and the object in lowercase
//		* after all manipulations at the end of the KeySyncer.run method the dom will be
//					written to the XML file

// collection of all situations where ab_store is used
// only standard methods used: readString, performXMLJob

public class Card {

	public Profile	profile;
	public String	id;
	String			name;
	String			phone_number;

	IncludeKeyInfo 	iki;	

	public Card(String id_parameter) {
		profile = KeySyncer.profile;
		id		= id_parameter;
		name	= get_card_NameFromId(profile.id, id);
	}

	public Card(String name_parameter, String name_indicator) {
		profile = KeySyncer.profile;
		name	= name_parameter;
		id		= get_card_IdFromName(profile.id, name);
	}

	public Card(String phone_number_parameter, int phone_number_indicator) {

		profile 		= KeySyncer.profile;
		phone_number 	= phone_number_parameter;
		name			= getNameFromPhone(phone_number_parameter);
		id				= get_card_IdFromName(profile.id, name);
	}

	/*

class           IncludeKeyInfo  {int, String, String, String}

                String          getNameFromPhone(String phone)
public          String          getPhoneFromName(String name)

                boolean         uniqueDoesNotExistInLocal(String df, int does_this_exsist)

                String          getXsToBeSent()

                int             getForeignKeyTrustedCount()
                IncludeKeyInfo  getForeignKeySelectedForInclude()
                String          getLocalKeyAnyCount()
                String          getLocalKeyTrustedCount()
                String          getLocalKeyHighest()
                String          getTrustOfKeyInclude(String key_inc)

public  static  String          getPhoneByInvitationKey(String id_of_owner, String keyinclude_parameter)
public  static  String          getTancByInvitationKey(String id_of_owner, String keyinclude_parameter)
public  static  String          getSpreadnameeByInvitationKey(String id_of_owner, String keyinclude_parameter)
public  static  String          getIdByInvitationKey(String id_of_owner, String keyinclude_parameter)
public  static  String          getIdByLocalKey(String id_of_owner, String keyinclude_parameter)

public          void            addInvitation(String phone, String key, String expiry_date, String tanc)

public          void            setIdXsForeignkey(String xs_string_parameter, String foreignkey_parameter)
                void            set_InvitationExceeded(String tanc)
                Transfer_msg      setLocalKeys(Key keys)

                void            updateIdXsByPhone(String phone, String exchange_status)
public          void            updateXsToBeSentByCardid(String card_id, String xs_to_be_sent)
public          void            updateXs(String xs_aspired)
                void            updateXs(String id_sender, String xs_aspired)

                void            removeForeignKey(String key_value)
                void            removeLocalKey(String key_value)

	 */
	class IncludeKeyInfo {
		int		includekey_date_number;
		String 	includekey_date;
		String 	includekey_value;
		String	includekey_trustcode;
	}

	IncludeKeyInfo getForeignKeySelectedForInclude() {
		// For including: the date (attribute trust_purge_unique) will be selected which is the smallest value
		// 1. use the oldest value (as it will be the first to be purged)
		// 2. smallest also means the most trustful

		iki 						= new IncludeKeyInfo();

		iki.includekey_date 		= get_card_ForeignDateForInclude(profile.id, id);

		if ( !iki.includekey_date.equals("") ) {
			iki.includekey_date_number 	= Integer.valueOf(iki.includekey_date);
		}
		else {
			iki.includekey_date_number 	= 0;
		}

		iki.includekey_value 		= get_card_ForeignValueByTrustPurgeUnique(profile.id, id, iki.includekey_date);

		return iki;
	}

	String getNameFromPhone(String phone) {
		return get_card_NameFromPhone(profile.id, phone);
	}

	public String getPhoneFromName (String name) {
		return get_card_PhoneFromName(profile.id, name);
	}

	boolean uniqueDoesNotExistInLocal(String df, int does_this_exsist) {
		String check_it = get_card_ExistsLocalWithUnique(profile.id, id, df, does_this_exsist);
		return check_it.equals("");
	}

	int getForeignKeyTrustedCount() {
		return get_card_count_foreign_key_after_removal(profile.id, id);
	}

	String getLocalKeyAnyCount() {
		return get_card_LocalKeyAnyCount(profile.id, id);
	}		

	String getLocalKeyTrustedCount() {
		return get_card_LocalKeyTrustedCount(profile.id, id);
	}

	String getLocalKeyHighest () {
		return get_card_LocalKeyDateNumberHighest(profile.id, id);
	}

	String getTrustOfKeyInclude(String key_inc) {
		return get_card_TrustOfKeyInclude(profile.id, key_inc);
	}

	static String getPhoneByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		return get_card_Phone_ByInvitationKey     (id_of_owner, keyinclude_parameter);
	}

	static String getTancByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		return get_card_Tanc_ByInvitationKey     (id_of_owner, keyinclude_parameter);
	}

	static String getSpreadnameeByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		return get_card_Spreadname_ByInvitationKey     (id_of_owner, keyinclude_parameter);
	}

	static String getIdByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		return get_card_ID_ByInvitationKey     (id_of_owner, keyinclude_parameter);
	}

	static String getIdByLocalKey(String id_of_owner, String keyinclude_parameter) {
		return get_card_Id_ByLocalKey(id_of_owner, keyinclude_parameter);
	}

	public void addInvitation(String phone, String key, String expiry_date, String tanc) {
		set_card_invitation(profile.id, phone, key, expiry_date, tanc);
	}

	public void setIdXsForeignkey(String xs_string_parameter, String foreignkey_parameter) {

		String up = Key.get_df(0) + "0000";

		updateIdXsByPhone(phone_number, xs_string_parameter);
		set_card_ByPhone_Foreignkey(profile.id, id, foreignkey_parameter, up, up);
	}

	void set_InvitationExceeded(String tanc) {
		set_card_ByPhone_InvitationExceeded(profile.id, phone_number, tanc);
	}

	Transfer_msg setLocalKeys(Key keys) {
		set_card_LocalKeys(profile.id, id, iki.includekey_trustcode, keys);
		return new Transfer_msg(profile.id, id, iki, keys, getXsToBeSent());
	}

	void updateIdXsByPhone (String phone, String exchange_status) {
		set_card_ByPhone_IdXs(profile.id, id, phone, exchange_status);
	}

	public void updateXsToBeSentByCardid(String card_id, String xs_to_be_sent) {
		set_card_ToXstobesentByCardid(profile.id,  card_id,  xs_to_be_sent);
	}

	public void updateXs(String xs_aspired) {
		updateXs(id, xs_aspired);
	}

	void updateXs(String id_sender, String xs_aspired) {
		set_card_ToNameIDSenderXSByCardid(profile.id, id_sender, name, id, xs_aspired);
	}

	void removeForeignKey(String key_value) {
		set_card_ForeignKeyRemoveByValue(profile.id, id, key_value);
	}

	void removeLocalKey(String key_value) {
		set_card_key_removeByValue(profile.id, id, key_value);
	}

	private String getXsToBeSent() {
		return get_card_XsToBeSent(profile.id, id);
	}

//	public static String getGeneralCardInfo(String profile_id, String card_attribute, String card_attribute_value, String path, String special) {
//		return get_card_read(profile_id, card_attribute, card_attribute_value, path, special);
//	}

	/*
	*******************************************************************************************************************
				The following methods ONLY interact with xml structure
									  ONLY  			 xml structure				_G_ETTER	ANY object		String
String  get_card_Id_ByLocalKey                  (String id_of_owner, String keyinclude_parameter)
String  get_card_Phone_ByInvitationKey          (String id_of_owner, String keyinclude_parameter)
String  get_card_Tanc_ByInvitationKey           (String id_of_owner, String keyinclude_parameter)
String  get_card_Spreadname_ByInvitationKey     (String id_of_owner, String keyinclude_parameter)
String  get_card_ID_ByInvitationKey             (String id_of_owner, String keyinclude_parameter)
String  get_card_read                           (String id, String attribute, String attribute_value, String path, String special)
									  ONLY  			 xml structure				_G_ETTER	SPEC. object	boolean
String  get_card_ExistsLocalWithUnique          (String profile_id, String card_id, String df, int n)
									  ONLY  			 xml structure				_G_ETTER	SPEC. object	number
int     get_card_count_foreign_key_after_removal(String profile_id_parameter, String to_id)
									  ONLY  			 xml structure				_G_ETTER	SPEC. object	KEYSyncer
String  get_card_ForeignDateForInclude          (String profile_id, String card_id)
String  get_card_ForeignValueByTrustPurgeUnique (String profile_id, String card_id, String date_number_oldest)
String  get_card_LocalKeyDateNumberHighest      (String profile_id_parameter, String card_id)
String	get_card_XsToBeSent                     (String profile_id, String card_id)
String  get_card_LocalKeyAnyCount               (String profile_id_parameter, String card_id)
String  get_card_LocalKeyTrustedCount           (String profile_id_parameter, String card_id)
String  get_card_TrustOfKeyInclude              (String profile_id, String kinc)
									  ONLY  			 xml structure				_G_ETTER	SPEC. object	other
String	get_card_NameFromPhone                  (String profile_id, String phone)
String	get_card_IdFromName                     (String profile_id, String name)
String	get_card_PhoneFromName                  (String profile_id, String name)
String	get_card_NameFromId                     (String profile_id, String id_parameter)
									  ONLY  			 xml structure				_S_ETTER	SPEC. object	KEYSyncer
void    set_card_ForeignKeyRemoveByValue        (String profile_id, String to_id, String value)
void	set_card_key_removeByValue              (String profile_id, String card_id_person, String key_to_be_removed)
void    set_card_ToXstobesentByCardid           (String profile_id, String card_id_person, String exch)
void    set_card_ToNameIDSenderXSByCardid       (String profile_id, String id_person, String name_per, String car, String ex)
void    set_card_ByPhone_IdXs                   (String profile_id_parameter, String card_id_parameter,
void    set_card_ByPhone_Foreignkey             (String profile_id_par, String card_id_pers, String val, String tpu, String up)
void    set_card_invitation                     (String profile_id, String phone, String key, String expiry_date, String tanc)
void    set_card_ByPhone_InvitationExceeded     (String profile_id, String phone, String tanc)
void    set_card_LocalKeys                      (String profile_id, String to_id_parameter, String trust_code, Key keys_par)
	*******************************************************************************************************************
	 */

	/*
	*******************************************************************************************************************
									  ONLY  			 xml structure		GETTER		ANY object		String
	*******************************************************************************************************************
	*/
	private static String get_card_Id_ByLocalKey(String id_of_owner, String keyinclude_parameter) {
		String id = Card.get_card_read(id_of_owner, "", "", "/localkey[@value='" + keyinclude_parameter + "']/../@id", "nothing_special");
		return id;
	}

	private static String get_card_Phone_ByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		String phone = Card.get_card_read(id_of_owner, "", "", "/invitation_key[@value='" + keyinclude_parameter + "']/../@phone", "nothing_special");
		return phone;
	}

	private static String get_card_Tanc_ByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		String tanc = Card.get_card_read(id_of_owner, "", "", "/invitation_key[@value='" + keyinclude_parameter + "']/@tanc", "nothing_special");
		return tanc;
	}

	private static String get_card_Spreadname_ByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		String sname = Card.get_card_read(id_of_owner, "", "", "/invitation_key[@value='" + keyinclude_parameter + "']/../@name", "nothing_special");
		return sname;
	}

	private static String get_card_ID_ByInvitationKey(String id_of_owner, String keyinclude_parameter) {
		String id = Card.get_card_read(id_of_owner, "", "", "/invitation_key[@value='" + keyinclude_parameter + "']/../@id", "nothing_special");
		return id;
	}

	private static String  get_card_read(String profile_id, String card_attribute, String card_attribute_value, String path, String special) {

		String card_part = ""; // if there is no attribute to look for this remains empty
		if (!card_attribute.equals("")) {
			card_part = card_part + "[@" + card_attribute + "='" + card_attribute_value + "']";
		}

		String result_path = "/identities/identity[@id='" + profile_id + "']/addressbook/card" + card_part + path;
		if (special.equals("count")) result_path = "count(" + result_path + ")";
		if (special.equals("max")) 		result_path = "max(" + result_path + ")";

		return KeySyncer.profile.ab.ab_store.readString(result_path);
	}

	/*
	*******************************************************************************************************************
									  ONLY  			 xml structure		GETTER		SPEC. object	boolean
	*******************************************************************************************************************
	*/
	private String get_card_ExistsLocalWithUnique (String profile_id, String card_id, String df, int n) {

		String profile	= "/identities/identity[@id='" + profile_id + "']/addressbook";
		String card		= "/card[@id='" + card_id + "']";
		String check	= "/localkey[@uniquepurge='" + df + (n < 1000 ? "0":"") + (n < 100 ? "0":"") + (n < 10 ? "0":"") + String.valueOf(n) + "']/@uniquepurge";
		return this.profile.ab.ab_store.readString(profile + card + check );
	}

	/*
	*******************************************************************************************************************
									  ONLY  			 xml structure		GETTER		SPEC. object	number
	*******************************************************************************************************************
	*/
	private int   get_card_count_foreign_key_after_removal(String profile_id_parameter, String to_id) {
		int 	count 	= 0;
		String 	path 	= 			"/identities/identity[@id='" + profile_id_parameter + "']/addressbook";
		path 	= path + 	"/card[@id='" + to_id + "']/foreignkey[@trust_purge_unique<";
		path 	= path + 	Key.NO + "]/attribute::value";
		path 	= 			"count(" + path + ")";
		String 	cnt 	= profile.ab.ab_store.readString(path);

		if ( (cnt!= null) && (!cnt.equals("")) ) count = Integer.valueOf(cnt);
		return count;
	}

	/*
	*******************************************************************************************************************
									  ONLY  			 xml structure		GETTER		SPEC. object	KEYSyncer
	*******************************************************************************************************************
	*/
	private String get_card_ForeignDateForInclude(String profile_id, String card_id) {
		String profile	= "/identities/identity[@id='" + profile_id + "']/addressbook";
		String card		= "/card[@id='" + card_id + "']";
		String specific	= "/foreignkey[not(@trust_purge_unique > ../foreignkey/@trust_purge_unique)]/@trust_purge_unique[1]";
		return this.profile.ab.ab_store.readString(profile + card + specific );
	}

	private String get_card_ForeignValueByTrustPurgeUnique (String profile_id, String card_id, String date_number_oldest) {
		String profile	= "/identities/identity[@id='" + profile_id + "']/addressbook";
		String card		= "/card[@id='" + card_id + "']";
		String specific	= "/foreignkey[@trust_purge_unique='" + date_number_oldest + "']/@value[1]";
		return this.profile.ab.ab_store.readString(profile + card + specific );
	}

	private String   get_card_LocalKeyDateNumberHighest(String profile_id_parameter, String card_id) {
		String 	path 	= 			"/identities/identity[@id='" + profile_id_parameter + "']/addressbook";
		path 	= path + 	"/card[@id='" + card_id + "']/localkey/@uniquepurge[not(. < ../../localkey/@uniquepurge)][1]";
		path 	= 			"count(" + path + ")";
		String 	cnt 	= profile.ab.ab_store.readString(path);

		return cnt;
	}

	private String	get_card_XsToBeSent(String profile_id, String card_id) {
		return profile.ab.ab_store.readString("/identities/identity[@id='" + profile_id + "']/addressbook/card[@id='" + card_id + "']/attribute::xs_to_be_sent");
	}

	private String   get_card_LocalKeyAnyCount(String profile_id_parameter, String card_id) {
		String 	path 	= 			"/identities/identity[@id='" + profile_id_parameter + "']/addressbook";
		path 	= path + 	"/card[@id='" + card_id + "']/localkey/attribute::value";
		path 	= 			"count(" + path + ")";
		String 	cnt 	= profile.ab.ab_store.readString(path);

		return cnt;
	}

	private String   get_card_LocalKeyTrustedCount(String profile_id_parameter, String card_id) {
		String 	path 	= 			"/identities/identity[@id='" + profile_id_parameter + "']/addressbook";
		path 	= path + 	"/card[@id='" + card_id + "']/localkey[@trust_purge_unique<";
		path 	= path + 	Key.POTENTIALLY_YES + "]/attribute::value";
		path 	= 			"count(" + path + ")";
		String 	cnt 	= profile.ab.ab_store.readString(path);

		return cnt;
	}

	private String get_card_TrustOfKeyInclude(String profile_id, String kinc) {
		return get_card_read(profile_id, "", "", "/localkey[@value='" + kinc + "']/@trust_purge_unique", "nothing_special");
	}

	/*
	*******************************************************************************************************************
									  ONLY  			 xml structure		GETTER		SPEC. object	other
	*******************************************************************************************************************
	*/
	private String	get_card_NameFromPhone(String profile_id, String phone) {
		return get_card_read(profile_id, "phone", phone, "/attribute::name", "nothing_special");
	}

	private String	get_card_IdFromName(String profile_id, String name) {
		return get_card_read(profile_id, "name", name, "/attribute::id", "nothing_special");
	}

	private String	get_card_PhoneFromName(String profile_id, String name) {
		return get_card_read(profile_id, "name", name, "/attribute::phone", "nothing_special");
	}

	private String	get_card_NameFromId(String profile_id, String id_parameter) {
		return get_card_read(profile_id, "id", id_parameter, "/attribute::name", "nothing_special");
	}

	/*
	*******************************************************************************************************************
									  ONLY  			 xml structure		SETTER		SPEC. object	KEYSyncer
	*******************************************************************************************************************
	*/
	private void set_card_ForeignKeyRemoveByValue(String profile_id, String to_id, String value) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as",								profile_id	});
		v.add(new String[] {"card_id",							to_id		});
		v.add(new String[] {"foreign_key_key_to_be_removed",	value		});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"card_id\"/>\n"
						+ "    <xsl:param name=\"foreign_key_key_to_be_removed\"/>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">\n"
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]/foreignkey[@value=$foreign_key_key_to_be_removed]\"/>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void set_card_ByPhone_IdXs(String profile_id_parameter, String card_id_parameter,
									  String phone_parameter, String xs_string_parameter){

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 				profile_id_parameter});
		v.add(new String[] {"id_sender",		card_id_parameter});
		v.add(new String[] {"phone",			phone_parameter});
		v.add(new String[] {"exchange_status",	xs_string_parameter});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
						+ ""
						+ "    <xsl:param name=\"as\"/>"
						+ "    <xsl:param name=\"id_sender\"/>"
						+ "    <xsl:param name=\"phone\"/>"
						+ "    <xsl:param name=\"exchange_status\"/>"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@phone=$phone]\">"
						+ "        <card xs_to_be_sent=\"{@xs_to_be_sent}\" name=\"{@name}\" id=\"{$id_sender}\" exchange_status=\"{$exchange_status}\" phone=\"{@phone}\">"
						+ "            <xsl:apply-templates select=\"node()\"/>"
						+ "        </card>"
						+ "    </xsl:template>"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">"
						+ "        <xsl:copy>"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>"
						+ "        </xsl:copy>"
						+ "    </xsl:template>"
						+ ""
						+ "</xsl:stylesheet>";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void set_card_ByPhone_Foreignkey(String profile_id_parameter, String card_id_person, String val, String tpu, String up) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 			profile_id_parameter});
		v.add(new String[] {"card_id",		card_id_person});
		v.add(new String[] {"value",		val});
		v.add(new String[] {"tpu",			tpu});
		v.add(new String[] {"up",			up});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"card_id\"/>\n"
						+ "    <xsl:param name=\"value\"/>\n"
						+ "    <xsl:param name=\"tpu\"/>\n"
						+ "    <xsl:param name=\"up\"/>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">\n"
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"@*\"/>\n"
						+ "            <foreignkey enc=\"none\" value=\"{$value}\" trust_purge_unique=\"{$tpu}\" uniquepurge=\"{$up}\"/>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void set_card_invitation(String profile_id, String phone, String key, String expiry_date, String tanc) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 			profile_id});
		v.add(new String[] {"phone",		phone});
		v.add(new String[] {"key",			key});
		v.add(new String[] {"expiry_date",	expiry_date});
		v.add(new String[] {"tanc",			tanc});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"phone\"/>\n"
						+ "    <xsl:param name=\"key\"/>\n"
						+ "    <xsl:param name=\"expiry_date\"/>\n"
						+ "    <xsl:param name=\"tanc\"/>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@phone=$phone]\">\n"
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "            <invitation_key value=\"{$key}\" expiry=\"{$expiry_date}\" tanc=\"{$tanc}\"/>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void set_card_ByPhone_InvitationExceeded(String profile_id, String phone, String tanc) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 			profile_id});
		v.add(new String[] {"phone",		phone});
		v.add(new String[] {"tanc",			tanc});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"phone\"/>\n"
						+ "    <xsl:param name=\"tanc\"/>\n"
						+ "\n"
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@phone=$phone]/invitation_key[@tanc=$tanc]\">\n"
						+ "        <invitation_key_exceeded>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </invitation_key_exceeded>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void     set_card_LocalKeys(String profile_id, String to_id_parameter, String trust_code, Key keys_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as",								profile_id		});
		v.add(new String[] {"card_id",							to_id_parameter	});
		v.add(new String[] {"trusted",							trust_code		});

		for (int n = 1; n <= (Key.MAX_KEY_NUM); n++) {
			v.add(new String[] {"local_key" + n + "_enc", 	"none"									});
			v.add(new String[] {"local_key" + n + "_key",	keys_parameter.value[n-1]	});
			v.add(new String[] {"local_key" + n + "_date",	keys_parameter.date[n-1]	});
		}

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"card_id\"/>\n"
						+ "    <xsl:param name=\"trusted\"/>\n"
						+ "    <xsl:param name=\"local_key1_enc\"/>\n"
						+ "    <xsl:param name=\"local_key1_key\"/>\n"
						+ "    <xsl:param name=\"local_key1_date\"/>\n"
						+ "    <xsl:param name=\"local_key2_enc\"/>\n"
						+ "    <xsl:param name=\"local_key2_key\"/>\n"
						+ "    <xsl:param name=\"local_key2_date\"/>\n"
						+ "    <xsl:param name=\"local_key3_enc\"/>\n"
						+ "    <xsl:param name=\"local_key3_key\"/>\n"
						+ "    <xsl:param name=\"local_key3_date\"/>\n"
						+ "    <xsl:param name=\"local_key4_enc\"/>\n"
						+ "    <xsl:param name=\"local_key4_key\"/>\n"
						+ "    <xsl:param name=\"local_key4_date\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique1\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$local_key1_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:choose>\n"
						+ "                    <xsl:when test=\"$trusted='potentially_yes'\">\n"
						+ "                        <xsl:value-of select=\"800000000 + number($local_key1_date)\"/>\n"
						+ "                    </xsl:when>\n"
						+ "                    <xsl:otherwise>\n"
						+ "                        <xsl:value-of select=\"1600000000 + number($local_key1_date)\"/>\n"
						+ "                    </xsl:otherwise>\n"
						+ "                </xsl:choose>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique2\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$local_key2_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:choose>\n"
						+ "                    <xsl:when test=\"$trusted='potentially_yes'\">\n"
						+ "                        <xsl:value-of select=\"800000000 + number($local_key2_date)\"/>\n"
						+ "                    </xsl:when>\n"
						+ "                    <xsl:otherwise>\n"
						+ "                        <xsl:value-of select=\"1600000000 + number($local_key2_date)\"/>\n"
						+ "                    </xsl:otherwise>\n"
						+ "                </xsl:choose>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique3\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$local_key3_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:choose>\n"
						+ "                    <xsl:when test=\"$trusted='potentially_yes'\">\n"
						+ "                        <xsl:value-of select=\"800000000 + number($local_key3_date)\"/>\n"
						+ "                    </xsl:when>\n"
						+ "                    <xsl:otherwise>\n"
						+ "                        <xsl:value-of select=\"1600000000 + number($local_key3_date)\"/>\n"
						+ "                    </xsl:otherwise>\n"
						+ "                </xsl:choose>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"trust_purge_unique4\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$trusted='yes'\">\n"
						+ "                <xsl:value-of select=\"$local_key4_date\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:choose>\n"
						+ "                    <xsl:when test=\"$trusted='potentially_yes'\">\n"
						+ "                        <xsl:value-of select=\"800000000 + number($local_key4_date)\"/>\n"
						+ "                    </xsl:when>\n"
						+ "                    <xsl:otherwise>\n"
						+ "                        <xsl:value-of select=\"1600000000 + number($local_key4_date)\"/>\n"
						+ "                    </xsl:otherwise>\n"
						+ "                </xsl:choose>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"count\" select=\"count(/identities/identity[@id=$as]/addressbook/card[@id=$card_id]/localkey/attribute::uniquepurge)\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"max_key\" select=\"3\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"num_key_which_remain\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"$local_key4_key!=''\">\n"
						+ "                <xsl:value-of select=\"0\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:choose>\n"
						+ "                    <xsl:when test=\"$local_key3_key!=''\">\n"
						+ "                        <xsl:value-of select=\"0\"/>\n"
						+ "                    </xsl:when>\n"
						+ "                    <xsl:otherwise>\n"
						+ "                        <xsl:choose>\n"
						+ "                            <xsl:when test=\"$local_key2_key!=''\">\n"
						+ "                                <xsl:value-of select=\"1\"/>\n"
						+ "                            </xsl:when>\n"
						+ "                            <xsl:otherwise>\n"
						+ "                                <xsl:choose>\n"
						+ "                                    <xsl:when test=\"$local_key1_key!=''\">\n"
						+ "                                        <xsl:value-of select=\"2\"/>\n"
						+ "                                    </xsl:when>\n"
						+ "                                    <xsl:otherwise>\n"
						+ "                                        <xsl:value-of select=\"3\"/>\n"
						+ "                                    </xsl:otherwise>\n"
						+ "                                </xsl:choose>\n"
						+ "                            </xsl:otherwise>\n"
						+ "                        </xsl:choose>\n"
						+ "                    </xsl:otherwise>\n"
						+ "                </xsl:choose>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"index\">\n"
						+ "    <xsl:choose>\n"
						+ "    <xsl:when test=\"($num_key_which_remain + 1) > $count\">\n"
						+ "    <xsl:value-of select=\"number($count)\"/>\n"
						+ "    </xsl:when>\n"
						+ "    <xsl:otherwise>\n"
						+ "    <xsl:value-of select=\"number($num_key_which_remain + 1)\"/>\n"
						+ "    </xsl:otherwise>\n"
						+ "    </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"only_dates_above\">\n"
						+ "        <xsl:for-each select=\"/identities/identity[@id=$as]/addressbook/card[@id=$card_id]/localkey/attribute::uniquepurge\">\n"
						+ "            <xsl:sort select=\".\" data-type=\"number\" order=\"descending\"/>\n"
						+ "            <xsl:if test=\"position() = $index\">\n"
						+ "                <xsl:value-of select=\".\"/>\n"
						+ "            </xsl:if>\n"
						+ "        </xsl:for-each>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"minimum_date\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"($num_key_which_remain + 1) > $count\">\n"
						+ "                <xsl:value-of select=\"$only_dates_above\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"$only_dates_above + 1\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">\n"
						+ ""
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">\n"
						+ ""
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ ""
						+ "            <xsl:if test=\"$local_key1_key!=''\">\n"
						+ "                <localkey uniquepurge=\"{$local_key1_date}\" value=\"{$local_key1_key}\" enc=\"{$local_key1_enc}\" trust_purge_unique=\"{$trust_purge_unique1}\"/>\n"
						+ "            </xsl:if>\n"
						+ ""
						+ "            <xsl:if test=\"$local_key2_key!=''\">\n"
						+ "                <localkey uniquepurge=\"{$local_key2_date}\" value=\"{$local_key2_key}\" enc=\"{$local_key2_enc}\" trust_purge_unique=\"{$trust_purge_unique2}\"/>\n"
						+ "            </xsl:if>\n"
						+ ""
						+ "            <xsl:if test=\"$local_key3_key!=''\">\n"
						+ "                <localkey uniquepurge=\"{$local_key3_date}\" value=\"{$local_key3_key}\" enc=\"{$local_key3_enc}\" trust_purge_unique=\"{$trust_purge_unique3}\"/>\n"
						+ "            </xsl:if>\n"
						+ ""
						+ "            <xsl:if test=\"$local_key4_key!=''\">\n"
						+ "                <localkey uniquepurge=\"{$local_key4_date}\" value=\"{$local_key4_key}\" enc=\"{$local_key4_enc}\" trust_purge_unique=\"{$trust_purge_unique4}\"/>\n"
						+ "            </xsl:if>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"/identities/identity[@id=$as]/addressbook/card[@id=$card_id]/localkey\">\n"
						+ "        <xsl:if test=\"../localkey[@uniquepurge>=$minimum_date]\">\n"
						+ "            <xsl:copy>\n"
						+ "                <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "            </xsl:copy>\n"
						+ "        </xsl:if>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void		set_card_key_removeByValue(String profile_id, String card_id_person, String key_to_be_removed) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as",							profile_id			});
		v.add(new String[] {"card_id",						card_id_person		});
		v.add(new String[] {"local_key_key_to_be_removed",	key_to_be_removed	});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"card_id\"/>\n"
						+ "    <xsl:param name=\"local_key_key_to_be_removed\"/>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">\n"
						+ "        <card>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </card>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]/localkey[@value=$local_key_key_to_be_removed]\"/>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]/invitation_key[@value=$local_key_key_to_be_removed]\"/>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void set_card_ToXstobesentByCardid(String profile_id, String card_id_person, String exchange_status_to_be_sent_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 				profile_id});
		v.add(new String[] {"card_id",			card_id_person});
		v.add(new String[] {"xs_to_be_sent",	exchange_status_to_be_sent_parameter});

		String xslt = //  xs_to_be_sent="{$xs_to_be_sent}"
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
						+ ""
						+ "    <xsl:param name=\"as\"/>"
						+ "    <xsl:param name=\"card_id\"/>"
						+ "    <xsl:param name=\"xs_to_be_sent\"/>"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">"
						+ "        <card xs_to_be_sent=\"{$xs_to_be_sent}\" name=\"{@name}\" id=\"{@id}\" exchange_status=\"{@exchange_status}\" phone=\"{@phone}\">"
						+ "            <xsl:apply-templates select=\"node()\"/>"
						+ "        </card>"
						+ "    </xsl:template>"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">"
						+ "        <xsl:copy>"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>"
						+ "        </xsl:copy>"
						+ "    </xsl:template>"
						+ ""
						+ "</xsl:stylesheet>";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

	private void set_card_ToNameIDSenderXSByCardid(String profile_id, String id_person, String name_person, String card_id_person, String exchange_status_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 				profile_id});
		v.add(new String[] {"id_sender",		id_person});
		v.add(new String[] {"name",				name_person});
		v.add(new String[] {"card_id",			card_id_person});
		v.add(new String[] {"exchange_status",	exchange_status_parameter});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
						+ ""
						+ "    <xsl:param name=\"as\"/>"
						+ "    <xsl:param name=\"id_sender\"/>"
						+ "    <xsl:param name=\"name\"/>"
						+ "    <xsl:param name=\"card_id\"/>"
						+ "    <xsl:param name=\"exchange_status\"/>"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/addressbook/card[@id=$card_id]\">"
						+ "        <card xs_to_be_sent=\"{@xs_to_be_sent}\" name=\"{$name}\" id=\"{$id_sender}\" exchange_status=\"{$exchange_status}\" phone=\"{@phone}\">"
						+ "            <xsl:apply-templates select=\"node()\"/>"
						+ "        </card>"
						+ "    </xsl:template>"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">"
						+ "        <xsl:copy>"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>"
						+ "        </xsl:copy>"
						+ "    </xsl:template>"
						+ ""
						+ "</xsl:stylesheet>";

		profile.ab.ab_store.performXMLJob(xslt, v);
	}

}
