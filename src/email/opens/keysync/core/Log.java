/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.io.File;

public final class Log {
	
	public final static int OFF 							= 0;
	public final static int TO_FILE 						= 1;
	public final static int TO_CONSOLE 						= 2;
	public final static int TO_FILE_AND_CONSOLE 			= 3;
	public final static int TO_FILE_TIMESTAMP				= 4;
	public final static int TO_CONSOLE_TIMESTAMP			= 5;
	public final static int TO_FILE_AND_CONSOLE_TIMESTAMP	= 6;
	
	public final static int VERBOSE_LOWEST			= 5;
	public final static int VERBOSE_CLASS			= 15;
	public final static int VERBOSE_OBJECT			= 20; // is used
	public final static int VERBOSE_METHOD			= 25;
	public final static int VERBOSE_VARS			= 30; // is used
	public final static int VERBOSE_VARS_AND_MORE	= 35; // is used
	public final static int VERBOSE_EXCEPTIONS		= 40; // is used
	public final static int VERBOSE_ALL				= 45; // is used

	public 	static boolean	mode_screen_allowed		= false; // package will generally do no screen output
	public	static int		line_number_msg			=  1;
	public	static int		line_number_addrbk_x	=  1;
	public	static int		line_number_xml			=  1;
	
	public final static int MODE_TO_SCREEN_ONLY		= 30; // if no screen_allowed, there is no output at all
	public final static int MODE_TO_SCREEN			= 25; // goes to screen and gol
	public final static int MODE_TO_GOL				= 20; // goes to gol only
	public final static int MODE_TO_MAILER			= 15; // counts line numbers for msgs, sequence: finding the item
	public final static int MODE_TO_XML				= 10; // counts line numbers for xml changes (by addressbook or inbox), seq: see above
	public final static int MODE_TO_ADDRESSBOOKx	=  9; // counts (later) line numbers for addressbook changes, that will cause addressbook sync, seq: see above
	public final static int MODE_PROGRESS			=  2; // idea to give a listener a percent string or progress info text
	public final static int MODE_RESERVED			=  1;

	public static String 	fn						= "log.txt";
	public static Integer	t	= TO_CONSOLE_TIMESTAMP; // indicates where to write logs to
	public static Integer	v	= 4; // level of being verbose - it is optional, but when given there
									 //		output will stay silent, if the number given is greater or
									 //		equal than what is configured in this variable
									 //   an example: in the first month of production you would like to
									 // 	have any method call (and of course object creation, class usage
									 //		logged, so set this to 25 (VERBOSE_METHOD) and you will not have
									 // 	all the var dumps in the logs anymore, but you can just keep
									 // 	them in the source code for developing the next evolution
									 //
									 // a greater "verbose" stands for giving very detailed info items
									 // a greater v value given shows more more details in the output
									 // 
									 // the default value 0 gives none of the output which has a verbose value
	
	/*
	 * 
	 * maybe enable this class to register an external logger, so that once registered here the logging
	 * will no be send to CONSOLE or FILE... but to this registered logger instead
	 * 
	 */

	public static 	String[]							ordered_keys_msg		= {"progress", "line_msg", "from", "to", "date", "time", "body", "comment"};
	public static 	String[]							ordered_keys_book		= {"line_book", "from", "spread_to", "to_spread_id", "time", "comment"};
	public static	HashSet<HashMap<String, String>>	collected_output		= new HashSet<HashMap<String, String>>();
	public static			HashMap<String, String>		an_output_map			= new HashMap<String, String>();

	public static	String								last_status				= "";
    public static          String          					pre         			= "\r|     |";
    public static          String          					pre2        			= "\r| ";
	
	// how to use "l":

	// *** l(text)			***: NO LINE AT ALL: ony output this to the screen, if it is allowed; else do not do any output

	// *** l(text, mode)	***: CAN FINISH LINE: add this text for key "comment" and either do not store it (...SCREEN_ONLY)
	//												or store it to collected_output with the option to add a specific key
	//												like "line_msg", "line_book" or "progress"

	// *** l(key, text)		***: PART OF A LINE: add a key/text to the line

	// collected_output is made up like a table, its lines are HashMaps, the keys of
	//								the HashMaps are the column titles, the values the cotent of a line's specific column

	// e.g. a HashMap for an email could have from=t@com.com to=c@com.com body=Moin line_msg=1
	//				once saving an email should be finished, after doing
	//						l(from, t@com.com)
	//						l(to, c@com.com)
	//						l(body, Moin)
	//					do a l("", MODE_TO_MAILER) (optionally instead of "" give a comment)
	//				this will give it a line number, adds the "completed" HashMap to
	//						HashSet which represents the collected_output
	//				it will create a new empty HashMap for e.g. a next mail or for an
	//						addressbook change
	
	// the collected_output will be a summary of what has been done e.g. during auto operation

	public static boolean l(String key, String text) {
		an_output_map.put(key, text);
		return true;
	}

	public static boolean l(String text, int mode) {
		l("comment", text);
		last_status = text.replace(pre, "");
		last_status = last_status.replace(pre2, "");
		if ( last_status.length() < 4 ) {
			last_status = "";
		}

		switch (mode) {
			case MODE_TO_SCREEN_ONLY: {
				if ( mode_screen_allowed ) {
					if ( an_output_map.size() == 1 ) {
						System.out.print(text);
					} else {
						for ( int i = 0 ; i < ordered_keys_msg.length ; i++ ) {
							String item = an_output_map.get(ordered_keys_msg[i]);
							System.out.print((item == null)?"":item);
						}
						//System.out.println();
					}
				} // this will not be added to the collected_output, as below the "an_output_map" is created new
				break;
			}
			case MODE_TO_SCREEN: {
				if ( an_output_map.size() == 1 ) {
					System.out.print(text);
				} else {
					for ( int i = 0 ; i < ordered_keys_book.length ; i++ ) {
						String item = an_output_map.get(ordered_keys_book[i]);
						System.out.print((item == null)?"":item);
					}
					System.out.println();
				}
				// here is by emphasis no "break", so it will also be added to the collected_output
			}
			case MODE_TO_GOL: {
				collected_output.add(an_output_map);
				break;
			}

			case MODE_TO_MAILER: {
				an_output_map.put("line_msg", String.valueOf(line_number_msg));
				collected_output.add(an_output_map);
				line_number_msg ++;
				break;
			}
			case MODE_TO_XML: {
				an_output_map.put("line_xml", String.valueOf(line_number_xml));
				collected_output.add(an_output_map);
				line_number_xml ++;
				break;
			}
			case MODE_PROGRESS: {
				HashMap<String, String> pro = new HashMap<String, String>();
				pro.put("progress", text);
				collected_output.add(pro);
				// option to call a listener
				break;
			}
		}

		an_output_map = new HashMap<String, String>();
		return true;
	}

	public static boolean l(String text) {
		return l(text, MODE_TO_SCREEN_ONLY);
	}

	public static boolean log (int verbose, String text) {
		if (verbose <= v) return log (text);
		else return false;
	}

	public static boolean log (String text) {
		// in case t == OFF there will be no output at all
		if ( t == TO_FILE_TIMESTAMP || t == TO_FILE_AND_CONSOLE_TIMESTAMP || t == TO_CONSOLE_TIMESTAMP || t == TO_FILE_AND_CONSOLE_TIMESTAMP ) {

			LocalDateTime 	currenttime = LocalDateTime.now();
			String 		s 			= String.valueOf(currenttime.getSecond());
			String 		mi 			= String.valueOf(currenttime.getMinute());
			String 		h 			= String.valueOf(currenttime.getHour());
			String 		mo 			= String.valueOf(currenttime.getMonthValue());
			String 		d 			= String.valueOf(currenttime.getDayOfMonth());
			String 		tme			= (1 == h.length() ? "0":"") + h + ":" + (1 == mi.length() ? "0":"") + mi + ":" + (1 == s.length() ? "0":"") + s + "/"
			+ String.valueOf(currenttime.getYear()).substring(2, 4) + (1 == mo.length() ? "0":"") + mo + (1 == d.length() ? "0":"") + d + " ";

			text = tme + text;
		}
		
		if ( t == TO_FILE || t == TO_FILE_AND_CONSOLE || t == TO_FILE_TIMESTAMP || t == TO_FILE_AND_CONSOLE_TIMESTAMP )
		{
			File f = new File(fn);
			try {
				FileInputStream i = new FileInputStream(f);
				byte[] logread = new byte[(int)f.length()];
				//int a = i.read(logread);
				i.close();
				
				byte[] logfile = new byte[logread.length + text.length()];
				FileOutputStream o = new FileOutputStream(f);
				o.write(logfile);
				o.close();
			} catch (Exception ex) {}
		}
		
		if ( t == TO_CONSOLE || t == TO_FILE_AND_CONSOLE || t == TO_CONSOLE_TIMESTAMP || t == TO_FILE_AND_CONSOLE_TIMESTAMP ) {
			System.out.println(text);
		}
		
		return true;
	}

}
