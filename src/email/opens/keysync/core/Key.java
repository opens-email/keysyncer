/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

public class Key {

	/*
	 * a key object can do any action that is necessary about keys in the application
	 * - a key that is stored after exchange (there can be none or up to MAX_KEY_NUM) has the "local/foreign" attribute, the purge date, the trust level and the value
	 * - a key value can be created
	 * - a key that is to be sent inside a message (there can be none or up to MAX_KEY_NUM) has the date and the value attribute only
	 * - a proofkey or a trykey is only one without a date
	 * - an invitation key has also a value, additionally a tanc and a date for expiry, it can move to get the tag invitation_key_exceeded
	 * 
	 */

	// in case MAX_KEY_NUM and GOOD_KEY_NUM should be higher than 4 and 3 the XSLT part needs to be considered
    public static final int MAX_KEY_NUM			= 4; // maximum number of keys accepted, any additional key will replace another or will be deleted
    public static final int GOOD_KEY_NUM		= 3; // the number of keys that is tried to be achieved

    public static final int NO 					= 1600000000; // highest level of "less trust" : trusted = "no"
    public static final int POTENTIALLY_YES		= 800000000;  // medium level of "less trust"  : trusted = "potentially yes"        lowest level of "less trust" is trusted = "yes", the value is 0 (or adding nothing)

	/* the definition of trust:

		each key has its specific sense in its name in front of the word "key":
			foreign key, local key, trykey (is an include(d) key), proofkey (is an include(d) key), invitation key
			the specific word describes the CREATION of the key:
				foreign (stored in a card) 						means it has been received and it has been CREATED by SOMEONE ELSE
				local 	(stored in a card) 						means it has been CREATED on the LOCAL computer where it is saved as local key
				try - 	(stored in an email's meta)				CREATING a trykey is TAKING it from the FOREIGN KEYS one has (detecting it being untrusted)
				proof - (stored in an email's meta)				CREATING a proofkey is TAKING it from the FOREIGN KEYS one has (detecting it being trusted)
				invitation (stored in an INVITATION STRING)		means this key has been CREATED to add it in an INVITATION STRING

		a foreign key is received and there are only 2 options for trust, depending IF there IS an included key:
				if there IS 		and it is verified with a local key formerly sent 	it IS TRUSTED		(no matter if it is try or proof)
				if there IS NOT 														it IS UNTRUSTED
				(if the key is not verified the there will be no foreign key created at all, so there is no trust information)
				=> the user who receives and saves the key in a card decides with which trust value the key gets stored

		a local key is created and then send, there are 3 options depending on the TRUST of a FOREIGN KEY INCLUDED
				if there is NO FOREIGN KEY at all		it is UNTRUSTED
				if there is an UNTRUSTED FOREIGN KEY	it is POTENTIALLY TRUSTED	(consequence of this one is to 	USE THIS TRUST ONLY TO SEND KEYS)
				if there is a TRUSTED FOREIGN KEY		it is TRUSTED				(this kind of trust can be 		USED FOR ANYTHING incl. SEND KEY, ID CHANGE AND TASKS)
				=> the user who creates and saves the key in a card decides with which trust value the key gets stored
				
				
		the value of trust is found in the attribute "trust_purge_unique", in the first 2 decimal digits with a "and 0x1100" done on it
				it is either 	16 	which means "UNTRUSTED" 			or
								8	which means "POTENTIALLY TRUSTED"	or
								0 	which means "TRUSTED"
	*/

	public String[] 				value	= new String [MAX_KEY_NUM];
    public String[] 				date	= new String [MAX_KEY_NUM];
    
    String					proofkey			= "";
    String					trykey				= "";
    
    String					invitationkey_value = "";
    String					invitationkey_tanc	= "";
	String					invitationkey_expiry= "";
	
	Card					card;
    
    boolean					received_ok;
    
    public Key () {
		
		card = null;

		for (int n = 0 ; n < (MAX_KEY_NUM) ; n++) {
			value[n]	= "";
			date[n]		= "";
		}
	}

	public Key (Card card_parameter) {
		card = card_parameter;
	}
	
    public Key (HashSet<HashMap<String, Object>> keys_from_meta_parameter, HashMap<String, Object> meta_elements_parameter) {
		
		card = null;

		for (int n = 0 ; n < (MAX_KEY_NUM) ; n++) {
			value[n]	= "";
			date[n]		= "";
		}
		
		proofkey	= (String) meta_elements_parameter.get("proofkey");
		trykey 		= (String) meta_elements_parameter.get("trykey");
		
		int n_keys = keys_from_meta_parameter.size();
		boolean keys_in_range_for_key_received		= (n_keys <= GOOD_KEY_NUM && ( !trykey.equals("") || !proofkey.equals("")) );
		boolean keys_in_range_for_nokey_received	= (n_keys <= MAX_KEY_NUM );

		int n_key = 0;
		if ( keys_in_range_for_key_received || keys_in_range_for_nokey_received ) {
			Iterator<HashMap<String, Object>> it_key = keys_from_meta_parameter.iterator();
			while (it_key.hasNext()) {
				HashMap<String, Object> key = it_key.next();
				value[n_key]	= (String) key.get("v");
				date[n_key++]	= (String) key.get("d");
			}
			received_ok = true;
		}
		else received_ok = false;
    }
    
	public String createValue() {
		
		String ret = "";
		
		for (int x = 0 ; x <= 6 ; x++) {
			String nxt = "";
			int nx = new Random().nextInt(16);
			if ( nx < 10 ) nxt = String.valueOf(nx);
			else {
				if ( nx == 10 ) nxt = "a";
				if ( nx == 11 ) nxt = "b";
				if ( nx == 12 ) nxt = "c";
				if ( nx == 13 ) nxt = "d";
				if ( nx == 14 ) nxt = "e";
				if ( nx == 15 ) nxt = "f";
			}
			ret = ret + nxt;
		}
		return ret;
	}
	
	class NextInfo {
		int nxt;
		int max;
	}

	boolean createLocalKeys(boolean create_keys_anyway) {

		int n_write	= 0;

		if ( card!= null ) {
			NextInfo			nx				= getNextFreeLocalKeyDate();
			Card.IncludeKeyInfo	iki				= lookup_include_key();
			int 				number_of_keys	= get_number_of_keys(card, iki);

			if ( number_is_in_range(nx, number_of_keys + 1 /* add one due to possible number_of_keys ++, see below*/ ) ) {

				if ( (number_of_keys > 0) || (create_keys_anyway) ) {
					int next_number		= nx.nxt;

					card.removeForeignKey(iki.includekey_value);

					if (  ( iki.includekey_trustcode.equals("yes") )  &&  ( card.getForeignKeyTrustedCount() < Key.GOOD_KEY_NUM )  ) {
						/* next_number */ number_of_keys ++;
					}

					for (n_write = 0 ; n_write < number_of_keys; n_write++) {
						if (next_number != 0) {
							value[n_write] = card.id + "@" + createValue();
							date [n_write] = String.valueOf(next_number++);
						}
					}
				}
			} // if the number cannot be in range or no number can be found, no keys will be created
		}
		return (n_write > 0);
	}

	static String get_df(long plus) {

		LocalDate 	currentdate = LocalDate.now().plusDays(plus);
		String 		m 			= String.valueOf(currentdate.getMonthValue());
		String 		d 			= String.valueOf(currentdate.getDayOfMonth());
		String 		df 			= String.valueOf(currentdate.getYear()).substring(3, 4) + (1 == m.length() ? "0":"") + m + (1 == d.length() ? "0":"") + d;
		
		return df;
	}

	private NextInfo getNextFreeLocalKeyDate() {
		NextInfo 	ni 			= new NextInfo();
					ni.max 		= 9999;

		String df = get_df(0);
		int next_number = Integer.valueOf(df) * 10000; // has the format ymmddnnnn where y is the year 202y, m the month, d the day and n the 4 digit number

		String date_number_highest = card.getLocalKeyHighest();
		
		int date_only	= 0;
		int ln			= date_number_highest.length();

		if (ln == 8) date_only = Integer.valueOf(date_number_highest.substring(0, 4));
		if (ln == 9) date_only = Integer.valueOf(date_number_highest.substring(0, 5));
		if ( date_only > 0 ) {
			if (Integer.valueOf(df) < date_only) { // purge date that shall be applied might already exist, check that
				next_number = 0;
				for (int n = 0 ; n < ni.max ; n++) {
					if ( card.uniqueDoesNotExistInLocal(df, n) ) {
						next_number = Integer.valueOf(df) * 10000 + n; n = ni.max;
					}
				}
			} else if (Integer.valueOf(df) == date_only) { // create next number or no date_number at all
				int number_highest = Integer.valueOf(date_number_highest.substring(ln-4, ln));
				if (number_highest < ni.max) next_number = next_number + 1 + number_highest;
				else next_number = 0;
			}
		}

		ni.nxt = next_number;

		return ni;
	}

	private boolean number_is_in_range(NextInfo nx_parameter, int how_many) {
		return nx_parameter.max <= (nx_parameter.nxt + how_many);
	}

	private Card.IncludeKeyInfo lookup_include_key() {

		Card.IncludeKeyInfo 	info 		= card.getForeignKeySelectedForInclude();

		if ( info.includekey_date.equals("") ) {
			info.includekey_trustcode 		= "no";
		}
		else {
			if (info.includekey_date_number < Key.NO) {
				info.includekey_trustcode 	= "yes";
			}
			else {
				info.includekey_trustcode 	= "potentially_yes";
			}
		}

		return info;
	}

	private int get_number_of_keys(Card card, Card.IncludeKeyInfo iki) {
		int count;
		String number_string;
		if ( iki.includekey_trustcode.equals("yes") ) {
			number_string = card.getLocalKeyTrustedCount();
		} else {
			number_string = card.getLocalKeyAnyCount();
		}

		if ( (number_string == null) || (number_string.equals("")) ) {
			count = 0;
		} else {
			count = Integer.valueOf(number_string);
		}

		return Key.GOOD_KEY_NUM - count;
	}

}
