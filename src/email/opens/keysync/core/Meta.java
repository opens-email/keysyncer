/*

	keysyncer
    Copyright (C) 2023  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.util.HashMap;
import java.util.HashSet;

import org.w3c.dom.Node;

public class Meta {
    
			Node 	DOM_node;

	public 	Key 	key_set;
	public	String	xs_to_be_sent;
	public	String	proof_key;
	public	String	try_key;
			Xs		xs_of_msg_card_id;
			String	phone;
			boolean key_included;
			String	id_sender;

	public	String	listenrequest_id		= "";
	public	String	spreadnote_id			= "";
	public	String	ls_phone				= "";
			String	keyinclude				= "";
			String	meta_exchange_status	= "";
	public	String	card_id					= "";
			boolean has_valid_data			= false;

	public Meta(Store_msg m_parameter) {
		id_sender	= m_parameter.id_sender;
	}

	void check_for_valid_meta_data_xml() {

        if (DOM_node != null) {
            HashMap<String, Object> meta_elements = Inbox.getMetaByNode(id_sender, DOM_node);

                                                            meta_exchange_status	= (String) meta_elements.get("exchange_status");
                                                            proof_key				= (String) meta_elements.get("proofkey");
                                                            try_key	 				= (String) meta_elements.get("trykey");

                                                            listenrequest_id		= (String) meta_elements.get("listenrequest_id");
                                                            spreadnote_id			= (String) meta_elements.get("spreadnote_id");
                                                            ls_phone				= (String) meta_elements.get("ls_phone");

                                                            listenrequest_id		= (listenrequest_id	!=	null)?listenrequest_id:	"";
                                                            spreadnote_id			= (spreadnote_id	!=	null)?spreadnote_id:	"";
                                                            ls_phone				= (ls_phone			!=	null)?ls_phone:			"";
                                                        
            if (!proof_key.equals(""))	keyinclude = proof_key;
            else									keyinclude = try_key;
            
            HashSet<HashMap<String, Object>> 	keys_from_meta	= Inbox.getKeysByNode("./key", DOM_node);
												key_set			= new Key(keys_from_meta, meta_elements);
                                                has_valid_data	= key_set.received_ok;
        }
    }

	public static String getXmlBody(Store_msg mg) {

		String xml_body_part01 = "<meta exchange_status=\"" + mg.meta.xs_to_be_sent + "\"";
		String xml_body_part02 = "";
		if ( (mg.meta.proof_key != null) && !mg.meta.proof_key.equals("") ) {
			xml_body_part02 = 						" proofkey=\"" + mg.meta.proof_key + "\"";
		}
		
		String xml_body_part03 = "";
		if ( (mg.meta.try_key != null) && !mg.meta.try_key.equals("") ) {
			xml_body_part03 = 								" trykey=\"" + mg.meta.try_key + "\"";
		}

		String xml_body_part04 = "";
		String xml_body_part05 = "";
		String xml_body_part06 = "";
		if ( (mg.meta.ls_phone != null) && !mg.meta.ls_phone.equals("") ) {
			xml_body_part04 = 										" listenrequest_id=\"" + mg.meta.listenrequest_id + "\"";
			xml_body_part05 = 											" spreadnote_id=\"" + mg.meta.spreadnote_id+ "\"";
			xml_body_part06 = 												" ls_phone=\"" + mg.meta.ls_phone + "\"";
		}

		String xml_body_part07 = 															"" /* / */ + ">\n";

		String xml_body_part08 = "";
		String xml_body_part09 = "";
		String xml_body_part10 = "";
		String xml_body_part11 = "";
		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[0] != null) && ( !mg.meta.key_set.value[0].equals("") ) ) {
			xml_body_part08 = "    <key v=\"" + mg.meta.key_set.value[0] + "\" d=\"" + mg.meta.key_set.date[0] + "\"/>\n";
		}

		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[1] != null) && ( !mg.meta.key_set.value[1].equals("") ) ) {
			xml_body_part09 = "    <key v=\"" + mg.meta.key_set.value[1] + "\" d=\"" + mg.meta.key_set.date[1] + "\"/>\n";
		}

		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[2] != null) && ( !mg.meta.key_set.value[2].equals("") ) ) {
			xml_body_part10 = "    <key v=\"" + mg.meta.key_set.value[2] + "\" d=\"" + mg.meta.key_set.date[2] + "\"/>\n";
		}

		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[3] != null) && ( !mg.meta.key_set.value[3].equals("") ) ) {
			xml_body_part11 = "    <key v=\"" + mg.meta.key_set.value[3] + "\" d=\"" + mg.meta.key_set.date[3] + "\"/>\n";
		}

		String xml_body_part12 = "</meta>\n";

		String xml_body = xml_body_part01 + xml_body_part02 + xml_body_part03 + xml_body_part04
						+ xml_body_part05 + xml_body_part06 + xml_body_part07 + xml_body_part08
						+ xml_body_part09 + xml_body_part10 + xml_body_part11 + xml_body_part12;

		return xml_body;
	}
}
