/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import email.opens.keysync.KeySyncer;

// Object Profile - has representation in XML as "identity"
// ==============
// general idea of objects which have a representation in the XML file:
// 		* usually an object is read from XML and analysed, as long as manipulations
//				are not to be written, it can be thrown away after that
//		* if a manipulation is done (and should be saved) it will be written to the dom representation
//				ALWAYS using KeySyncer.localstore... where "..." indicates [get|set] and the object in lowercase
//		* after all manipulations at the end of the KeySyncer.run method the dom will be
//					written to the XML file

// collection of all situations where localstore is used
// only standard methods used: readString, readNodeList, performXMLJob and .doc

// objects which use *store* Key (indeed does not!)
//		* Inbox			-> uses inbox_store
//		* Card			-> uses ab_store
//		* Addressbook	-> uses ab_store
//		* Xs			-> uses localstore and ab_store
// 		* Profile		-> uses localstore

public class Profile {

	public					String		id = null;
	public					Xs 			xs;
	public					Addressbook	ab;

	public Profile() {
		id	= KeySyncer.localstore.readString("/identities/identity[1]/@id");
	}

	public void setup_addressbook(){
		ab	= new Addressbook();
	}

	public class Person {
		public String id;
		public String name;
		public String phone;
	}

	public HashSet<String> getProfileIds() {

		HashSet<HashMap<String, Object>> 	id_map 	= get_profile_Ids();
		HashSet<String> 					ids 	= new HashSet<String>();

		Iterator<HashMap<String, Object>> 	it_map 	= id_map.iterator();

		while (it_map.hasNext()) {
			ids.add((String) it_map.next().get("id"));
		}

		return ids;
	}

	public String getIdFromPhone(String phone_parameter) {
		return get_profile_ByPhone_Id(phone_parameter);
	}

	public HashMap<String, Person> getProfileItems() {

		HashSet<HashMap<String, Object>> 	id_map 	= get_profile_Items();
		HashMap<String, Person> 			items 	= new HashMap<String, Person>();

		Iterator <HashMap<String, Object>>	it_map = id_map.iterator();

		while ( it_map.hasNext() ) {
			HashMap<String, Object> single_profile = it_map.next();
			Person p = new Person();

			p.id	= (String) single_profile.get("id");
			p.name	= (String) single_profile.get("name");
			p.phone	= (String) single_profile.get("phone");

			items.put(p.id, p); // .add( (String) it_map.next().get("id"));
		}

		return items;
	}

	public void newItem(String name_parameter, String id_parameter, String phone_parameter) {
		set_profile_newItem(name_parameter, id_parameter, phone_parameter);
	}

	public void setXs(String profile_id, String xs_aspired) {
		set_profile_xs(profile_id, xs_aspired);
	}

	public void setIdXsByPhone(String invited_phone, String id, String xs) {
		set_profile_ByPhoneToIdXs(invited_phone, id, xs);
	}

	public void switchItem(String remove_profile_id_parameter, String profile_id_parameter) {
		set_profile_switch_id(remove_profile_id_parameter, profile_id_parameter);
	}

	/*
			Profile interface methods
	*******************************************************************************************************************
				The following methods ONLY interact with xml structure
									  ONLY  			 xml structure				_G_ETTER	SPEC object		String, HashSet
String                              get_profile_ByPhone_Id(String phone_parameter)
HashSet<HashMap<String, Object>>    get_profile_Ids()
HashSet<HashMap<String, Object>>    get_profile_Items()
									  ONLY  			 xml structure				_S_ETTER	SPEC object
void		                        set_profile_ByPhoneToIdXs(String phone_parameter, String profile_id_parameter, String xs_string_parameter)
void                                set_profile_newItem(String name_parameter, String id_parameter, String phone_parameter)
void                                set_profile_xs(String profile_id, String xs)
	*******************************************************************************************************************
	 */

	private String get_profile_ByPhone_Id(String phone_parameter) {

		return KeySyncer.localstore.readString("/identities/identity[@phone='" + phone_parameter + "']/@id");
	}

	private HashSet<HashMap<String, Object>> get_profile_Ids() {
		String 	path 	= 			"/identities/identity";

		HashSet<String[]> par = new HashSet<String[]>();
		String[] i = {"./attribute::id",	"STRING",	"id"};			par.add(i);

		return KeySyncer.localstore.readNodeList (path, KeySyncer.localstore.doc, par);
	}

	private HashSet<HashMap<String, Object>> get_profile_Items() {
		String 	path 	= 			"/identities/identity";

		HashSet<String[]> par = new HashSet<String[]>();
		String[] i		= {"./attribute::id",		"STRING",	"id"};			par.add(i);
		String[] name	= {"./attribute::name",		"STRING",	"name"};		par.add(name);
		String[] phone	= {"./attribute::phone",	"STRING",	"phone"};		par.add(phone);

		return KeySyncer.localstore.readNodeList (path, KeySyncer.localstore.doc, par);
	}

	private void		set_profile_ByPhoneToIdXs(String phone_parameter, String profile_id_parameter, String xs_string_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"phone",	phone_parameter});
		v.add(new String[] {"as", 		profile_id_parameter});
		v.add(new String[] {"value",	xs_string_parameter});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"phone\"/>\n"
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"value\"/>\n"
						+ "\n"
						+ "    <xsl:template match=\"identity[@phone=$phone]\">\n"
						+ "        <identity exchange_status=\"{$value}\" id=\"{$as}\" name=\"{@name}\" number=\"{@number}\" phone=\"{@phone}\">\n"
						+ "            <xsl:apply-templates select=\"node()\"/>\n"
						+ "        </identity>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		KeySyncer.localstore.performXMLJob(xslt, v);
	}

	private void     set_profile_newItem(String name_parameter, String id_parameter, String phone_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"id", 			id_parameter});
		v.add(new String[] {"name",			name_parameter});
		v.add(new String[] {"phone",		phone_parameter});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"id\"/>\n"
						+ "    <xsl:param name=\"name\"/>\n"
						+ "    <xsl:param name=\"phone\"/>\n"
						+ "\n"
						+ "    <xsl:variable name=\"max\">\n"
						+ "        <xsl:for-each select=\"/identities/identity/attribute::number\">\n"
						+ "            <xsl:sort select=\".\" data-type=\"number\" order=\"descending\"/>\n"
						+ "            <xsl:if test=\"position() = 1\"><xsl:value-of select=\".\"/></xsl:if>\n"
						+ "        </xsl:for-each>\n"
						+ "    </xsl:variable>\n"
						+ "\n"
						+ "    <xsl:variable name=\"max_num\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"number($max) = $max\">\n"
						+ "                <xsl:value-of select=\"$max\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"-1\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "                <identity id=\"{$id}\" name=\"{$name}\" number=\"{$max_num+1}\" phone=\"{$phone}\">\n"
						+ "                </identity>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		KeySyncer.localstore.performXMLJob(xslt, v);
	}

	private void set_profile_xs(String profile_id, String xs) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 			profile_id});
		v.add(new String[] {"value",		xs});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"value\"/>\n"
						+ "\n"
						+ "    <xsl:template match=\"identity[@id=$as]\">\n"
						+ "        <identity exchange_status=\"{$value}\" id=\"{@id}\" name=\"{@name}\" number=\"{@number}\" phone=\"{@phone}\">\n"
						+ "            <xsl:apply-templates select=\"node()\"/>\n"
						+ "        </identity>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		KeySyncer.localstore.performXMLJob(xslt, v);
	}

	private void set_profile_switch_id(String remove_profile_id_parameter, String profile_id_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"old_profile_id", 		remove_profile_id_parameter});
		v.add(new String[] {"profile_id", 			profile_id_parameter });

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"old_profile_id\"/>\n"
						+ "    <xsl:param name=\"profile_id\"/>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$old_profile_id]/inbox/@id\">\n"
						+ "       <xsl:attribute name=\"id\">\n"
						+ "           <xsl:value-of select=\"$profile_id\"/>\n"
						+ "       </xsl:attribute>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities/identity/inbox[@id=$old_profile_id]/@id\">\n"
						+ "       <xsl:attribute name=\"id\">\n"
						+ "           <xsl:value-of select=\"$profile_id\"/>\n"
						+ "       </xsl:attribute>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		KeySyncer.localstore.performXMLJob(xslt, v);
	}

}
