/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

import email.opens.keysync.KeySyncer;

// Object Xs - has representation in XML as "xs"
// ==============
// general idea of objects which have a represention in the XML file:
// 		* usually an object is read from XML and analysed, as long as manipulations
//				are not to be written, it can be thrown away after that
//		* if a manipulation is done (and sould be saved) it will be written to the dom representation
//				ALWAYS using KeySyncer.store... where "..." indicates [get|set] and the object in lowercase
//				e.g. KeySyncer.store.get_msg_meta..., exceptions are listed here: none
//		* after all manipulations at the end of the KeySyncer.run method the dom will be
//					written to the XML file

// collection of all situations where Profile.store. is used (complete list of objects found in Profile.java):
// 		* KeySyncer.store.get_xs_profile(profile_id);
// 		* KeySyncer.store.get_xs_cardOfProfile(card_id, m.profile.id);

public class Xs {

    static String[] all_xs = { // all possible exchange statuses
			"automatic_listen_spread",		// 0  a "" was "automatic" before
			"automatic_listen",				// 1  a "" was "automatic_no_spread" before
    		"automatic",					// 2  a "" was "automatic_no_spread_no_need" before
			"manual",						// 3
			"no",							// 4  n
			"",								// 5  n
			"not_at_all"					// 6  n
			};

	static int	automatic_listen_spread = 0;

	static int [] automatic		 		= {0,3}	; // automatic		range
	static int [] automatic_listen		= {1,3}	; // automatic		range
	static int [] exchange				= {0,4}	; // exchange		range 
	static int [] no_exchange		 	= {4,7}	; // no_exchange	range 
	
	static int 		manual_index		= 3		;

	public static String	auto_listen_spread	= all_xs[automatic_listen_spread];
	public static String	manual				= all_xs[manual_index];
    
    public static HashSet<String> any_automatic = new HashSet<String>(Arrays.asList(Arrays.copyOfRange(all_xs, automatic		[0], automatic   	 [1])));
    static HashSet<String> any_exchange 		= new HashSet<String>(Arrays.asList(Arrays.copyOfRange(all_xs, exchange			[0], exchange	 	 [1])));
    static HashSet<String> any_no_exchange		= new HashSet<String>(Arrays.asList(Arrays.copyOfRange(all_xs, no_exchange		[0], no_exchange 	 [1])));
    static HashSet<String> any_automatic_listen	= new HashSet<String>(Arrays.asList(Arrays.copyOfRange(all_xs, automatic_listen	[0], automatic_listen[1])));

	public boolean intends_exchange;
	boolean intends_no_automatic;
	public String string;
	
	Xs(Store_msg m_parameter) {
		getIntentionOfProfileIdsCardId(m_parameter); // looks into the addressbook of the profile_id in msg and then get the XS of the msg's card_id
	}
	
	public Xs(String profile_id) {
		string = get_xs_profile(profile_id);
		intends_exchange = intends_exchange(string);
	}

    public static String any_automatic_path(String status) {
    	String ret = "";
		Iterator<String> it = any_automatic.iterator();
		while (it.hasNext()) {
			if ( !ret.equals("") ) ret = ret + "or";
			ret = ret + "@" + status + "='" + it.next().toString() + "'";
		}
		return ret;
    }
    
    static String any_automatic_listen_path(String status) {
    	String ret = "";
		Iterator<String> it = any_automatic_listen.iterator();
		while (it.hasNext()) {
			if ( !ret.equals("") ) ret = ret + "or";
			ret = ret + "@" + status + "='" + it.next().toString() + "'";
		}
		return ret;
    }

	private boolean getIntentionOfProfileIdsCardId(Store_msg m) {
		String 	card_id					= m.getId4Name();
		string					= get_xs_cardOfProfile(card_id, m.profile.id);
		intends_exchange 		= intends_exchange(string);
		intends_no_automatic	= intends_no_automatic(string);

		return intends_exchange;
	}

	private boolean intends_exchange(String xs) {
		return any_exchange.contains(xs);
	}

	public boolean intends_no_automatic(String xs) {
		return !any_automatic.contains(xs);
	}

	private String get_xs_profile(String profile_id_parameter) {
		String attr = "exchange_status";
		return KeySyncer.localstore.readString("/identities/identity[@id='" + profile_id_parameter + "']/attribute::" + attr);
	}

	private String get_xs_cardOfProfile(String card_id_parameter, String profile_id_parameter) {
		String attr = "exchange_status";
		return KeySyncer.profile.ab.ab_store.readString("/identities/identity[@id='" + profile_id_parameter + "']/addressbook/card[@id='" + card_id_parameter + "']/@" + attr);
	}

}
