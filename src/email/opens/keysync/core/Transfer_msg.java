/*

	keysyncer
    Copyright (C) 2025  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import email.opens.keysync.io.Send;

import email.opens.keysync.KeySyncer;

// Object Transfer_msg (extends Store_msg - has either representation in XML as "msg" or is sent by e.g. email)
//			This object is made to transfer a msg: put into the Store/Inbox of someone else (e.g. xml, email, others...)
// ==============
// general idea of objects which have a represention in the XML file:
// 		* usually an object is read from XML and analysed, as long as manipulations
//				are not to be written, it can be thrown away after that
//		* if a manipulation is done (and sould be saved) it will be written to the dom representation
//				ALWAYS using KeySyncer.store... where "..." indicates [get|set] and the object in lowercase
//				e.g. KeySyncer.store.get_msg_meta..., exceptions are listed here: none
//		* after all manipulations at the end of the KeySyncer.run method the dom will be
//					written to the XML file

// collection of all situations where KeySyncer.inbox_store. is used (complete list of objects found in Profile.java):
// 		* KeySyncer.inbox_store.set_msg_outgoing(mg, will_send_meta);

public class Transfer_msg extends Store_msg implements Send {
	
	public				String	id_localkey_owner	= "";			//	-> Addrbk
	public				String	id_foreignkey_owner	= "";			//	-> Addrbk
    static final    	String	open_tag    		= ".·.";		//	-> Addrbk
    static final    	String	close_tag   		= "·.·";		//	-> Addrbk
    public static final	String	pre_open    		= "--\n.·.\n";	//	-> Addrbk
    public static final	String	pre_close   		= "·.·\n-\n";	//	-> Addrbk

	public Transfer_msg() {
		init_Store_msg();
	}

	public Transfer_msg(String from_id_parameter, String to_id_parameter, String text_parameter) {

		init_Store_msg();
		id_localkey_owner	= from_id_parameter;
		id_foreignkey_owner	= to_id_parameter;
		text = text_parameter; // name of the person to send a message to
	}

	public Transfer_msg(String profile_id, String send_to_id, Card.IncludeKeyInfo ik_info, Key localKey, String xs_to_be_sent_parameter) {

		init_Store_msg();
		id_localkey_owner	= profile_id;
		id_foreignkey_owner	= send_to_id;

		meta.try_key = "";
		meta.proof_key = "";

		if ( ik_info.includekey_trustcode.equals("yes") ) {
			meta.proof_key	= ik_info.includekey_value;
		} else if ( ik_info.includekey_trustcode.equals("potentially_yes") ) {
			meta.try_key		= ik_info.includekey_value;
		}

		meta.xs_to_be_sent = xs_to_be_sent_parameter;

		if ( ( meta.xs_to_be_sent.equals("") ) || ( !"".equals( meta.try_key + meta.proof_key ) ) ) {
			meta.xs_to_be_sent = profile.xs.string;
		}

		meta.key_set = localKey;
	}

		public void set_msg_outgoing(Transfer_msg m, String will_send_meta_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as",				m.id_localkey_owner});
		v.add(new String[] {"to_id",			m.id_foreignkey_owner});
		v.add(new String[] {"text",				m.text});
		v.add(new String[] {"exchange_status",	m.meta.xs_to_be_sent});
		v.add(new String[] {"will_send_meta",	will_send_meta_parameter});

		v.add(new String[] {"listenrequest_id",	m.meta.listenrequest_id});
		v.add(new String[] {"spreadnote_id",	m.meta.spreadnote_id});
		v.add(new String[] {"ls_phone",			m.meta.ls_phone});

		boolean wsmp = !will_send_meta_parameter.equals("yes");

		for (int n = 1; n <= Key.MAX_KEY_NUM; n++) {
			v.add(new String[] {"local_key" 	+ n, wsmp?"":m.meta.key_set.value	[n-1]});
			v.add(new String[] {"local_date"  	+ n, wsmp?"":m.meta.key_set.date	[n-1]});
		}

		v.add(new String[] {"my_included_key",		m.meta.proof_key});
		v.add(new String[] {"my_included_try_key",	m.meta.try_key}	);

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"to_id\"/>\n"
						+ "    <xsl:param name=\"text\"/>\n"
						+ "    <xsl:param name=\"will_send_meta\"/>\n"
						+ "    <xsl:param name=\"exchange_status\"/>\n"


						+ "    <xsl:param name=\"listenrequest_id\"/>\n"
						+ "    <xsl:param name=\"spreadnote_id\"/>\n"
						+ "    <xsl:param name=\"ls_phone\"/>\n"


						+ "    <xsl:param name=\"local_key1\"/>\n"
						+ "    <xsl:param name=\"local_key2\"/>\n"
						+ "    <xsl:param name=\"local_key3\"/>\n"
						+ "    <xsl:param name=\"local_key4\"/>\n"
						+ "    <xsl:param name=\"local_date1\"/>\n"
						+ "    <xsl:param name=\"local_date2\"/>\n"
						+ "    <xsl:param name=\"local_date3\"/>\n"
						+ "    <xsl:param name=\"local_date4\"/>\n"
						+ "    <xsl:param name=\"my_included_key\"/>\n"
						+ "    <xsl:param name=\"my_included_try_key\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"max\">\n"
						+ "        <xsl:for-each select=\"/identities/identity[@id=$to_id]/inbox/msg/attribute::number\">\n"
						+ "            <xsl:sort select=\".\" data-type=\"number\" order=\"descending\"/>\n"
						+ "            <xsl:if test=\"position() = 1\"><xsl:value-of select=\".\"/></xsl:if>\n"
						+ "        </xsl:for-each>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"max_num\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"number($max) = $max\">\n"
						+ "                <xsl:value-of select=\"$max\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"-1\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$to_id]/inbox\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "                <msg cardid=\"{$as}\" name=\"\" number=\"{$max_num+1}\" text=\"{$text}\">\n"
						+ "                    <xsl:if test=\"$will_send_meta='yes'\">\n"
						+ "                        <meta exchange_status=\"{$exchange_status}\">\n"
						+ "                        <xsl:choose>\n"
						+ "                            <xsl:when test=\"$my_included_key!=''\">\n"
						+ "                                <xsl:attribute name=\"proofkey\">\n"
						+ "                                    <xsl:value-of select=\"$my_included_key\"/>\n"
						+ "                                </xsl:attribute>\n"
						+ "                            </xsl:when>\n"
						+ "                            <xsl:otherwise>\n"
						+ "                                <xsl:if test=\"$my_included_try_key!=''\">\n"
						+ "                                    <xsl:attribute name=\"trykey\">\n"
						+ "                                        <xsl:value-of select=\"$my_included_try_key\"/>\n"
						+ "                                    </xsl:attribute>\n"
						+ "                                </xsl:if>\n"
						+ "                            </xsl:otherwise>\n"
						+ "                        </xsl:choose>\n"

						+ "                                <xsl:if test=\"$ls_phone!=''\">\n"

						+ "                        <xsl:attribute name=\"listenrequest_id\">\n"
						+ "                            <xsl:value-of select=\"$listenrequest_id\"/>\n"
						+ "                        </xsl:attribute>\n"
						+ "                        <xsl:attribute name=\"spreadnote_id\">\n"
						+ "                            <xsl:value-of select=\"$spreadnote_id\"/>\n"
						+ "                        </xsl:attribute>\n"
						+ "                        <xsl:attribute name=\"ls_phone\">\n"
						+ "                            <xsl:value-of select=\"$ls_phone\"/>\n"
						+ "                        </xsl:attribute>\n"

						+ "                                </xsl:if>\n"

						+ "                        <xsl:if test=\"$local_key1!=''\">\n"
						+ "                            <key v=\"{$local_key1}\" d=\"{$local_date1}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        <xsl:if test=\"$local_key2!=''\">\n"
						+ "                            <key v=\"{$local_key2}\" d=\"{$local_date2}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        <xsl:if test=\"$local_key3!=''\">\n"
						+ "                            <key v=\"{$local_key3}\" d=\"{$local_date3}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        <xsl:if test=\"$local_key4!=''\">\n"
						+ "                            <key v=\"{$local_key4}\" d=\"{$local_date4}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        </meta>\n"
						+ "                    </xsl:if>\n"
						+ "                </msg>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		KeySyncer.inbox_store.performXMLJob(xslt, v);
	}

	public static String getXmlBodyFromMeta(Transfer_msg mg) {

		String xml_body_part01 = "<meta exchange_status=\"" + mg.meta.xs_to_be_sent + "\"";
		String xml_body_part02 = "";
		if ( (mg.meta.proof_key != null) && !mg.meta.proof_key.equals("") ) {
			xml_body_part02 = 						" proofkey=\"" + mg.meta.proof_key + "\"";
		}

		String xml_body_part03 = "";
		if ( (mg.meta.try_key != null) && !mg.meta.try_key.equals("") ) {
			xml_body_part03 = 								" trykey=\"" + mg.meta.try_key + "\"";
		}

		String xml_body_part04 = "";
		String xml_body_part05 = "";
		String xml_body_part06 = "";
		if ( (mg.meta.ls_phone != null) && !mg.meta.ls_phone.equals("") ) {
			xml_body_part04 = 										" listenrequest_id=\"" + mg.meta.listenrequest_id + "\"";
			xml_body_part05 = 											" spreadnote_id=\"" + mg.meta.spreadnote_id+ "\"";
			xml_body_part06 = 												" ls_phone=\"" + mg.meta.ls_phone + "\"";
		}

		String xml_body_part07 = 															"" /* / */ + ">\n";

		String xml_body_part08 = "";
		String xml_body_part09 = "";
		String xml_body_part10 = "";
		String xml_body_part11 = "";
		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[0] != null) && ( !mg.meta.key_set.value[0].equals("") ) ) {
			xml_body_part08 = "    <key v=\"" + mg.meta.key_set.value[0] + "\" d=\"" + mg.meta.key_set.date[0] + "\"/>\n";
		}

		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[1] != null) && ( !mg.meta.key_set.value[1].equals("") ) ) {
			xml_body_part09 = "    <key v=\"" + mg.meta.key_set.value[1] + "\" d=\"" + mg.meta.key_set.date[1] + "\"/>\n";
		}

		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[2] != null) && ( !mg.meta.key_set.value[2].equals("") ) ) {
			xml_body_part10 = "    <key v=\"" + mg.meta.key_set.value[2] + "\" d=\"" + mg.meta.key_set.date[2] + "\"/>\n";
		}

		if ( (mg.meta.key_set != null) && (mg.meta.key_set.value[3] != null) && ( !mg.meta.key_set.value[3].equals("") ) ) {
			xml_body_part11 = "    <key v=\"" + mg.meta.key_set.value[3] + "\" d=\"" + mg.meta.key_set.date[3] + "\"/>\n";
		}

		String xml_body_part12 = "</meta>\n";

		String xml_body = xml_body_part01 + xml_body_part02 + xml_body_part03 + xml_body_part04
				+ xml_body_part05 + xml_body_part06 + xml_body_part07 + xml_body_part08
				+ xml_body_part09 + xml_body_part10 + xml_body_part11 + xml_body_part12;

		return xml_body;
	}

	void updateWithListenSpread(String profile_id_parameter, String to_id_parameter, String listenrequest_id_parameter, String spreadnote_id_parameter, String ls_phone_parameter) {

		id_localkey_owner		= profile_id_parameter;
		id_foreignkey_owner		= to_id_parameter;
		meta.listenrequest_id	= listenrequest_id_parameter;
		meta.spreadnote_id		= spreadnote_id_parameter;
		meta.ls_phone			= ls_phone_parameter;
	}

	public static void merge(HashSet<Transfer_msg> ms, Transfer_msg m) {

		Iterator<Transfer_msg> it_ms = ms.iterator();

		boolean isMerged = false;
		while ( it_ms.hasNext() ) {
			Transfer_msg ms_next = it_ms.next();

			if ( ms_next.id_foreignkey_owner.equals(m.id_foreignkey_owner) ) {

				if ( m.meta.key_set != null && m.meta.key_set.value.length > 0) {
					if ( (ms_next.meta.key_set == null ) || (ms_next.meta.key_set.value.length < 1) ) {
						ms_next.text			= (ms_next.text==null)?"":ms_next.text + ( (m.text==null)?"":m.text );
						ms_next.meta.key_set		= m.meta.key_set;
						ms_next.meta.proof_key		= m.meta.proof_key;
						ms_next.meta.try_key		= m.meta.try_key;
						ms_next.meta.xs_to_be_sent	= m.meta.xs_to_be_sent;
						isMerged				= true; // only keys in new msg, so they are merged, now
					} // else means there are keys in the exsiting msg and in the msg to add: do not merge
				}
				else { // only keys in the existing msg, so merge texts
					ms_next.text		= (ms_next.text==null)?"":ms_next.text + ( (m.text==null)?"":m.text );
					isMerged = true;
				}

				// merge each field ( Key / key_in_msg ) (String / text) (String / proof_key) (String / try_key) (String xs_to_be_sent)
			}
		}

		if ( !isMerged ) {
			ms.add(m);
		}
	}

	static HashSet<Transfer_msg> mergeAll(HashSet<Transfer_msg> ms, HashSet<Transfer_msg> m) {

		HashSet<Transfer_msg> msg_set = ms;

		Iterator<Transfer_msg> it_m = m.iterator();

		while ( it_m.hasNext() ) {
			merge(ms, it_m.next());
		}

		return msg_set;
	}

}
