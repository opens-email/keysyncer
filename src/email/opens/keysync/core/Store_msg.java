/*

	keysyncer
    Copyright (C) 2023  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import email.opens.keysync.KeySyncer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// Object Store_msg (extends Msg - has representation in XML as "msg")
//			This object is made to put a msg into the Store (e.g. xml, later maybe IMAP draft)
// ==============
// general idea of objects which have a represention in the XML file:
// 		* usually an object is read from XML and analysed, as long as manipulations
//				are not to be written, it can be thrown away after that
//		* if a manipulation is done (and sould be saved) it will be written to the dom representation
//				ALWAYS using KeySyncer.store... where "..." indicates [get|set] and the object in lowercase
//				e.g. KeySyncer.inbox_store.get_msg_meta..., exceptions are listed here: none
//		* after all manipulations at the end of the KeySyncer.run method the dom will be
//					written to the XML file

// collection of all situations where KeySyncer.inbox_store. is used (complete list of objects found in Profile.java):
// 		* KeySyncer.inbox_store.set_msg_outgoing(mg, will_send_meta);

public class Store_msg {

	public 	Profile 				profile			= KeySyncer.profile;
	public	String					id_sender;
	public 	String					text;
	boolean 						to_be_deleted	= false;
	public	Meta					meta;
	static DocumentBuilderFactory 	factory   		= DocumentBuilderFactory.newInstance();
	static DocumentBuilder 			builder;
	public 	String					number;

	public				String	id_localkey_owner	= "";			//	-> Addrbk
	public				String	id_foreignkey_owner	= "";			//	-> Addrbk
    static final    	String	open_tag    		= ".·.";		//	-> Addrbk
    static final    	String	close_tag   		= "·.·";		//	-> Addrbk
    public static final	String	pre_open    		= "--\n.·.\n";	//	-> Addrbk
    public static final	String	pre_close   		= "·.·\n-\n";	//	-> Addrbk

	public Store_msg() {
		init_Store_msg();
	}

	void init_Store_msg() {

		id_sender	= "";
		meta 		= new Meta(this);
	}
	
    public static class TextMsgStruct {

        public String from;
		public String to;
        public String subject;
        public String body;
    }

	public static void post_send(HashSet<Store_msg> m) {
		post_send(m, true);
	}

	public static void post_send(HashSet<Store_msg> m, boolean log_it) {

		if (m.size() > 0) {

			Iterator<Store_msg> it = m.iterator();

			while (it.hasNext()) {
				Store_msg mg = it.next();

				if ( 		(mg.meta.key_set != null)
						&& 	(mg.meta.key_set.value.length > 0)
						&& 	( !"".equals(mg.meta.key_set.value[0]))
					) {
					// in case an exchange_status change is goin on...
					if (  ( (mg.meta.proof_key != null) && (!mg.meta.proof_key.equals("")) ) || ( (mg.meta.xs_to_be_sent != null) && (mg.meta.xs_to_be_sent.equals(Xs.manual)) )  ) {
						new Card(mg.id_foreignkey_owner).updateXsToBeSentByCardid(mg.id_foreignkey_owner, "");
					}
				}

				if (log_it) logMsg(mg);
			}
		}
	}

	public static HashSet<Store_msg> addrbk2store(HashSet<Transfer_msg> m_add) {

		HashSet<Store_msg> m_store = new HashSet<Store_msg>();

		Iterator<Transfer_msg> it = m_add.iterator();
		while (it.hasNext()) {
			m_store.add( (Store_msg) it.next());
		}

		return m_store;
	}

	public static Store_msg getMsgFromTextMsg(TextMsgStruct item_parameter) {

		String		text	= item_parameter.body;
		Store_msg	m_ret	= null;

		Store_msg	m		= new Store_msg();
		//m.init();
		m.id_localkey_owner = item_parameter.from;
		m.id_foreignkey_owner = item_parameter.to;

		if ( text.contains(open_tag) && text.contains(close_tag) ) {

			int			pos_start   = text.indexOf(open_tag) + open_tag.length();
			int			pos_end     = text.indexOf(close_tag) - 1;

			String		meta		= text.substring(pos_start, pos_end);

			try {
				builder = factory.newDocumentBuilder();
				m.meta.DOM_node = (builder.parse(new InputSource(new StringReader(meta)))).getFirstChild();
			} catch (ParserConfigurationException | SAXException | IOException e) {
				Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
			}

			m.meta.check_for_valid_meta_data_xml();

			String cut = pre_open.substring(0, 2);
			if ( pos_start > 0 ) {
				m.text = text.substring(0, pos_start - 1);
				if ( m.text.startsWith(cut) && m.text.length() < 2 * pre_open.length() ) {
					m.text = "";
				}
			}

		}

		m_ret = m;

		return m_ret;
	}

	public static void msg_store_loop(HashSet<Store_msg> ms) {
    
		if (ms.size() > 0) {
			Iterator<Store_msg> it = ms.iterator();
			while (it.hasNext()) {
				Store_msg mg = it.next();

				String will_send_meta = "no";					// ist die lenth nicht IMMER 4?
				if ( 		(mg.meta.key_set != null)
						&& 	(mg.meta.key_set.value.length > 0)
						&& 	( !"".equals(mg.meta.key_set.value[0]))
					) {
						mg.meta.xs_to_be_sent = mg.meta.meta_exchange_status;
										will_send_meta = "yes";
				}

				mg.set_msg_inbox_store(mg, will_send_meta); // ((Transfer_msg) mg).set_msg_outgoing( (Transfer_msg) mg, will_send_meta);
			}
		}
	}

	public void set_msg_inbox_store(Store_msg m, String will_send_meta_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as",				m.id_localkey_owner});
		v.add(new String[] {"to_id",			m.id_foreignkey_owner});
		v.add(new String[] {"text",				m.text});
		v.add(new String[] {"exchange_status",	m.meta.xs_to_be_sent});
		v.add(new String[] {"will_send_meta",	will_send_meta_parameter});

		v.add(new String[] {"listenrequest_id",	m.meta.listenrequest_id});
		v.add(new String[] {"spreadnote_id",	m.meta.spreadnote_id});
		v.add(new String[] {"ls_phone",			m.meta.ls_phone});

		boolean wsmp = !will_send_meta_parameter.equals("yes");

		for (int n = 1; n <= Key.MAX_KEY_NUM; n++) {
			v.add(new String[] {"local_key" 	+ n, wsmp?"":m.meta.key_set.value	[n-1]});
			v.add(new String[] {"local_date"  	+ n, wsmp?"":m.meta.key_set.date	[n-1]});
		}

		v.add(new String[] {"my_included_key",		m.meta.proof_key});
		v.add(new String[] {"my_included_try_key",	m.meta.try_key}	);

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"to_id\"/>\n"
						+ "    <xsl:param name=\"text\"/>\n"
						+ "    <xsl:param name=\"will_send_meta\"/>\n"
						+ "    <xsl:param name=\"exchange_status\"/>\n"


						+ "    <xsl:param name=\"listenrequest_id\"/>\n"
						+ "    <xsl:param name=\"spreadnote_id\"/>\n"
						+ "    <xsl:param name=\"ls_phone\"/>\n"


						+ "    <xsl:param name=\"local_key1\"/>\n"
						+ "    <xsl:param name=\"local_key2\"/>\n"
						+ "    <xsl:param name=\"local_key3\"/>\n"
						+ "    <xsl:param name=\"local_key4\"/>\n"
						+ "    <xsl:param name=\"local_date1\"/>\n"
						+ "    <xsl:param name=\"local_date2\"/>\n"
						+ "    <xsl:param name=\"local_date3\"/>\n"
						+ "    <xsl:param name=\"local_date4\"/>\n"
						+ "    <xsl:param name=\"my_included_key\"/>\n"
						+ "    <xsl:param name=\"my_included_try_key\"/>\n"
						+ ""
						+ "    <xsl:variable name=\"max\">\n"
						+ "        <xsl:for-each select=\"/identities/identity[@id=$to_id]/inbox/msg/attribute::number\">\n"
						+ "            <xsl:sort select=\".\" data-type=\"number\" order=\"descending\"/>\n"
						+ "            <xsl:if test=\"position() = 1\"><xsl:value-of select=\".\"/></xsl:if>\n"
						+ "        </xsl:for-each>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:variable name=\"max_num\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"number($max) = $max\">\n"
						+ "                <xsl:value-of select=\"$max\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"-1\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$to_id]/inbox\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "                <msg cardid=\"{$as}\" name=\"\" number=\"{$max_num+1}\" text=\"{$text}\">\n"
						+ "                    <xsl:if test=\"$will_send_meta='yes'\">\n"
						+ "                        <meta exchange_status=\"{$exchange_status}\">\n"
						+ "                        <xsl:choose>\n"
						+ "                            <xsl:when test=\"$my_included_key!=''\">\n"
						+ "                                <xsl:attribute name=\"proofkey\">\n"
						+ "                                    <xsl:value-of select=\"$my_included_key\"/>\n"
						+ "                                </xsl:attribute>\n"
						+ "                            </xsl:when>\n"
						+ "                            <xsl:otherwise>\n"
						+ "                                <xsl:if test=\"$my_included_try_key!=''\">\n"
						+ "                                    <xsl:attribute name=\"trykey\">\n"
						+ "                                        <xsl:value-of select=\"$my_included_try_key\"/>\n"
						+ "                                    </xsl:attribute>\n"
						+ "                                </xsl:if>\n"
						+ "                            </xsl:otherwise>\n"
						+ "                        </xsl:choose>\n"

						+ "                                <xsl:if test=\"$ls_phone!=''\">\n"

						+ "                        <xsl:attribute name=\"listenrequest_id\">\n"
						+ "                            <xsl:value-of select=\"$listenrequest_id\"/>\n"
						+ "                        </xsl:attribute>\n"
						+ "                        <xsl:attribute name=\"spreadnote_id\">\n"
						+ "                            <xsl:value-of select=\"$spreadnote_id\"/>\n"
						+ "                        </xsl:attribute>\n"
						+ "                        <xsl:attribute name=\"ls_phone\">\n"
						+ "                            <xsl:value-of select=\"$ls_phone\"/>\n"
						+ "                        </xsl:attribute>\n"

						+ "                                </xsl:if>\n"

						+ "                        <xsl:if test=\"$local_key1!=''\">\n"
						+ "                            <key v=\"{$local_key1}\" d=\"{$local_date1}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        <xsl:if test=\"$local_key2!=''\">\n"
						+ "                            <key v=\"{$local_key2}\" d=\"{$local_date2}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        <xsl:if test=\"$local_key3!=''\">\n"
						+ "                            <key v=\"{$local_key3}\" d=\"{$local_date3}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        <xsl:if test=\"$local_key4!=''\">\n"
						+ "                            <key v=\"{$local_key4}\" d=\"{$local_date4}\"/>\n"
						+ "                        </xsl:if>\n"
						+ ""
						+ "                        </meta>\n"
						+ "                    </xsl:if>\n"
						+ "                </msg>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		KeySyncer.inbox_store.performXMLJob(xslt, v);
	}

	String getId4Name () {
		String id4name = (meta.card_id == null)?"":meta.card_id;
		if (id4name.equals("")) {
			id4name = id_sender;
		}
		return id4name;
	}

	private static void logMsg(Store_msg m) {

		String[] l = { "--", "--", "--", "--", "--", "--" };

		if ( m.id_localkey_owner	!= null ) l[0] = m.id_localkey_owner;
		if ( m.id_foreignkey_owner	!= null ) l[1] = m.id_foreignkey_owner;
		if ( m.meta.proof_key		!= null ) l[2] = m.meta.proof_key;
		if ( m.meta.try_key			!= null ) l[3] = m.meta.try_key;
		if ( m.meta.key_set			!= null ) l[3] = m.meta.key_set.value[0];
		if ( m.text 				!= null ) l[5] = m.text;

		if ( m.meta.key_set != null ) {
			if ( m.meta.key_set.value[0] != null ) l[4] = m.meta.key_set.value[0];
		}

		Log.log(5, "    Class Msg: [from, to, text, inlc, firstkey]: " + l[0] + " / " + l[1] + " / " + l[5] + "/" + l[2] + l[3] + " / " + l[4]);
	}

}
