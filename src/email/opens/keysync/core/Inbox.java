/*

	keysyncer
    Copyright (C) 2021  opens email (Thomas Müller / keysyncer@trink2.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Author: Thomas Müller / keysyncer@trink2.com

*/
package email.opens.keysync.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import email.opens.keysync.KeySyncer;
import email.opens.keysync.io.IO;
import email.opens.keysync.io.net.Invite_ServerComm;
import org.w3c.dom.Node;

// Object Inbox - has representation in XML as "inbox"
// ==============
// general idea of objects which have a represention in the XML file:
// 		* usually an object is read from XML and analysed, as long as manipulations
//				are not to be written, it can be thrown away after that
//		* if a manipulation is done (and sould be saved) it will be written to the dom representation
//				ALWAYS using KeySyncer.inbox_store... where "..." indicates [get|set] and the object in lowercase
//		* after all manipulations at the end of the KeySyncer.run method the dom will be
//					written to the XML file

// collection of all situations where inbox_store is used
// only standard methods used: readNode, readNodeList, performXMLJob and .doc

public class Inbox {

			Profile					profile;
			HashSet<Store_msg> 		m;
			Xs 						meXs;

	// m ist the set of messages to go through
	public Inbox(HashSet<HashMap<String, String>> inbox_input_parameter, boolean new_mails_out) {
		profile 	= KeySyncer.profile;

		if ( inbox_input_parameter!= null && inbox_input_parameter.size() > 0 ) {
			Iterator<HashMap<String, String>> i = inbox_input_parameter.iterator();

			HashSet<Store_msg> ms = new HashSet<Store_msg>();
			while ( i.hasNext() ) {
				Store_msg.TextMsgStruct		t		= new Store_msg.TextMsgStruct();
				HashMap<String, String>		item	= i.next();

				t.from		= item.get("from");
				t.to		= item.get("to");
				t.subject	= item.get("subject");
				t.body		= item.get("body");

				ms.add(Store_msg.getMsgFromTextMsg(t));
			}

			Store_msg.msg_store_loop(ms);
		} else {
			IO.setInboxInstance(profile.id, new_mails_out);
		}

		m 			= getInbox();
		meXs 		= profile.xs;
	}

	public static	HashMap<String, Properties>	properties_strings;

    public static class Properties {

        public boolean     is_active;
        public String  str_is_active;
        public String      id_location;// if not given, defaults to what is given in identity / profile, if there is nothing defaults to local
        public String      id_solution;// if not given, defaults to what is given in identity / profile, if there is nothing defaults to xml
        public String      id;			// if not given, defaults to what is given in identity / profile
        public String      login;		// account: optional if no account property is given, if not given, but other properties, defaults to what is given in id
        public String      pwd;		// account: optional if no account property is given, else: mandatory
        public String      smtp_server;// account: optional if no account property is given, if not given, but other properties, defaults to smtp.(id_domain)
        public String      smtp_port;	// account: optional if no account property is given, if not given, but other properties, defaults to dependig on tls
        public String      imap_server;// account: optional if no account property is given, if not given, but other properties, defaults to imap.(id_domain)
        public String      imap_port;	// account: optional if no account property is given, if not given, but other properties, defaults to dependig on tls
		public long		parameter1_lastuid; // email specific parameter 1: remember "UIDFolder.LASTUID" from last time to not check existing emails in inbox again
        // ...
    }

	public class Result {
		HashSet<String>		consider_send_keys;
		HashSet<String[]>	consider_spread_been_spread;
	}
	
	public Result getOpenActions() {
		
		HashSet<String> consider_keys						= new HashSet<String>();
		HashSet<String[]> consider_spread_been_spread		= new HashSet<String[]>();

		// This is the set of strings which will be created during examination of those messages gone through
		Result			ir 									= new Result();
						ir.consider_send_keys 				= consider_keys;
						ir.consider_spread_been_spread		= consider_spread_been_spread;
		
		if ( meXs.intends_exchange ) {

			// Prepare mailbox: go through it and consider functions in messages (e.g. getting foreign keys)
			for ( int status_filter = 0 ; status_filter <= 1 ; status_filter ++ ) { // care about automatic senders first! e.g. to be aware of 1_1 and 2_1 tasks
				Iterator<Store_msg>		local_inbox_iterator	= m.iterator();
				MetaCheck 			meta_result;
				
				while (local_inbox_iterator.hasNext()) {
					// Read Message
					Store_msg			msg 			= local_inbox_iterator.next();

					int exchange_status_message = msg.meta.xs_of_msg_card_id.string.equals(Xs.auto_listen_spread)?0:1;

					if ( exchange_status_message == status_filter ) {
						meta_result = check(msg);
						if (meta_result.isValid) {
							messageJob(meta_result, msg, ir);
						}

						String name = "";
						if (!msg.to_be_deleted) {
							// save foreign keys
							if ( ( msg.meta.key_set != null ) && ( meta_result.trust_foreign_keys_inside_meta || msg.meta.xs_of_msg_card_id.intends_no_automatic ) ) {
								profile.ab.setForeignKeysByMsgAndTrust(msg, meta_result.trust_foreign_keys_inside_meta); // .set_addressbook_ForeignKeysByMsgAndTrust(msg, meta_result.trust_foreign_keys_inside_meta);
							}
							// Put the name right
							Card	c		= new Card(msg.getId4Name());
									name	= c.name; 
							// if necessary update the card
							c.updateXs(msg.id_sender, msg.meta.meta_exchange_status);
						}
						if ( msg.text.equals("") ) msg.to_be_deleted = true;
						replaceOrDeleteMsgInInboxByNameAndNumber(profile.id, msg.id_sender, name, msg.text, msg.number, msg.to_be_deleted?"yes":"");
					}
				}
			}
		}
		return ir;
	}

	class MetaCheck {
		static final int CHECKING		= 0;
		static final int VALID_KEY		= 1;
		static final int INVITATION 	= 2;
		static final int LAUNCH_TASK	= 3;
		static final int INVALID_KEY	= 4;
		static final int MANUAL_JOINS	= 5;

		boolean isValid;
		boolean hasAction;
		int		type;
		String	card_id;
		String 	tanc;
		String 	spread_name;
		String 	key_lead_time_date;

		boolean trust_foreign_keys_inside_meta;
		boolean trust_local_verified_key;
	}

	MetaCheck check(Store_msg mg) {

		MetaCheck mc = new MetaCheck();

		mc.type 							= MetaCheck.CHECKING;
		mc.trust_foreign_keys_inside_meta 	= false;
		mc.trust_local_verified_key			= false;

		if ( mg.meta.has_valid_data ) {

			Addressbook				book	= profile.ab;
			Addressbook.KeyResult 	kr		= book.checkForKey(mg.meta.keyinclude);

			if (kr.isValidKey) {
				String trust = new Card(kr.id).getTrustOfKeyInclude(mg.meta.keyinclude);
				if ( !((trust.equals("")) || (Integer.valueOf(trust) > Key.POTENTIALLY_YES)) ) {
					mc.trust_local_verified_key		= true; // local key which made verification successful was trusted (not only potentially), contact has
				}											//  trusted key from me already, if there is a card_id change or tasks in this meta, it will be performed
				mc.type 							= MetaCheck.VALID_KEY;
				mc.trust_foreign_keys_inside_meta 	= true;	// foreign keys will be saved and get the trust status "TRUSTED"
				mc.card_id 							= kr.id;
			} else if (kr.isInvitationKey) {
				mc.type 							= MetaCheck.INVITATION;
				mc.trust_foreign_keys_inside_meta 	= true;
				mg.meta.phone 							= kr.phone;
				mc.tanc								= kr.tanc;
				mc.spread_name						= kr.spread_name;
				mc.card_id 							= kr.id;
			} else if ( mg.meta.meta_exchange_status.equals(Xs.manual) && ( (mg.meta.xs_of_msg_card_id.string.equals("no")) || (mg.meta.xs_of_msg_card_id.string.equals("")) ) ) {
				mc.type								= MetaCheck.MANUAL_JOINS;
				// some kind of workaround to change the meta@exchange_status attribute without a trusted key
				// exchange_status will only be changed from ("" or "no") to "manual" this way
				// only if exchange_status is "no_at_all" or anything else it will never be change by an email of the card
				mg.meta.card_id = mg.id_sender;
			}

			mc.isValid = ( mc.type != MetaCheck.CHECKING );
		}
		return mc;
	}

	static HashMap<String, Object> getMetaByNode(String as, Node meta_parameter) {
		return get_msg_meta(as, meta_parameter);
	}

	static HashSet<HashMap<String, Object>> getKeysByNode(String path_parameter, Node meta_parameter) {
		return get_msg_keys(path_parameter, meta_parameter);
	}

	public static void newItem(String name_parameter, String id_parameter, String phone_parameter) {
		set_inbox_newItem(name_parameter, id_parameter, phone_parameter);
	}

	public static void setProp(Inbox.Properties ips_parameter, String profile_id_parameter) {
		set_inbox_prop(ips_parameter, profile_id_parameter);
	}

	public static void setLastUIDById(String profile_id_parameter, String location_parameter, String solution_parameter, /* long */ String lastuid_parameter) {
		set_inbox_lastuid_update(profile_id_parameter, location_parameter, solution_parameter, lastuid_parameter);
	}

	static void replaceOrDeleteMsgInInboxByNameAndNumber(String profile_id, String id_sender, String  name, String text, String number, String  to_be_deleted) {
		set_inbox_msg_ByNumber(profile_id, id_sender, name, text, number, to_be_deleted);
	}

	public static void updateProperties(String id_parameter) {

		properties_strings = new HashMap<String, Properties>();

		HashSet<HashMap<String, Object>> configs;
		configs = get_inbox_prop(id_parameter);

		Iterator<HashMap<String, Object>>   it = configs.iterator();

		if ( configs.size() > 0 ) {
			while(it.hasNext()) {
				HashMap<String, Object> a_config_string_map = it.next();

				Properties   	ips 	= new Properties();

				ips.str_is_active       = (String) a_config_string_map.get  ("is_active");
				ips.is_active           = (ips.str_is_active.equals("yes")) ?true:false;
				ips.id_location         = (String) a_config_string_map.get  ("id_location");
				ips.id_solution         = (String) a_config_string_map.get  ("id_solution");
				ips.id                  = (String) a_config_string_map.get  ("id");
				ips.login               = (String) a_config_string_map.get  ("login");
				ips.pwd              	= (String) a_config_string_map.get  ("pwd");
				ips.smtp_server         = (String) a_config_string_map.get  ("smtp_server");
				ips.smtp_port           = (String) a_config_string_map.get  ("smtp_port");
				ips.imap_server         = (String) a_config_string_map.get  ("imap_server");
				ips.imap_port           = (String) a_config_string_map.get  ("imap_port");
				String 		lastuid		= (String) a_config_string_map.get	("parameter1_lastuid");
				long		lu			= 1;
				if ( ( lastuid != null ) && ( !"default".equals(lastuid) ) ) {
					try {
						lu = Long.parseLong(lastuid);
					} catch (NumberFormatException e) {
						Log.log(Log.VERBOSE_EXCEPTIONS, e.getStackTrace().toString());
					}
				}
				ips.parameter1_lastuid	= (lu > 1 )?lu:1;

				if ( "".equals(ips.str_is_active + ips.id_location + ips.id_solution) ) {
					ips = getDefaultProperties();
				}

				if ( ( "".equals(ips.id) ) || ( "default".equals(ips.id) ) ) {
					ips.id = id_parameter;
				}

				if ( ( "".equals(ips.login) ) || ( "default".equals(ips.login) ) ) {
					ips.login = ips.id;
				}

				properties_strings.put(ips.id_location + "/" + ips.id_solution + "@" + ips.id, ips);
			}
		} else { // default

			Properties p = getDefaultProperties();
			properties_strings.put(p.id_location + "/" + p.id_solution + "@", p);
		}
	}

	private HashSet<Store_msg> getInbox() {

		HashSet<HashMap<String, Object>>	inbox_elements		= get_inbox_all_new_msg(profile.id);
		HashSet<Store_msg>					new_msgs			= new HashSet<Store_msg>();

		Iterator<HashMap<String, Object>>	it_inbox_element	= inbox_elements.iterator();

		while (it_inbox_element.hasNext()) {
			Store_msg mm = new Store_msg();
			Meta meta = new Meta(mm);
			meta.xs_of_msg_card_id = new Xs(mm);
			HashMap<String, Object> element = it_inbox_element.next();
			mm.meta = meta;
			mm.text = (String) element.get("text");
			mm.id_sender = (String) element.get("id");
			mm.number = (String) element.get("msg_number");
			mm.meta.DOM_node = (Node) element.get("meta");

			meta.check_for_valid_meta_data_xml();

			new_msgs.add(mm);
		}

		return new_msgs;
	}

	private void messageJob(MetaCheck meta_result, Store_msg msg, Result ir) {
		switch (meta_result.type) {
			case MetaCheck.VALID_KEY: {
				msg.meta.card_id = meta_result.card_id;

				/*boolean xs_auto_before	= Xs.any_automatic.contains(msg.xs_of_msg_card_id.string);
				boolean xs_auto_after	= Xs.any_automatic_spread.contains(msg.meta_exchange_status);

				if ( !xs_auto_before && xs_auto_after ) {

					ir.consider_spread_been_spread.add(new String[] {msg.id_sender, msg.phone});
					System.out.println("sbs (msg.id_sender, msg.phone): " + msg.id_sender + " / " + msg.phone + " / " + msg.xs_of_msg_card_id.string + " / " + msg.meta_exchange_status);
				}*/

				boolean listen			= (msg.meta.listenrequest_id.contains("@"));
				boolean spread			= (msg.meta.spreadnote_id.contains	("@"));
				boolean listen_spread 	= ( ( listen ^ spread ) && !msg.meta.ls_phone.equals("")	);

				if ( 	( listen_spread )
					&& 	msg.meta.xs_of_msg_card_id.intends_exchange 
					&& 	meXs.intends_exchange ) {
					
					if ( listen_spread ) {
						HashSet<String> 	ls			= new HashSet<String>();
						String				ls_id		= listen?msg.meta.listenrequest_id:msg.meta.spreadnote_id;
		
						Card	c	= new Card(msg.meta.ls_phone, 0 /* phone_number indicator */ );
						if (!c.getNameFromPhone(msg.meta.ls_phone).equals("")) {
							c.id = ls_id;
							c.updateIdXsByPhone(msg.meta.ls_phone, Xs.auto_listen_spread);
							c.updateXsToBeSentByCardid(ls_id, Xs.manual);
							ls.add(ls_id);
						}
						ir.consider_send_keys.addAll(ls);
					}
				}

				new Card(msg.getId4Name()).removeLocalKey(msg.meta.keyinclude);
				break;
			}
			case MetaCheck.INVITATION: {
				String		tanc				= meta_result.tanc;
				String		key_lead_time_date	= Invite_ServerComm.getPage(Invite_ServerComm.URL_PREFIX + "answer.php?req=check_key_lead_time&tanc=" + tanc);
							msg.meta.card_id	= meta_result.card_id;
				Card	c					= new Card(msg.meta.phone, 0 /*  phone_number indicator  */);
				
				if ( Invite_ServerComm.keyArrivalExceeded(key_lead_time_date, 2 /* should be "2" - for testing was: 60 */) ) {
					msg.to_be_deleted = true;
					c.set_InvitationExceeded(tanc);
				}
				else {
					c.updateIdXsByPhone(msg.meta.phone, Xs.auto_listen_spread); // this is only important for updating the id, xs will be overwritten by the value the user sent
					meta_result.trust_foreign_keys_inside_meta = true;
					new Card(msg.getId4Name()).removeLocalKey(msg.meta.keyinclude);

					ir.consider_spread_been_spread.add	(new String[] {msg.id_sender, msg.meta.phone});
					ir.consider_send_keys.add			(msg.id_sender);
				}
				break;
			}
			case MetaCheck.INVALID_KEY: {
				msg.to_be_deleted = true;
				break;
			}
			case MetaCheck.MANUAL_JOINS: {
				Card c = new Card(msg.meta.card_id);

				if ( "".equals(c.name) ) {
					msg.to_be_deleted = true;
				} else if ( !msg.meta.key_set.value[0].equals("") ) {
					ir.consider_send_keys.add(msg.getId4Name());
				}
				break;
			}
		}
	}	

	private static Properties getDefaultProperties() {

		Properties   ips 	= new Properties();

		ips.str_is_active       = "yes";
		ips.is_active           = true;
		ips.id_location         = "local";
		ips.id_solution         = "xml";
		ips.id                  = "";
		ips.login               = "";
		ips.pwd              	= "";
		ips.smtp_server         = "";
		ips.smtp_port           = "";
		ips.imap_server         = "";
		ips.imap_port           = "";
		ips.parameter1_lastuid	= 1;

		return ips;

	}

	/*
			Inbox interface methods
	*******************************************************************************************************************
				The following methods ONLY interact with xml structure
									  ONLY  			 xml structure				_G_ETTER
        HashSet<HashMap<String, Object>>    get_inbox_all_new_msg(String as)
static  HashSet<HashMap<String, Object>>    get_inbox_prop(String profile_id_parameter)
static  HashMap<String, Object>             get_msg_meta(String as, Node meta_parameter) {
static  HashSet<HashMap<String, Object>>    get_msg_keys(String path_parameter, Node meta_parameter) {
									  ONLY  			 xml structure				_S_ETTER
static  void                                set_inbox_prop(Inbox.Properties ips_parameter, String profile_id_parameter)
static  void                                set_inbox_lastuid_update(String profile_id, String location, String solution, String lastuid)
static  void                                set_inbox_newItem(String name_parameter, String id_parameter, String phone_parameter) {
static  void                                set_inbox_msg_ByNumber(String prof_id, String sender, String name, String tx, String no, String del)
	*******************************************************************************************************************
	 */

	private HashSet<HashMap<String, Object>> get_inbox_all_new_msg(String as) {

		String path = "/identities/identity[@id='" + as + "']/inbox/msg[@name='']";

		HashSet<String[]> par = new HashSet<String[]>();

		String[] t = {"./attribute::text",			"STRING",	"text"};		par.add(t);
		String[] n = {"./attribute::number",		"STRING",	"msg_number"};	par.add(n);
		String[] i = {"./attribute::cardid",		"STRING",	"id"};			par.add(i);
		String[] m = {"./meta",						"NODE",		"meta"};		par.add(m);

		return KeySyncer.inbox_store.readNodeList (path, KeySyncer.inbox_store.doc, par);
	}

	private static HashSet<HashMap<String, Object>> get_inbox_prop(String profile_id_parameter) { // up to now only ONE inbox

		String path = "/identities/identity[@id='" + profile_id_parameter + "']/inbox"; // specify inbox later by id_inbox, id_location, id_solution

		HashSet<String[]> par = new HashSet<String[]>();

		String[] a  = {"./attribute::is_active",				"STRING",	"is_active"			};	par.add(a);
		String[] t  = {"./attribute::id_location",			"STRING",	"id_location"		};	par.add(t);
		String[] s  = {"./attribute::id_solution",			"STRING",	"id_solution"		};	par.add(s);
		String[] d  = {"./attribute::id",					"STRING",	"id"				};	par.add(d);
		String[] l  = {"./attribute::login",					"STRING",	"login"				};	par.add(l);
		String[] w  = {"./attribute::pwd",					"STRING",	"pwd"				};	par.add(w);
		String[] s1 = {"./attribute::smtp_server",			"STRING",	"smtp_server"		};	par.add(s1);
		String[] s2 = {"./attribute::smtp_port",				"STRING",	"smtp_port"			};	par.add(s2);
		String[] i1 = {"./attribute::imap_server",			"STRING",	"imap_server"		};	par.add(i1);
		String[] i2 = {"./attribute::imap_port",				"STRING",	"imap_port"			};	par.add(i2);
		String[] p1 = {"./attribute::parameter1_lastuid",	"STRING",	"parameter1_lastuid"};	par.add(p1);

		return KeySyncer.inbox_store.readNodeList (path, KeySyncer.inbox_store.doc, par);
	}

	private static HashMap<String, Object> get_msg_meta(String as, Node meta_parameter) {

		HashSet<String[]> par = new HashSet<String[]>();

		String[] p =	{"./attribute::proofkey",			"STRING", "proofkey"};			par.add(p);
		String[] t =	{"./attribute::trykey",				"STRING", "trykey"};			par.add(t);
		String[] x =	{"./attribute::exchange_status",	"STRING", "exchange_status"};	par.add(x);
		String[] l =	{"./attribute::listenrequest_id",	"STRING", "listenrequest_id"};	par.add(l);
		String[] s =	{"./attribute::spreadnote_id",		"STRING", "spreadnote_id"};		par.add(s);
		String[] ls =	{"./attribute::ls_phone",			"STRING", "ls_phone"};			par.add(ls);

		return KeySyncer.inbox_store.readNode("", meta_parameter, par);
	}

	private static HashSet<HashMap<String, Object>> get_msg_keys(String path_parameter, Node meta_parameter) {

		HashSet<String[]> par = new HashSet<String[]>();

		String[] v = {"./attribute::v",	"STRING",	"v"};	par.add(v);
		String[] d = {"./attribute::d",	"STRING",	"d"};	par.add(d);

		return KeySyncer.inbox_store.readNodeList (path_parameter, meta_parameter, par);
	}

	private static void set_inbox_prop(Inbox.Properties ips_parameter, String profile_id_parameter) { // up to now only ONE inbox

		// specify inbox later by id (_inbox), id_location, id_solution, also make possible to update existing data (e.g. for account change)

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"is_active", 			ips_parameter.str_is_active});
		v.add(new String[] {"id_location", 			ips_parameter.id_location });
		v.add(new String[] {"id_solution",			ips_parameter.id_solution });
		v.add(new String[] {"id", 					ips_parameter.id });
		v.add(new String[] {"login",					ips_parameter.login });
		v.add(new String[] {"pwd", 					ips_parameter.pwd });
		v.add(new String[] {"smtp_server",			ips_parameter.smtp_server });
		v.add(new String[] {"smtp_port", 			ips_parameter.smtp_port });
		v.add(new String[] {"imap_server",			ips_parameter.imap_server});
		v.add(new String[] {"imap_port", 			ips_parameter.imap_port});
		v.add(new String[] {"parameter1_lastuid", 	String.valueOf(ips_parameter.parameter1_lastuid)});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"is_active\"/>\n"
						+ "    <xsl:param name=\"id_location\"/>\n"
						+ "    <xsl:param name=\"id_solution\"/>\n"
						+ "    <xsl:param name=\"id\"/>\n"
						+ "    <xsl:param name=\"login\"/>\n"
						+ "    <xsl:param name=\"pwd\"/>\n"
						+ "    <xsl:param name=\"smtp_server\"/>\n"
						+ "    <xsl:param name=\"smtp_port\"/>\n"
						+ "    <xsl:param name=\"imap_server\"/>\n"
						+ "    <xsl:param name=\"imap_port\"/>\n"
						+ "    <xsl:param name=\"parameter1_lastuid\"/>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities/identity[@id='" + profile_id_parameter + "']/inbox\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "            <xsl:if test=\"$is_active!=''\">\n"
						+ "            <xsl:attribute name=\"is_active\">\n"
						+ "                <xsl:value-of select=\"$is_active\"/>\n"
						+ "            </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$id_location!=''\">\n"
						+ "            <xsl:attribute name=\"id_location\">\n"
						+ "                <xsl:value-of select=\"$id_location\"/>\n"
						+ "            </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$id_solution!=''\">\n"
						+ "            <xsl:attribute name=\"id_solution\">\n"
						+ "                <xsl:value-of select=\"$id_solution\"/>\n"
						+ "            </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$id!=''\">\n"
						+ "                <xsl:attribute name=\"id\">\n"
						+ "                    <xsl:value-of select=\"$id\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$login!=''\">\n"
						+ "                <xsl:attribute name=\"login\">\n"
						+ "                    <xsl:value-of select=\"$login\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$pwd!=''\">\n"
						+ "                <xsl:attribute name=\"pwd\">\n"
						+ "                    <xsl:value-of select=\"$pwd\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$smtp_server!=''\">\n"
						+ "                <xsl:attribute name=\"smtp_server\">\n"
						+ "                    <xsl:value-of select=\"$smtp_server\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$smtp_port!=''\">\n"
						+ "                <xsl:attribute name=\"smtp_port\">\n"
						+ "                    <xsl:value-of select=\"$smtp_port\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$imap_server!=''\">\n"
						+ "                <xsl:attribute name=\"imap_server\">\n"
						+ "                    <xsl:value-of select=\"$imap_server\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$imap_port!=''\">\n"
						+ "                <xsl:attribute name=\"imap_port\">\n"
						+ "                    <xsl:value-of select=\"$imap_port\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"

						+ "            <xsl:if test=\"$parameter1_lastuid!=''\">\n"
						+ "                <xsl:attribute name=\"parameter1_lastuid\">\n"
						+ "                    <xsl:value-of select=\"$parameter1_lastuid\"/>\n"
						+ "                </xsl:attribute>\n"
						+ "            </xsl:if>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		KeySyncer.inbox_store.performXMLJob(xslt, v);
	}

	private static void set_inbox_lastuid_update(String profile_id_parameter, String location_parameter, String solution_parameter, /* long */ String lastuid_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"profile_id", 			profile_id_parameter});
		v.add(new String[] {"id_location", 			location_parameter });
		v.add(new String[] {"id_solution",			solution_parameter });
		v.add(new String[] {"parameter1_lastuid",	lastuid_parameter });

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"profile_id\"/>\n"
						+ "    <xsl:param name=\"id_location\"/>\n"
						+ "    <xsl:param name=\"id_solution\"/>\n"
						+ "    <xsl:param name=\"parameter1_lastuid\"/>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities/identity[@id=$profile_id]/inbox[not(@id)and(@id_location=$id_location)and(@id_solution=$id_solution)]/@parameter1_lastuid\">\n"
						+ "       <xsl:attribute name=\"parameter1_lastuid\">\n"
						+ "           <xsl:value-of select=\"$parameter1_lastuid\"/>\n"
						+ "       </xsl:attribute>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities/identity/inbox[(@id=$profile_id)and(@id_location=$id_location)and(@id_solution=$id_solution)]/@parameter1_lastuid\">\n"
						+ "       <xsl:attribute name=\"parameter1_lastuid\">\n"
						+ "           <xsl:value-of select=\"$parameter1_lastuid\"/>\n"
						+ "       </xsl:attribute>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		KeySyncer.inbox_store.performXMLJob(xslt, v);
	}

	private static void     set_inbox_newItem(String name_parameter, String id_parameter, String phone_parameter) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"id", 			id_parameter});
		v.add(new String[] {"name",			name_parameter});
		v.add(new String[] {"phone",		phone_parameter});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ "\n"
						+ "    <xsl:param name=\"id\"/>\n"
						+ "    <xsl:param name=\"name\"/>\n"
						+ "    <xsl:param name=\"phone\"/>\n"
						+ "\n"
						+ "    <xsl:variable name=\"max\">\n"
						+ "        <xsl:for-each select=\"/identities/identity/attribute::number\">\n"
						+ "            <xsl:sort select=\".\" data-type=\"number\" order=\"descending\"/>\n"
						+ "            <xsl:if test=\"position() = 1\"><xsl:value-of select=\".\"/></xsl:if>\n"
						+ "        </xsl:for-each>\n"
						+ "    </xsl:variable>\n"
						+ "\n"
						+ "    <xsl:variable name=\"max_num\">\n"
						+ "        <xsl:choose>\n"
						+ "            <xsl:when test=\"number($max) = $max\">\n"
						+ "                <xsl:value-of select=\"$max\"/>\n"
						+ "            </xsl:when>\n"
						+ "            <xsl:otherwise>\n"
						+ "                <xsl:value-of select=\"-1\"/>\n"
						+ "            </xsl:otherwise>\n"
						+ "        </xsl:choose>\n"
						+ "    </xsl:variable>\n"
						+ "\n"
						+ "    <xsl:template match=\"/identities\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "                <identity id=\"{$id}\" name=\"{$name}\" number=\"{$max_num+1}\" phone=\"{$phone}\">\n"
						+ "                    <inbox/>\n"
						+ "                </identity>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ "\n"
						+ "</xsl:stylesheet>\n";

		KeySyncer.inbox_store.performXMLJob(xslt, v);
	}

	private static void     set_inbox_msg_ByNumber(String profile_id, String id_sender, String  name, String text, String number, String  to_be_deleted) {

		Vector<String[]> v = new Vector<String[]>();

		v.add(new String[] {"as", 				profile_id});
		v.add(new String[] {"id_sender",		id_sender});
		v.add(new String[] {"name",				name});
		v.add(new String[] {"text",				text});
		v.add(new String[] {"msg_number",		number});
		v.add(new String[] {"to_be_deleted",	to_be_deleted});

		String xslt =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
						+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n"
						+ ""
						+ "    <xsl:param name=\"as\"/>\n"
						+ "    <xsl:param name=\"id_sender\"/>\n"
						+ "    <xsl:param name=\"name\"/>\n"
						+ "    <xsl:param name=\"text\"/>\n"
						+ "    <xsl:param name=\"msg_number\"/>\n"
						+ "    <xsl:param name=\"to_be_deleted\"/>\n"
						+ ""
						+ "    <xsl:template match=\"identity[@id=$as]/inbox/msg[@number=$msg_number]\">\n"
						+ "        <xsl:if test=\"$to_be_deleted=''\">\n"
						+ "            <msg cardid=\"{$id_sender}\" name=\"{$name}\" number=\"{$msg_number}\" text=\"{$text}\"/>\n"
						+ "        </xsl:if>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "    <xsl:template match=\"node()|@*\">\n"
						+ "        <xsl:copy>\n"
						+ "            <xsl:apply-templates select=\"node()|@*\"/>\n"
						+ "        </xsl:copy>\n"
						+ "    </xsl:template>\n"
						+ ""
						+ "</xsl:stylesheet>\n";

		KeySyncer.inbox_store.performXMLJob(xslt, v);
	}

}
