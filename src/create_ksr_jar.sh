#!/bin/sh

[ ! -d "lib" ] && {
    mkdir -p "lib";
}

cd lib
! [ -f "jakarta.mail-1.6.6.jar" ]         && curl -O https://repo1.maven.org/maven2/com/sun/mail/jakarta.mail/1.6.6/jakarta.mail-1.6.6.jar;
! [ -f "jakarta.activation-1.2.1.jar" ]   && curl -O https://repo1.maven.org/maven2/com/sun/activation/jakarta.activation/1.2.1/jakarta.activation-1.2.1.jar
cd ..

mkdir tmp
cp -r email tmp
cp -r lib tmp
cd tmp

javac -cp lib/jakarta.activation-1.2.1.jar:lib/jakarta.mail-1.6.6.jar email/opens/keysync/*.java email/opens/keysync/core/*.java email/opens/keysync/cli/*.java email/opens/keysync/io/*.java email/opens/keysync/io/local/*.java email/opens/keysync/io/net/*.java

jar -xf lib/jakarta.activation-1.2.1.jar
jar -xf lib/jakarta.mail-1.6.6.jar

rm email/opens/keysync/*.java
rm email/opens/keysync/core/*.java
rm email/opens/keysync/cli/*.java
rm email/opens/keysync/io/*.java
rm email/opens/keysync/io/local/*.java
jar cfe ksr.jar email/opens/keysync/KeySyncer .
cd ..
cp tmp/ksr.jar .
rm -r tmp

echo "KeySyncer Copyright (C) 2021 opens email (Thomas Müller/keysyncer@trink2.com)"
echo "This program comes with ABSOLUTELY NO WARRANTY; This is free software,"
echo "   and you are welcome to redistribute it under certain conditions; see"
echo "   https://gitlab.com/opens-email/keysyncer/-/blob/master/LICENSE for details."
echo "Or see GPL3 <https://www.gnu.org/licenses/> for details."
echo
echo "For 3rd party licenses for jakarta.mail and jakarta.activation see"
echo "   https://gitlab.com/opens-email/keysyncer/-/tree/master/src/lib"
echo
echo "Run the KeySyncer using java -jar ksr.jar - an exmaple:"
echo "java -jar ksr.jar do=profile_create profile_name=Thomas profile_id=t@hom.as profile_phone=7696"

