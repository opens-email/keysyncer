# Communication is key - let last sentence of README point to CONTRIBUTING
I am not yet experienced in Free and Open Source Projects and CONTRIBUTING. Therefore I believe I would like to put communication in the first place. Apart from making it public I opened this project as open source to get feedback... As I am not (yet) aware of what all kinds of contribution can happen. So, please let us communicate to pick me up, if necessary!
#### Your wishes - My wishes - Decentralization - This file is likey to grow and develop
---
## Your whishes - find my guesses, to add more, create an issue, get in touch
#### You found an error or a problem
Please create an issue or email me!
#### The POC shall get a mature piece of software
I intend to include it into an email based messenger. I do not give a prediction, yet.
#### Found another use case? Disagree with the usecase?
Please get in touch with me!
#### "I would like to add/change this or that"
Get in touch with me, explain your point. Either clone, develop for youself, but please let me know! Or in case you are interested or expereienced, just propose a process how to include your contribution.
## My whishes - I like the idea, not this code
#### Simplify
This is a POC ("proof of concept"). KeySyncer is an assembly of already existing stuff. Shouldn't it be possible to do it in 10 lines of bash and 5 XSLT files? Reminder: https://en.wikipedia.org/wiki/Unix_philosophy It is about changing the account flawlessly without the need of a server. Can this be done with a more easy concept?
#### Maturity
It is working with expected values, but it does not give good messages in case of wrong values, it causes exceptions. I did not yet run code checkers on it, I will do that. Simplification should happen first. Email encryption and disadvantages of email need to be addressed.
#### Develop the idea of decentralization
Can an email arrive on a device without the need of a server? I think yes.
## This file is likey to grow and develop
...with every lesson I learn.

contact: keysyncer@trink2.com