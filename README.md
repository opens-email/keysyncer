# KeySyncer
#### Purpose - Status - Progress - Build - Conclusion
---
## Purpose
#### General

* The general idea of "email opens" is to use email as the basis of opening the net for **quick, easy and standardized messaging**. Email is not considered the goal, it is considered as the start of a journey. Goal for a far away future could be XMPP with OMEMO.
* KeySyncer in particular is a **"proof of concept"** for providing the ability to **change a sender's address flawlessly**. KeySyncer's "core" does this based on XML data using "io.local". "io.net" can do it using email based on javamail.
* By this, it **takes away the dependency** on a provider or on a specific account. In the future it might enable indepenency of the protocol (like the protocol today is email).
* As the name suggests, it **synchronizes keys** among persons in the addressbook who also participate in doing this. Having done that, it can **presents a key as proof of identity**. By that the sender's address will be taken over for futher usage.
* The **ability** (of a user and related contacts) to **participate can be configured manually**
* It is more **easily** achieved through **inviting** each contact whwo will be a new participant
* Additionally an **invitation** will **introduce** the ability to sync **to other contacts** who can

#### Use case - quick, easy and independant messaging

**Providing an email account** at least optionally **is key** to achieve **"quick and easy"**.

Considering to provide an account, there is the goal to not depend on it. The ability to flawlessly change away from it is mandatory. That feature is completely given as long as it is only used with contacts which sync keys with it. This can easily be achieved by adding contacts by the use of inivitations, only.

For both parties (inviter and invited) it is agreed to sync keys with each other. The invitation is designed to hand over the new genrated provided account of the invited contact back to the inviter. They immediately start to sync. In a second step the inviter introduces the invited contact's new address to other contacts who are able to sync keys as well and vice versa. Those known to each other will start to sync keys.

Apart from this quick and easy way to start, all other variations are possible additionally, to describe one of them: give ones own account right from the start, manually setting up with whom to sync. Others are described in a more detailed documentation which is to be created.


On a **self hosted** basis it is possible to **provide email account**s' credentials, **invite** friends who are enabled to get one of these new accounts and getting them **in touch with other contacts**.

The self hosted solution is already existing and a messaging app to use as a basis exists, but is not yet integrated. Both are considred to be linked here.
## Status
Source code upload is finished. The next step is putting the known problems into gitlab issues. While doing that also add documentation.
## Progress
the **current step** is indicated using bold letters

creating the POC --> **sharing it with the public** --> getting feedback --> improving until maturity --> merge with email based applications --> enable the connected network to switch to any other decentralised network (besides email) of choice and availability

## How to build and learn how it works
#### Build
Download and unzip the repository or clone it. Assuming to do it inside **Linux** (and **jdk15**), go to the folder "src" run the script **"create_ksr_jar.sh"**. On Windows analyse the file and go manually. The file "ksr.jar" is the result. If it is avaiable, the terminal output shows a sample usage, which creates a new user in the file "identities.xml" which will be created in the same folder.
#### Setup some users to synchronize keys
The result of each step is saved in the file "identities.xml" and with this file it is possible to monitor what is happening.
The structure of the file can have a number of users who can receive messages and who have an inbox to store messages. Additionally the have an addressbok where those persons are in who are trusted to connect to their address.

* "**identities**" is the top node
    * below it has nodes for each individual "**identity**" (who has a name, an ID (can be email address like) and a phone number.
        * an identity has subnodes: an **inbox** holding messages (**msg**) and an **addresbook** holding **card**s which represent the contacts.
            * a card as well has an individual name, an ID and a phone number.
                * a card can store **key**s, there are local (the identity created them and intends to send them to the contact) and foreign ones (they have been received from the contact and can be used to identify the contact).
                    * each key holds information about its encryption (while transported from only system to another)
                    * it holds information when it will be due
                    * it has a value which has an ID and a random number in it and is considered the key to get identified by
                    * it has a status about the trust level (it is only trusted, if has been received with a key in it that has been sent including a key also). It takes some steps until this is the case. Only trusted keys can be used to simply send a message with a completely new ID (to be taken as the new ID).

In the following **scenario** we will see, how **two identities exchange keys**. *Part I* will set up the users in the XML file, *Part II* shows how a synchronization begins and *Part III* shows how it comes to an end and how to analyse the steps to get to an end.

*Part I - the setup*

*   create an identity:
> java -jar ksr.jar do=profile_create profile_name=Stella profile_id=s@trink2.com profile_phone=4901
*   create another identity:
> java -jar ksr.jar do=profile_create profile_name=Hera id=h@trink2.com phone=4902
*   getting all available contacts into the other one's addressbook:
> java -jar ksr.jar do=card_link_all

*Part II - configure who intends to participate in sync*

*   configure an identity to be willing to synchronize:
> java -jar ksr.jar do=profile_conf_xs id=s@trink2.com xs=automatic_listen_spread
*   other identity is also willing to synchronize:
> java -jar ksr.jar do=profile_conf_xs id=h@trink2.com xs=automatic_listen_spread
*   getting to know the other one joins the sync (if you intend to try and follow identities.xml, read Part III before proceeding):
> java -jar ksr.jar do=card_conf_xs profile_id=s@trink2.com card_id=h@trink2.com xs=automatic_listen_spread

*Part III*

The latter one lets the sync start as Stella (automatic_listen_spread) and the related contact Hera (xs=automatic_listen_spread) are **willing to do so**.

As given above it is run from Stellas position. Only the tasks will be done, that are done by Stella. So she sends a message and that's it. If you like to **follow all the steps of other users** who are known to Stella add a "**simulate_write**" as the first parameter to the Java application.
> java -jar ksr.jar simulate_write do=card_conf_xs profile_id=s@trink2.com card_id=h@trink2.com xs=automatic_listen_spread

Watch the file "identities.xml" to get an idea how keys are stored and how messages appear in the inbox.

If you run the last command from Part II without "simulate_write" you will see what is the result of Stella sending her first message to initiate an exchange of keys (in other words a key synchronization).

There is also the option to **watch the "simulation" step by step**. For the time it is necessary to change the source code for that:
* the file "SimulateAndMonitor.java" will be the one to adapt
* in the file find the line which starts with "static final boolean **STOP_ON_STEPS**"
* modifiy what you find on the right side of "=" to "true" (there is probably "false")
* as there are different possible behaviours these are listed below
* the name of a behaviour start with "STOP_AND_"
* if you intend to choose one of them copy the complete name (this is optional, you can leave in place what is chosen)
* find the line which starts with "static final int STOP_BEHAVIOUR_SELECTED"
* replace in that line what is between "=" and ";"

After that compile again by running the script "**create_ksr_jar.sh**".

To **start from scratch**, either **edit the file "identities.xml" and delete all lines, then save it**. Alternatively you can delete the complete file.

Run again the commands given above, the last one done with "simulate_write" will now stop, depending on what behaviour has been chosen.

Here a hint how to **visually follow what is happening** to the file "identities.xml" easier: open another terminal and then follow the file's content by typing
> **watch -n 2** cat identities.xml

Hope this makes it possible to understand the principle of what the application does.
#### Make use of the email protocol (only IMAP in this case)
Intoduce the access to an email account to the inbox handing over the following data:

* profile_id ==> id/email address    id of the already existing inbox
* is_active ==> yes                 no alternative, for later use
* id_location ==> net                 (local/net)
* id_solution ==> email               (email only up to now)
* id_inbox ==> id/email address    in case a profile has more than one inbox
* login ==> for email account   this might differ from email address
* pwd ==> for email account
* smtp_server ==> servername
* smtp_port ==> serverport
* imap_server ==> servername
* imap_port ==> serverport
* parameter1_lastuid ==> filed by the system to remember what messages have been check already

An exmaple how to introduce an email inbox for ther user Hera (see above)
> java -jar ksr.jar do=inbox_conf profile_id=h@trink2.com is_active=yes id_location=net id_solution=email id_inbox=default login=default pwd=secret smtp_server=smtp.ionos.de smtp_port=465 imap_server=imap.ionos.de imap_port=default parameter1_lastuid=default

Available options having the account introduced to the inbox:

* on the command line: sending a regular message
* on the command line: observere the mailbox (while automatically answering sync emails)
* use KeySyncer as library to care about synchronization and send and receive messages

Sending a message
> java -jar ksr.jar do=send profile_id=your_inbox_id card_name=recipient_name_from_addressbook text=messagetext

Obeserving the mailbox
> java -jar ksr.jar do=profile_auto profile_id=your_inbox_id mailer=no

## Concluding notes
**Remember this is a proof of concept.** The software we see here should be there to study the concept and learn.

Remember **email** is **not the goal**, it is just the decentralised piece I am familiar with and the one with the broadest audience. You could choose XMPP or Matrix as protocol that could do a similar thing.

Resepctful feedback is welcome: keysyncer@trink2.com

For consideration to contribute, see CONTRIBUTING.md.
